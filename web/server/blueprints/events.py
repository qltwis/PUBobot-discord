from quart import Blueprint, jsonify, current_app, request, abort
from .utils import local_request

app = Blueprint('Events', __name__)


@app.route('/events', methods=['POST'])
@local_request
async def events():
    data = await request.get_json()
    # TODO: use dict of functions and make is_in_match_decorator
    for client in current_app.connected_websocket_clients:
        if data['event'] == 'match_change':
            if str(client.user_id) in [player['id'] for player in data['match']['players']]:
                await client.queue.put(data)
        elif data['event'] == 'map_pick':
            if str(client.user_id) in [player['id'] for player in data['match']['players']]:
                # remove players list, not needed
                match = {key: data['match'][key] for key in data['match'] if key != 'players'}
                await client.queue.put({'event': data['event'], 'match': match})
        elif data['event'] == 'player_pick':
            if str(client.user_id) in [player['id'] for player in data['match']['players']]:
                # remove players list, not needed
                match = {key: data['match'][key] for key in data['match'] if key != 'players'}
                await client.queue.put({'event': data['event'], 'match': match})
        elif data['event'] == 'set_ready':
            if str(client.user_id) in [player['id'] for player in data['match']['players']]:
                await client.queue.put(data)
        elif data['event'] == 'pickup_change':
            if str(client.user_id) in [player['id'] for player in data['pickup']['players']]:
                client.channels.add(data['pickup']['channel_id'])
                # TODO: update client channels when needed
                await client.queue.put(data)
            elif data['pickup']['channel_id'] in client.channels:
                client.channels.remove(data['pickup']['channel_id'])
                await client.queue.put(data)
        else:
            print('WARN: event not implemented/recognized', data)
            # await client.queue.put(data)
    return jsonify(True)
