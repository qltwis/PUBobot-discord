from discord import User


class PubobotException(BaseException):

    def with_mention(self, user: User):
        return f"{user.mention} {str(self)}"


class NotInActiveMatch(PubobotException):
    def __str__(self):
        return "Could not find an active match."


class AlreadyInActiveMatchException(PubobotException):
    def __str__(self):
        return "You are already in an active match"


class NoAddException(PubobotException):
    def __init__(self, string):
        self._str = string

    def __str__(self):
        return self._str


class MissingModeratorRightsException(PubobotException):
    def __str__(self):
        return "missing moderator rights"


class WrongTimeFormatException(PubobotException):
    def __init__(self, part):
        self.part = part

    def __str__(self):
        return f"Bad argument @ `{self.part}`, format is: 1h 2m 3s"
