"""
module for integration with http://gamestv.org
API keys are exclusive for GatherMaster#5809 bot account
"""

from modules import stats3, client, console
from modules.utils import int_to_emoji
from time import sleep
from threading import Thread
import re

try:
    from modules import gtvd_config
except ImportError as e:
    print(
        str(e) + "\n"
        "This gtvd module is only meant for GatherMaster#5809.\n"
        "Just create empty file and continue setting up :)\n"
        "to create empty file: touch modules/gtvd_config.py"
    )
    exit(-1)

if hasattr(gtvd_config, 'ENDPOINT_URL'):
    import requests

"""
{
    "team1": [
        "player1",
        "player2"
    ],
    "team2": [
        "player3",
        "player4"
    ],
    "matchserver": {
        "ip": "127.0.0.1:27960",
        "pass": "xxx",
        "ettvpass": "xxx"
    }
}
"""


def sanitize_nick(nick):
    sub = re.sub(r'[^0-9a-zA-Z]+', '', nick)
    if sub == '':
        return 'EmptyNick'
    else:
        return sub


def get_member_info(member, server_id):
    member_info = stats3.get_member_info(member.id, server_id)
    if member_info is not None and member_info[2] is not None:
        nick = member_info[2]
    else:
        nick = member.nick or member.name
    if member_info is not None and member_info[3] is not None:
        country = member_info[3][:2]
    else:
        country = 'xx'
    return {
        'player': sanitize_nick(nick),
        'country': country
    }


def create_match(bot_match):
    if bot_match.channel.guild.id not in gtvd_config.GUILD_WHITELIST:
        console.display('guild not in gtvd whitelist')
        return

    json = {
        'team1': [get_member_info(i, bot_match.channel.guild.id) for i in bot_match.alpha_team],
        'team2': [get_member_info(i, bot_match.channel.guild.id) for i in bot_match.beta_team],
        'matchserver': {
            'ip': bot_match.ip,
            'pass': bot_match.password,
            'ettvpass': bot_match.ettv_password
        },
        'maps': bot_match.map.split(', ') if bot_match.map is not None else []
    }
    try:
        r = requests.post(gtvd_config.ENDPOINT_URL, headers={'Gtv-Auth': gtvd_config.AUTH}, json=json)
        console.display(r.__dict__)
        console.display(r.json())
    except requests.exceptions.ContentDecodingError:
        try:
            console.display("requests.exceptions.ContentDecodingError")
            moderator_role = bot_match.pickup.channel.get_value('moderator_role')
            admin_role = bot_match.pickup.channel.get_value('moderator_role')
            msg = ""
            if moderator_role is not None:
                msg += "<@&{}> ".format(moderator_role)
            if admin_role is not None:
                msg += "<@&{}> ".format(admin_role)
            msg += "Error decoding response from gamestv - match score and status will have to be reported manually"
            client.notice(bot_match.channel, msg)
        except:
            pass
    else:
        try:
            bot_match.gtv_match_id = r.json()['matchid']
            poll_relays(bot_match)
        except KeyError:
            client.notice(
                bot_match.channel,
                "Error creating gamestv match")


def finish_match(match_id, teama_score=None, teamb_score=None):
    json = {'matchid': match_id}
    if teama_score is not None and teamb_score is not None:
        json['scores'] = {
            "teama": teama_score,
            "teamb": teamb_score
        }
    json['status'] = 'finished'
    r = requests.patch(gtvd_config.ENDPOINT_URL, headers={'Gtv-Auth': gtvd_config.AUTH}, json=json)
    console.display(r.content)


def cancel_match(match_id):
    r = requests.patch(gtvd_config.ENDPOINT_URL, headers={'Gtv-Auth': gtvd_config.AUTH},
                       json={'matchid': match_id, 'status': 'removed'})
    console.display(r.content)


def match_info(match_id):
    r = requests.get(gtvd_config.ENDPOINT_URL, headers={'Gtv-Auth': gtvd_config.AUTH},
                     params={'matchid': match_id})
    return r.json()


def update_match_server(bot_match):
    json = {
        'matchid': bot_match.gtv_match_id,
        'matchserver': {
            'ip': bot_match.ip,
            'pass': bot_match.password,
            'ettvpass': bot_match.ettv_password
        }
    }
    r = requests.patch(gtvd_config.ENDPOINT_URL, headers={'Gtv-Auth': gtvd_config.AUTH},
                     json=json)
    console.display(r.content)
    return r.json()


def get_relays(match_id):
    json = match_info(match_id)
    return [] if json == [] else json['relays']


def relays_to_str(relays, prepend=""):
    if len(relays):
        rv = prepend
        for relay in relays:
            rv += relay['uri']
            if int(relay['players']) > 0:
                rv += " {} :eyes:".format(int_to_emoji(relay['players']))
        return rv
    else:
        return ""


def poll_relays(match):
    def poll_thread():
        retry_count = 0
        sleep(10)
        while True:
            if retry_count >= 10:
                client.notice(
                    match.channel,
                    "Match #{} ettv poll timeout".format(match.id))
                return
            relays = get_relays(match.gtv_match_id)
            if len(relays):
                client.notice(
                    match.channel,
                    "Match #{} ETTV broadcast: {}".format(match.id, relays_to_str(relays)))
                return
            sleep(30)
            retry_count += 1

    Thread(target=poll_thread).start()


def match_link(match_id):
    return "https://www.gamestv.org/event/"+str(match_id)
