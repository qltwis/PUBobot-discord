import datetime
import json
import random
import re
import time
import os
from enum import Enum

from discord.utils import escape_mentions
from itertools import combinations
from json import dump, loads, JSONDecodeError
from time import mktime
from typing import Optional, List, Tuple, Dict

from discord import errors, Streaming, Embed, Message, User, Member, Interaction, TextChannel, ButtonStyle
from discord.ui import View, Button
from tabulate import tabulate, SEPARATING_LINE
import arrow
from sentry_sdk import start_transaction

from modules import client, config, console, stats3, scheduler, utils, member_info, events, rating
from modules import game_server
from modules import gtvd
from modules.exceptions import *
from modules.party import Party, PublicParty, PickupParty, NotInPartyException, PartyNotCaptainException
from modules.utils import split

max_expire_time = 6 * 60 * 60  # 6 hours
max_bantime = 30 * 24 * 60 * 60 * 12 * 3  # 30 days * 12 * 3
max_match_alive_time = 12 * 60 * 60  # 12 hours
team_emojis = [":fox:", ":wolf:", ":dog:", ":bear:", ":panda_face:", ":tiger:", ":lion:", ":pig:", ":octopus:",
               ":boar:", ":spider:", ":scorpion:", ":crab:", ":eagle:", ":shark:", ":bat:", ":gorilla:", ":rhino:",
               ":dragon_face:", ":deer:", ":elephant:"]


def init():
    global channels, active_pickups, active_matches, allowoffline
    channels = []
    active_pickups = []
    active_matches = []
    allowoffline = []  # users with !allowoffline


class Teams(Enum):
    NONE = None
    ALPHA = 0
    BETA = 1


class Match:

    def __init__(self, pickup, players, winner=None, parties=None, match_dict=None):

        # set match id
        stats3.last_match += 1
        self.id = stats3.last_match

        self.picked_maps = []
        self.ip = None
        self.password = None
        self.ettv_password = None
        self.server = None
        self.gtv_match_id = None
        self.streaming_players: Dict[int, Streaming] = {}
        self.re_queue: Dict[Channel, Dict[Pickup, List[Member]]] = {}
        self.captains: List[Member] = []
        self.alpha_team = None
        self.beta_team = None
        self.alpha_draw = False
        self.beta_draw = False
        self.cancel_request = Teams.NONE
        self.proposed_server = None
        self.captain_pick_text = None
        self.suggested_teams = None
        self.suggested_teams_quality = None
        self.suggested_teams_accepted = Teams.NONE

        # for reporting match manually - !report_match
        if winner:
            self.players = list(players)
            self.pickup = pickup
            self.channel = pickup.channel.channel
            self.winner = winner
            self.alpha_team = self.players[0:len(self.players) // 2]
            self.beta_team = self.players[len(self.players) // 2:]
            self.ranked = pickup.channel.get_value('ranked', pickup)
            self.unpicked = []
            self.lastpick = None
            self.ranked_streaks = pickup.channel.cfg['ranked_streaks']
            if self.ranked:
                self.ranks = stats3.get_ranks(pickup.channel, [i.id for i in players])
                self.panzer_ranks = stats3.get_panzer_ranks(pickup.channel, [i.id for i in players])
            return

        # these values cannot be changed until match end, so we need to save them
        self.maxplayers = pickup.cfg['maxplayers']
        self.pick_teams = pickup.channel.get_value('pick_teams', pickup) if parties is None else "auto"
        self.require_ready = pickup.channel.get_value('require_ready', pickup)
        self.pick_order = pickup.cfg['pick_order']
        self.ranked = bool(pickup.channel.get_value('ranked', pickup) and self.pick_teams != 'no_teams')
        self.ranked_streaks = pickup.channel.cfg['ranked_streaks']
        self.map_pick_order = pickup.cfg['map_pick_order']
        self.best_of = pickup.cfg['best_of'] or 1
        maps = pickup.channel.get_value('maps', pickup)
        if maps is None:
            self.unpicked_maps = []
        else:
            self.unpicked_maps = [_map.strip() for _map in maps.split(",")]
        self.maps = self.unpicked_maps.copy()

        if self.ranked:
            self.ranks = stats3.get_ranks(pickup.channel, [i.id for i in players])
            self.panzer_ranks = stats3.get_panzer_ranks(pickup.channel, [i.id for i in players])
            self.players = list(sorted(players, key=lambda _p: self.ranks[_p.id], reverse=True))
            if self.pick_teams == 'manual' and 4 <= len(self.players) <= 12 and len(self.players) % 2 == 0:
                rating_dict = stats3.get_trueskill_ratings(pickup.channel, [user.id for user in self.players])
                suggested_teams, self.suggested_teams_quality = rating.get_balanced_teams(rating_dict)
                self.suggested_teams = rating.sort_players_in_teams(rating_dict, suggested_teams)

        else:
            self.players = list(players)
        self.captains_role = pickup.channel.get_value('captains_role', pickup)

        self.map = None
        maps = pickup.channel.get_value('maps', pickup)
        map_probabilities = pickup.cfg['map_probabilities']
        if map_probabilities and len(map_probabilities.split(",")) == len(maps.split(",")):
            map_prob_list = [float(prob.strip()) for prob in map_probabilities.split(",")]
        else:
            map_prob_list = None

        if maps:
            map_list = [_map.strip() for _map in maps.split(",")]

            if pickup.cfg['no_repeat_maps'] and self.map_pick_order != "mappref":  # if no_repeat_maps is set we remove the last played maps from the mappool
                prev_maps = [] if pickup.channel.lastgame_cache[7] is None else [_map.strip() for _map in
                                                                                 pickup.channel.lastgame_cache[7].split(
                                                                                     ",")]
                if pickup.channel.get_value('best_of', pickup) <= len(map_list) - len(prev_maps):
                    for _map in prev_maps:
                        if _map in map_list:
                            map_index = map_list.index(_map)
                            map_list.pop(map_index)
                            if map_prob_list:
                                map_prob_list.pop(map_index)
                            self.unpicked_maps.pop(map_index)

            if self.map_pick_order == "mappref":
                map_dict = {mapname: 0 for mapname in self.unpicked_maps}
                for player in players:
                    prefs = stats3.get_pref_maps(pickup.channel.channel.id, player.id)  # 1 - always, 2 - sometimes, 3 - never, 4 - dont know
                    if prefs:  # convert preferences to map:pref dictionary
                        ind_preferences = {a: b for a, b in [pref.split(":") for pref in prefs.split(",")]}
                        for map_name in map_dict:  # fill up preferences, if maps were rated. consider user to want to play them sometimes
                            if map_name not in ind_preferences:
                                ind_preferences[map_name] = "2"
                    else:
                        if map_prob_list:  # no preferences given, treat as average user by taking overall map probabilities
                            ind_preferences = {map_name: "1" if prob > 10 else "2" if prob > 0 else "3" for map_name, prob
                                              in zip(map_list, map_prob_list)}
                        else:  # no individual preferences given and no knowledge about general map preferences: consider to like every map moderately
                            ind_preferences = {map_name: "2" for map_name in map_dict}
                    # fill mapdict and mapdictfine with values representing preferences of all players in the match
                    for map_name in ind_preferences:
                        if map_name in map_dict:
                            if ind_preferences[map_name] == "1":
                                map_dict[map_name] += 1
                            if ind_preferences[map_name] == "2" or ind_preferences[map_name] not in ["1", "2", "3", "4"]:  # if there are nonsense entries, consider them as "sometimes":
                                map_dict[map_name] += 0.25
                            if ind_preferences[map_name] == "3":
                                map_dict[map_name] -= 1
                            if ind_preferences[map_name] == "4":  # dont consider players who dont know the map
                                map_dict[map_name] -= 0.25
                    if pickup.cfg['no_repeat_maps']:  # we reduce preference for last played maps if last game was recent
                        lastmaps, at = stats3.get_last_maps(pickup.channel.channel.id, player.nick or player.name)
                        if lastmaps:
                            for map_name in [_map.strip() for _map in lastmaps.split(",")]:
                                if map_name in map_dict:
                                    if time.time() - at < 10800:  # 3h
                                        map_dict[map_name] -= 1.5
                                    elif time.time() - at < 129600:  # 36h
                                        map_dict[map_name] -= 0.5

                # set map probabilities, use mapdict to get weights to be used as probabilities
                map_prob_list = [0 for _ in map_list]
                for i, map_name in enumerate(map_list):
                    map_prob_list[i] = map_dict[map_name]
                attempt = 0  # just in case not enough maps are in the pool and we get an endless loop (not sure its possible)
                while len([prob for prob in map_prob_list if
                           prob > 0]) < self.best_of:  # edge case: not enough maps are liked by the players
                    attempt += 1
                    map_prob_list = [prob + 0.25 for prob in map_prob_list]
                    if attempt > 1000:
                        break
                map_prob_list = [max(prob, 0) for prob in map_prob_list]

            if self.map_pick_order in [None, "mappref"] and self.best_of:
                if map_prob_list:
                    chosen_maps = []
                    for _ in range(self.best_of-1):
                        chosen_maps.append(random.choices(map_list, weights=map_prob_list, k=1)[0])
                        map_index = map_list.index(chosen_maps[-1])
                        map_list.pop(map_index)
                        map_prob_list.pop(map_index)
                    # the following ensures that one of the chosen maps is among the three most popular ones.
                    if len(map_prob_list) >= 3:
                        map_prob_list = [prob if prob >= sorted(map_prob_list)[-3] else 0 for prob in map_prob_list]
                    chosen_maps.append(random.choices(map_list, weights=map_prob_list, k=1)[0])
                    self.map = ', '.join(chosen_maps)

                else:
                    self.map = ', '.join(random.sample(self.unpicked_maps, self.best_of))

        # prepare working variables
        self.state = "none"  # none, waiting_ready, map_picking, teams_picking or waiting_report
        self.pickup = pickup
        self.channel = pickup.channel.channel
        self.winner = None
        self.unpicked = []
        self.panzers = []
        self.secondary_panzers = []
        self.lastpick = None  # fatkid
        self.beta_draw = False
        self.alpha_draw = False

        if self.require_ready:
            self.players_ready = set()

        emojis = pickup.channel.get_value('team_emojis', pickup)
        if emojis:
            self.alpha_icon, self.beta_icon = emojis.split(' ')
        else:
            self.alpha_icon, self.beta_icon = random.sample(team_emojis, 2)

        self.team_names = (pickup.channel.get_value('team_names', pickup) or "alpha beta").split(' ')

        # requests.post(client.web_url, json={'a':'b'})

        if parties is not None:
            self.alpha_team = parties[0].members.copy()
            self.beta_team = parties[1].members.copy()
            self.captains = [self.alpha_team[0], self.beta_team[0]]
            self.from_parties = True
            self.start_time = time.time()
            active_matches.append(self)
            self.next_state_with_notice()
            return

        if match_dict is not None:
            self.id = match_dict['id']
            self.state = match_dict['state']
            self.start_time = match_dict['start_time']
            self.alpha_team = match_dict['alpha_team']
            self.beta_team = match_dict['beta_team']
            self.server = match_dict['server']
            self.gtv_match_id = match_dict['gtv_match_id']
            active_matches.append(self)
            return

        pick_captains = self.pickup.channel.get_value('pick_captains', pickup)
        if pick_captains and len(players) > 2 and self.pick_teams != 'auto':
            if self.ranked:
                if pick_captains == 1:
                    candidates = sorted(self.players,
                                        key=lambda _p: [self.captains_role in [role.id for role in _p.roles],
                                                       self.ranks[_p.id]], reverse=True)
                    self.captains = [candidates[0], candidates[1]]
                elif pick_captains == 2:
                    candidates = sorted(self.players, key=lambda _p: [self.ranks[_p.id]], reverse=True)
                    i = random.randrange(len(candidates) - 1)
                    self.captains = random.sample([candidates[i], candidates[i + 1]], 2)
                elif pick_captains == 3:
                    if len(self.players) < 4:
                        self.captains = random.sample(self.players, 2)
                    else:
                        matches_count = stats3.get_matches_count(pickup.channel, [i.id for i in players])
                        candidates = sorted(self.players,
                                            key=lambda _p: [matches_count[_p.id] > 20, self.ranks[_p.id]], reverse=True)
                        selection_captain = random.randint(1, 4)

                        if selection_captain == 1:
                            self.captains = [candidates[0], candidates[1]]
                        elif selection_captain == 2:
                            self.captains = [candidates[1], candidates[0]]
                        elif selection_captain == 3:
                            self.captains = [candidates[2], candidates[3]]
                        else:
                            self.captains = [candidates[3], candidates[2]]
                elif pick_captains == 5:
                    self.captain_pick_text = ""
                    channel = self.pickup.channel

                    def get_captain_candidates(role_id):
                        return [_player for _player in self.players if
                                role_id in [role.id for role in _player.roles]]

                    role_ids = self.pickup.channel.get_cfg_extension('multiple_captain_roles_v1', pickup=pickup)
                    if role_ids is not None:
                        shuffle_role_ids = list(role_ids)
                        random.shuffle(shuffle_role_ids)
                        skipped_role_ids = []
                        candidates = []
                        while shuffle_role_ids:
                            role_id = shuffle_role_ids.pop()
                            candidates = get_captain_candidates(role_id)
                            if len(candidates) <= 1:
                                skipped_role_ids.append(role_id)
                            else:
                                self.captain_pick_text += f"Picking captains from role `{channel.role_name_from_id(role_id)}`\n"
                                break
                        if skipped_role_ids:
                            skipped_roles_text = ','.join([f"{channel.role_name_from_id(_role_id)}" for _role_id in skipped_role_ids])
                            self.captain_pick_text += f"Less than 2 captain candidates from roles `{skipped_roles_text}`\n"
                        if len(candidates) <= 1:
                            self.captain_pick_text += f"Picking captains from captains_role {channel.role_name_from_id(self.captains_role)}\n"
                            candidates = get_captain_candidates(self.captains_role)
                    else:
                        self.captain_pick_text += f"Picking captains from captains_role `{channel.role_name_from_id(self.captains_role)}`\n"
                        candidates = get_captain_candidates(self.captains_role)
                    if len(candidates) <= 1:
                        self.captain_pick_text += f"Only {len(candidates)} captain candidates, adding more by rating\n"
                        candidates += sorted([player for player in self.players if player not in candidates],
                                             key=lambda _p: [self.ranks[_p.id]], reverse=True)[0:2 - len(candidates)]
                    self.captains = random.sample(candidates, 2)

                elif pick_captains == 6:
                    try:
                        candidates = sorted([player for player in self.players if
                                             self.captains_role in [role.id for role in player.roles]],
                                            key=lambda _p: [self.ranks[_p.id]], reverse=True)
                        console.display("Match #{} candidates: {}".format(self.id, len(candidates)))
                        if len(candidates) <= 1:
                            candidates += sorted([player for player in self.players if player not in candidates],
                                                 key=lambda _p: [self.ranks[_p.id]], reverse=True)[0:2 - len(candidates)]
                        i = random.randrange(len(candidates) - 1)
                        self.captains = random.sample([candidates[i], candidates[i + 1]], 2)
                    except Exception as e:
                        PubobotException(str(e))
                else:  # pick_captains == 4:
                    self.captains = random.sample(self.players, 2)

            elif pick_captains == 1 and self.captains_role:
                self.captains = []
                candidates = list(filter(lambda x: self.captains_role in [role.id for role in x.roles], self.players))
                while len(self.captains) < 2:
                    if len(candidates):
                        p = random.choice(candidates)
                        self.captains.append(p)
                        candidates.remove(p)
                    else:
                        p = random.choice([x for x in self.players if x not in self.captains])
                        self.captains.append(p)
            else:
                self.captains = random.sample(self.players, 2)
        else:
            self.captains = None

        if len(players) > 2:
            if self.pick_teams == 'no_teams' or self.pick_teams is None:
                self.alpha_team = None
                self.beta_team = None

            elif self.pick_teams == 'manual':
                self.pick_step = 0
                self.unpicked = list(players)
                self.alpha_team = []
                self.beta_team = []
                if self.captains:
                    self.alpha_team.append(self.captains[0])
                    self.beta_team.append(self.captains[1])
                    self.unpicked.remove(self.captains[0])
                    self.unpicked.remove(self.captains[1])

            elif self.pick_teams in ['auto', 'autortcw']:
                self.create_teams()

        else:  # for 1v1 pickups
            self.pick_teams = 'auto'
            self.alpha_team = [self.players[0]]
            self.beta_team = [self.players[1]]
            self.captains = self.players

        # set state and start time
        self.start_time = time.time()
        active_matches.append(self)
        self.next_state_with_notice()

    def create_teams(self, sub=False):

        self.ranks = stats3.get_ranks(self.pickup.channel, [i.id for i in self.players])
        self.panzer_ranks = stats3.get_panzer_ranks(self.pickup.channel, [i.id for i in self.players])

        pick_captains = self.pickup.channel.get_value('pick_captains', self.pickup)
        self.alpha_team = []
        self.beta_team = []

        if self.ranked:
            if self.pick_teams == 'auto':  # form balanced teams by rank

                teamlen = int(len(self.players) / 2)
                perfect_rank = sum(self.ranks.values()) / 2
                best_diff = 10000
                best_team = None
                for team in combinations(self.players, teamlen):
                    rank = sum([self.ranks[i.id] for i in team])
                    if abs(perfect_rank - rank) < best_diff:
                        best_diff = abs(perfect_rank - rank)
                        best_team = list(team)

                self.alpha_team = best_team
                self.beta_team = list(filter(lambda i: i not in self.alpha_team, self.players))
                if pick_captains:
                    # sort by captains_role, then elo
                    self.captains = [
                        sorted(self.alpha_team, key=lambda _p: self.captains_role in [role.id for role in _p.roles],
                               reverse=True)[0],
                        sorted(self.beta_team, key=lambda _p: self.captains_role in [role.id for role in _p.roles],
                               reverse=True)[0]
                    ]

            if self.pick_teams == 'autortcw':  # form balanced teams by rank - slightly more advanced logic tuned to rtcw specifics
                prev_panzers = self.panzers
                self.panzers = []
                self.secondary_panzers = []
                notset = list(self.players)
                teamlen = int(len(self.players) / 2)

                panzers = sorted([i for i in self.players if
                                  self.pickup.channel.cfg['panzer_role'] in [role.id for role in i.roles]],
                                 key=lambda i: self.panzer_ranks[i.id], reverse=True)

                if len(panzers) >= 2:
                    # select panzers with adjacent elo (make it as close as possible for everyone to have an equal chance at panzering)
                    options = list(range(0, len(panzers), 2))
                    if len(panzers) % 2 == 0:
                        options = list(range(0, len(panzers), 2))
                    else:
                        options = list(range(0, len(panzers) - 1, 2))
                    randomindex = random.choice(options)
                    panz1 = panzers.pop(randomindex)
                    panz2 = panzers.pop(randomindex)
                    self.panzers = [panz1, panz2]
                    notset.remove(panz1)
                    notset.remove(panz2)
                    self.alpha_team.append(panz1)
                    self.beta_team.append(panz2)

                elif len(panzers) == 1:
                    panz1 = panzers.pop()
                    self.panzers.append(panz1)
                    self.alpha_team.append(panz1)
                    notset.remove(panz1)
                    notset.sort(key=lambda i: self.panzer_ranks[i.id] - self.ranks[i.id])
                    panz2 = notset.pop()
                    self.panzers.append(panz2)
                    self.secondary_panzers.append(panz2)
                    self.beta_team.append(panz2)

                elif len(panzers) == 0:
                    notset.sort(key=lambda i: self.panzer_ranks[i.id] - self.ranks[i.id])
                    panz1 = notset.pop()
                    panz2 = notset.pop()
                    self.panzers = [panz1, panz2]
                    self.secondary_panzers = [panz1, panz2]
                    self.alpha_team.append(panz1)
                    self.beta_team.append(panz2)

                if sub:
                    notset.extend(self.panzers)
                    panzers = [i for i in self.players if
                               self.pickup.channel.cfg['panzer_role'] in [role.id for role in i.roles]]
                    panz1 = prev_panzers[0] if prev_panzers[0] in panzers else None
                    panz2 = prev_panzers[1] if prev_panzers[1] in panzers else None

                    if panz1 is None:
                        if self.panzers[0] != panz2:
                            panz1 = self.panzers[0]
                        else:
                            panz1 = self.panzers[1]

                    if panz2 is None:
                        if self.panzers[0] != panz1:
                            panz2 = self.panzers[0]
                        else:
                            panz2 = self.panzers[1]

                    self.panzers = [panz1, panz2]
                    self.secondary_panzers = [panz for panz in self.panzers if panz not in panzers]
                    self.alpha_team = [panz1]
                    self.beta_team = [panz2]
                    notset = [player for player in notset if player not in self.panzers]

                acceptable_diff = 60  # try to find good teams that differ by no more than acceptable_diff rating points

                for player in self.panzers:
                    self.ranks[player.id] = self.panzer_ranks[
                        player.id]  # use panzer elo for panzer players in team balancing and for display
                perfect_rank = sum(self.ranks.values()) / 2

                best_score = 100000
                best_team = None
                alpharating = sum([self.ranks[i.id] for i in self.alpha_team])
                for team in combinations(notset, teamlen - len(self.alpha_team)):
                    rank = alpharating + sum([self.ranks[i.id] for i in team])
                    if abs(
                            perfect_rank - rank) < acceptable_diff // 2:  # (If alpha has x less points then perfect_rank, then beta has x more points. Hence we need to divide by 2 here)

                        alpha_candidate = self.alpha_team + list(team)
                        alpha_candidate.sort(key=lambda i: self.ranks[i.id])

                        beta_candidate = list(filter(lambda i: i not in alpha_candidate, self.players))
                        beta_candidate.sort(key=lambda i: self.ranks[i.id])

                        score = sum(abs(self.ranks[x.id] - self.ranks[y.id]) for x, y in
                                    zip(alpha_candidate, beta_candidate)) + abs(sum([self.ranks[i.id] for i in
                                                                                     alpha_candidate]) - perfect_rank)  # try to split players with similar elo
                        if score < best_score:
                            best_score = score
                            best_team = list(team)
                if not best_team:  # if somehow no teams within acceptable_diff can be found, select lowest possible elo difference
                    best_diff = 100000
                    for team in combinations(notset, teamlen - len(self.alpha_team)):
                        rank = alpharating + sum([self.ranks[i.id] for i in team])
                        if abs(perfect_rank - rank) < best_diff:
                            best_diff = abs(perfect_rank - rank)
                            best_team = list(team)

                self.alpha_team.extend(best_team)
                self.beta_team.extend(list(filter(lambda i: i not in self.alpha_team, notset)))

                # Sort by rating
                self.alpha_team.sort(key=lambda _p: self.ranks[_p.id], reverse=True)
                self.beta_team.sort(key=lambda _p: self.ranks[_p.id], reverse=True)

                if pick_captains and sub and self.captains: # keep captains if possible

                    if self.captains[0] in self.alpha_team and self.captains[1] in self.beta_team:
                        return

                    if self.captains[0] in self.beta_team and self.captains[1] in self.alpha_team:
                        self.captains = [self.captains[1], self.captains[0]]
                        return

                    if self.captains[0] in self.alpha_team or self.captains[1] in self.alpha_team:
                        orig_cap = self.captains[0] if self.captains[0] in self.alpha_team else self.captains[1]
                        caps = sorted([player for player in self.beta_team if
                                    self.captains_role in [role.id for role in player.roles]],
                                    key=lambda _p: abs(self.ranks[_p.id]-self.ranks[orig_cap.id]))
                        if caps:
                            self.captains = [orig_cap, caps[0]]
                        else:
                            self.captains = [orig_cap, random.choice(self.beta_team)]
                        return

                    if self.captains[0] in self.beta_team or self.captains[1] in self.beta_team:
                        orig_cap = self.captains[0] if self.captains[0] in self.beta_team else self.captains[1]
                        caps = sorted([player for player in self.alpha_team if
                                    self.captains_role in [role.id for role in player.roles]],
                                    key=lambda _p: abs(self.ranks[_p.id]-self.ranks[orig_cap.id]))
                        if caps:
                            self.captains = [caps[0], orig_cap]
                        else:
                            self.captains = [random.choice(self.beta_team), orig_cap]
                        return

                if pick_captains and pick_captains != 3:
                    # sort by captains_role
                    alphacaps = [player for player in self.alpha_team if
                                 self.captains_role in [role.id for role in player.roles]]
                    betacaps = [player for player in self.beta_team if
                                self.captains_role in [role.id for role in player.roles]]

                    if alphacaps:
                        alphacap = random.choice(alphacaps)
                    else:
                        alphacap = random.choice(self.alpha_team)
                    if betacaps:
                        betacap = random.choice(betacaps)
                    else:
                        betacap = random.choice(self.beta_team)
                    self.captains = [alphacap, betacap]

                if pick_captains == 3:
                    matches_count = stats3.get_matches_count(self.pickup.channel, [i.id for i in self.players])

                    candidates_alpha = sorted(self.alpha_team,
                                              key=lambda _p: [matches_count[_p.id] > 20, self.ranks[_p.id]],
                                              reverse=True)
                    candidates_beta = sorted(self.beta_team,
                                             key=lambda _p: [matches_count[_p.id] > 20, self.ranks[_p.id]],
                                             reverse=True)
                    candidates_count_alpha = len([p for p in candidates_alpha if matches_count[p.id] > 20])
                    candidates_count_beta = len([p for p in candidates_beta if matches_count[p.id] > 20])

                    if candidates_count_alpha:
                        candidates_alpha = candidates_alpha[0:candidates_count_alpha]
                    if candidates_count_beta:
                        candidates_beta = candidates_beta[0:candidates_count_beta]

                    if min(candidates_count_alpha, candidates_count_beta):
                        n = random.randint(0, min(candidates_count_alpha, candidates_count_beta) - 1)
                        self.captains = [candidates_alpha[n], candidates_beta[n]]
                    else:
                        self.captains = [candidates_alpha[0], candidates_beta[0]]

        # generate random teams
        else:
            unpicked = list(self.players)
            self.lastpick = unpicked[len(unpicked) - 1]
            while len(unpicked) > 1:
                self.alpha_team.append(unpicked.pop(random.randint(0, len(unpicked) - 1)))
                self.beta_team.append(unpicked.pop(random.randint(0, len(unpicked) - 1)))
            if len(unpicked):
                self.alpha_team.append(unpicked.pop(0))

    def __repr__(self):
        ago = datetime.timedelta(seconds=int(time.time() - self.start_time))
        return "Match#{} {} {} {} ago".format(self.id, self.pickup.channel.name, self.state, ago)

    def think(self, frametime):
        alive_time = frametime - self.start_time
        if self.state == "waiting_ready":
            if alive_time > self.require_ready:
                not_ready = list(filter(lambda x: x.id not in self.players_ready, self.players))
                self.players = list(filter(lambda x: x.id in self.players_ready, self.players))
                for player in not_ready:
                    stats3.noadd(self.channel.id, player.id, player.name, self.pickup.channel.cfg["default_bantime"],
                                 None, "not ready in time")
                    try:
                        party = Party.find_party_by_player(player, self.pickup.channel)
                        party.remove_player(player)
                    except NotInPartyException:
                        pass
                not_ready = ["<@{0}>".format(i.id) for i in not_ready]
                client.notice(self.channel,
                              "{0} was not ready in time!\r\nReverting **{1}** pickup to gathering state...".format(
                                  ", ".join(not_ready), self.pickup.name))
                self.state = 'cancelled'
                self.ready_fallback()
        elif alive_time > (self.pickup.channel.cfg['match_livetime'] or max_match_alive_time):
            client.notice(self.channel, "Match *({0})* has timed out.".format(str(self.id)))
            self.cancel_match()

    def send_pm_start_not_ready(self, message_string):
        """
        sends DM to players that haven't readied up yet
        """
        if self.state != 'waiting_ready':
            return
        not_ready_list = list(filter(lambda x: x.id not in self.players_ready, self.players))
        console.display(f"sending start PM to {len(not_ready_list)}/{len(self.players)} users.")
        for player in not_ready_list:
            client.private_reply(player, message_string)

    def _teams_to_str(self):
        if self.ranked and not self.pickup.channel.cfg['hide_ranks']:
            alpha_str = " ".join(
                ["`{0}`<@{1}>".format(utils.rating_to_icon(self.ranks[i.id], self.pickup.channel.cfg['custom_ranks']), i.id) for i in self.alpha_team])
            beta_str = " ".join(
                ["`{0}`<@{1}>".format(utils.rating_to_icon(self.ranks[i.id], self.pickup.channel.cfg['custom_ranks']), i.id) for i in self.beta_team])
            team_ratings = ['〈__{0}__〉'.format(sum([self.ranks[i.id] for i in team]) // len(team)) for team in
                            (self.alpha_team, self.beta_team)]
        else:
            alpha_str = " ".join(["<@{0}>".format(i.id) for i in self.alpha_team])
            beta_str = " ".join(["<@{0}>".format(i.id) for i in self.beta_team])
            team_ratings = ["", ""]

        if len(self.players) == 2:
            return "{0} :fire:**VERSUS**:fire: {1}".format(alpha_str, beta_str)
        else:
            return "{0} ❲{1}❳ {4}\r\n          :fire: **VERSUS** :fire:\r\n{2} ❲{3}❳ {5}".format(self.alpha_icon,
                                                                                                 alpha_str,
                                                                                                 self.beta_icon,
                                                                                                 beta_str,
                                                                                                 *team_ratings)

    def teams_picking_to_str(self):
        use_custom_ranks = self.pickup.channel.cfg['custom_ranks']
        hide_ranks = self.pickup.channel.cfg['hide_ranks']

        def team_to_str(team: List[Member], team_idx: int) -> str:
            if len(team):
                if self.ranked and not hide_ranks:
                    avg_rating = sum([self.ranks[i.id] for i in team]) // len(team)
                    return "❲{1}❳ 〈__{0}__〉".format(
                            avg_rating, " + ".join(
                                ["`{}{}`".format(
                                    utils.format_member_nick(i),
                                    utils.rating_to_icon(self.ranks[i.id], use_custom_ranks))
                                 for i in team]))
                else:
                    return "❲{0}❳".format(
                        " + ".join(["`{0}`".format(utils.format_member_nick(i)) for i in team]))
            else:
                return "❲{0}❳".format(self.team_names[team_idx])

        alpha_str = team_to_str(self.alpha_team, 0)
        beta_str = team_to_str(self.beta_team, 1)

        if self.ranked and not hide_ranks:
            unpicked_str = "\n".join(
                [" - `{0}{1}`".format(utils.format_member_nick(i), utils.rating_to_icon(self.ranks[i.id], use_custom_ranks)) for i
                 in sorted(self.unpicked, key=lambda p: self.ranks[p.id], reverse=True)])
        else:
            unpicked_str = "\n".join([" - `{0}`".format(utils.format_member_nick(i)) for i in self.unpicked])
        return f"{self.alpha_icon} {alpha_str}\n"\
               "          :fire:**VERSUS**:fire:\n"\
               f"{self.beta_icon} {beta_str}\n\n"\
               f"__Unpicked__:\n{unpicked_str}"

    def teams_picking_to_embed(self) -> Embed:
        use_custom_ranks = self.pickup.channel.cfg['custom_ranks']
        hide_ranks = self.pickup.channel.cfg['hide_ranks']
        member_info_dict = stats3.get_bulk_member_info([member.id for member in self.players], self.channel.guild.id)

        def team_to_str(team: List[Member]) -> str:
            return "\n".join([
                "`{}{}`{}".format(
                    utils.format_member_nick(i) if i.id not in member_info_dict else member_info_dict[i.id]['nick'],
                    utils.rating_to_icon(self.ranks[i.id], use_custom_ranks) if self.ranked and not hide_ranks else "",
                    f" :flag_{member_info_dict[i.id]['country']}:" if i.id in member_info_dict else ""
                )
                for i in team])

        embed = Embed()
        teams = (self.alpha_team, self.beta_team)
        icons = (self.alpha_icon, self.beta_icon)
        team_players = [team_to_str(team) for team in teams]
        for team_idx, team in enumerate(teams):
            if team_players[team_idx]:
                field_name = f"{icons[team_idx]} **{self.team_names[team_idx]}**"
                if self.ranked and not hide_ranks:
                    avg_rating = sum([self.ranks[i.id] for i in team]) // len(team)
                    field_name += f" `〈{avg_rating}〉`"
                embed.add_field(name=field_name, value=team_players[team_idx], inline=True)

        def format_member_with_info(member: Member) -> str:
            info = member_info_dict.get(member.id, None)
            info_str = ""
            nick = (member.nick or member.name).lower()
            if info is not None:
                if info['nick'].lower() not in nick and nick not in info["nick"].lower():
                    info_str += " " + info['nick']
                if info['country']:
                    info_str += f" :flag_{info['country']}:"
            rv = f"{member.mention} {info_str}"
            if self.ranked and not hide_ranks:
                rv = utils.rating_to_icon(self.ranks[member.id], use_custom_ranks) + rv
            return rv

        if len(self.unpicked):
            if self.pick_order:
                def format_pick(idx, pick):
                    return f"**{pick}**" if self.pick_step == idx else pick
                order = ''.join([format_pick(idx, pick) for idx, pick in enumerate(self.pick_order)])
                embed.add_field(name="Order:", value=order, inline=False)
            field_value = "\n".join([format_member_with_info(i) for i in self.unpicked])
            embed.add_field(name="Unpicked:", value=field_value, inline=False)
        return embed

    def teams_picking_to_view(self) -> View:
        view = View(timeout=10*60)

        async def button_pick_callback(interaction: Interaction):
            picker = interaction.user
            pick_target = self.channel.guild.get_member(int(interaction.data["custom_id"]))
            try:
                self.pickup.channel.pick_player(picker, pick_target, self)
                await interaction.response.edit_message(
                    content=f"{utils.format_member_nick(picker)} picked {utils.format_member_nick(pick_target)}",
                    view=None)
            except PubobotException as e:
                await interaction.response.send_message(e.with_mention(picker))

        for player in self.unpicked:
            nick = utils.format_member_nick(player)
            item = Button(label=nick, custom_id=str(player.id))
            item.callback = button_pick_callback
            view.add_item(item=item)
        return view

    def _add_embed_fields_to_start_msg(self, embed: Embed):

        ipstr = self.pickup.channel.get_value("startmsg", self.pickup)
        if self.ip is None:
            ip = self.pickup.channel.get_value("ip", self.pickup)
        else:
            ip = self.ip
        if self.password is None:
            password = self.pickup.channel.get_value("password", self.pickup)
        else:
            password = self.password
        ipstr = ipstr.replace("%ip%", ip or "").replace("%password%", password or "")
        embed.add_field(name="Message:", value=ipstr, inline=False)

        if self.map:
            embed.add_field(name="Maps:", value=f"**{self.map}**", inline=False)

        if self.gtv_match_id:
            embed.add_field(name="gtv:", value=gtvd.match_link(self.gtv_match_id), inline=False)

        for player in self.players:
            for activity in player.activities:
                if isinstance(activity, Streaming):
                    self.streaming_players[player.id] = activity
                    break

        if len(self.streaming_players):
            embed.add_field(name="Streams:", value='\n'.join(
                [f"<@{user_id}> {self.streaming_players[user_id].url}" for user_id in self.streaming_players]
            ), inline=False)
        embed.add_field(name="Started:", value=f"<t:{int(self.start_time)}:R>", inline=False)
        return embed

    def _players_to_str(self, players):
        return " ".join([player.mention for player in players])

    def startmsg_instant(self):
        if self.ranked:
            if self.server is None:
                self.pick_server()
            title = f"__*({str(self.id)})* **{self.pickup.name}** pickup has been started!__"
        else:
            title = f"__**{self.pickup.name}** pickup has been started!__"

        if self.beta_team and self.alpha_team:
            embed = self.teams_picking_to_embed()
        else:
            embed = Embed()

        embed.title = title

        self._add_embed_fields_to_start_msg(embed)
        if len(self.panzers) == 2:
            if self.panzers[0] in self.secondary_panzers:
                panzermsg = "No Alpha Panzer found - suggested: <@{0}>, ".format(self.panzers[0].id)
            else:
                panzermsg = "<@{0}>, ".format(self.panzers[0].id)
            if self.panzers[1] in self.secondary_panzers:
                panzermsg += "No Beta Panzer found - suggested: <@{0}>.".format(self.panzers[1].id)
            else:
                panzermsg += "<@{0}>.".format(self.panzers[1].id)

            embed.add_field(name="Panzers", value=panzermsg, inline=False)

        if self.captains and len(self.players) > 2 and not hasattr(self, 'from_parties'):
            embed.add_field(name="Captains", value=f"<@{self.captains[0].id}> <@{self.captains[1].id}>", inline=False)

        return self._players_to_str(self.players), embed, None

    def startmsg_teams_picking_start(self):
        startmsg = "__*({0})* **{1}** pickup started!__\n".format(str(self.id), self.pickup.name)
        if self.map_pick_order == "mappref":
            if self.map and self.best_of > 1:
                startmsg += "Maps: **{0}**.\r\n".format(self.map)
            elif self.map:
                startmsg += "Map: **{0}**.\r\n".format(self.map)
        if self.captain_pick_text:
            startmsg += self.captain_pick_text
        if self.captains:
            startmsg += "<@{0}> and <@{1}> start picking teams.\n".format(self.captains[0].id,
                                                                                       self.captains[1].id)
        else:
            startmsg += self._players_to_str(
                self.players) + "please use '!capfor **{0}**' and start picking teams.\n".format(
                '**/**'.join(self.team_names))

        team_str, embed, view = self.pickup.channel.teams_picking_to_str_or_embed_and_view(self)
        if team_str:
            startmsg += team_str

        if self.pick_order:
            if self.pick_order[0] == 'a':
                if self.captains:
                    first = "<@{0}>".format(self.captains[0].id)
                else:
                    first = '**' + self.team_names[0] + '**'
            else:
                if self.captains:
                    first = "<@{0}>".format(self.captains[1].id)
                else:
                    first = '**' + self.team_names[1] + '**'
            startmsg += "\r\n{0} picks first!".format(first)

        if self.suggested_teams:
            startmsg += "\n" + " and ".join(captain.mention for captain in self.suggested_teams_captains()) +\
                        " can accept suggested teams"
            suggested_teams_members = tuple([
                    next(player for player in self.players if player.id == user_id) for user_id in
                    team] for team in self.suggested_teams)
            embed.add_field(name='Suggested teams',
                            value='\n'.join(utils.member_list(team, ' ') for team in suggested_teams_members) +
                            "\nQuality: " + f"{self.suggested_teams_quality * 100:.2f}%",
                            inline=False)
            button = Button(label="Accept suggested teams", style=ButtonStyle.green)

            async def button_callback(interaction: Interaction):
                try:
                    if self.accept_suggested_teams(interaction.user):
                        await interaction.response.send_message('Suggested teams accepted.')
                    else:
                        other_captain_idx = (self.suggested_teams_accepted.value + 1) % 2
                        other_captain = self.suggested_teams_captains()[other_captain_idx]
                        await interaction.response.send_message(f"Waiting for {other_captain.mention} to accept.")
                except PubobotException as e:
                    client.private_reply(interaction.user, e.with_mention(interaction.user))
            button.callback = button_callback
            view.add_item(button)

        return startmsg, embed, view

    def startmsg_teams_picking_finish(self):
        if self.ranked and self.server is None:
            self.pick_server()

        embed = self.teams_picking_to_embed()
        embed.title = "TEAMS READY"
        embed.description = ""
        self.start_time = time.time()
        self._add_embed_fields_to_start_msg(embed)
        return self._players_to_str(self.players), embed, None

    def map_picking_message(self):
        # picked_map = self.unpicked_maps[int(map_idx-1)]
        if self.alpha_team is None or self.beta_team is None:
            alpha_captain, beta_captain = random.sample(self.players, 2)
            self.alpha_team = [alpha_captain]
            self.beta_team = [beta_captain]
        else:
            alpha_captain = self.alpha_team[0]
            beta_captain = self.beta_team[0]
        if self.map_pick_order[0] == 'a':
            first = "<@{0}> bans".format(alpha_captain.id)
        elif self.map_pick_order[0] == 'b':
            first = "<@{0}> bans".format(beta_captain.id)
        elif self.map_pick_order[0] == 'A':
            first = "<@{0}> picks".format(alpha_captain.id)
        elif self.map_pick_order[0] == 'B':
            first = "<@{0}> picks".format(beta_captain.id)
        else:
            first = "oops"
        msg = "\n{0} map!\n".format(first)

        # maps = "[1] Adlernest [2] frostbite ..."

        def print_map_diff(idx, map):
            if map in self.unpicked_maps:
                return "[{}] {}".format(idx + 1, map)
            elif map in self.picked_maps:
                return "+   {}".format(map)
            else:
                return "-   {}".format(map)

        maps = "\n".join([print_map_diff(idx, map) for idx, map in enumerate(self.maps)])
        msg += "```diff\n{}```".format(maps)

        view = View(timeout=3 * 60)

        async def button_callback(interaction: Interaction):
            picker = interaction.user
            map_idx = int(interaction.data["custom_id"])
            try:
                self.map_pick(picker, map_idx)
                map_name = self.maps[map_idx - 1]
                action = "picked" if map_name in self.picked_maps else "banned"
                await interaction.response.edit_message(
                    content=f"{utils.format_member_nick(picker)} {action} {map_name}",
                    view=None)
            except PubobotException as e:
                await interaction.response.send_message(e.with_mention(picker))

        for idx, map in enumerate(self.maps):
            if map in self.unpicked_maps:
                item = Button(label=map, custom_id=str(idx + 1))
                item.callback = button_callback
                view.add_item(item=item)
        return msg, None, view

    def suggested_teams_captains(self) -> Optional[Tuple[Member, Member]]:
        if self.suggested_teams is None:
            return None
        return next(player for player in self.players if player.id == self.suggested_teams[0][0]), next(player for player in self.players if player.id == self.suggested_teams[1][0]),

    def accept_suggested_teams(self, member: Member) -> bool:
        """
        return true if both captains accepted
        """
        if self.state != 'teams_picking':
            raise PubobotException("Match not in picking stage.")
        try:
            captain_index = self.suggested_teams_captains().index(member)
        except ValueError:
            raise PubobotException("You must be captain of the suggested team to accept suggested teams.")
        other_captain_index = (captain_index + 1) % 2
        if self.suggested_teams_accepted == Teams.NONE:
            self.suggested_teams_accepted = Teams(captain_index)
            return False
        elif self.suggested_teams_accepted.value == other_captain_index:
            try:
                self.alpha_team, self.beta_team = tuple([
                    next(player for player in self.players if player.id == user_id) for user_id in
                    team] for team in self.suggested_teams)
            except RuntimeError:
                raise PubobotException("Players in suggested teams don't match with players in match")
            self.captains = [self.alpha_team[0], self.beta_team[0]]
            self.unpicked = []
            self.next_state_with_notice()
            return True
        else:
            raise PubobotException("You already accepted the suggested teams.")

    def next_state(self):
        return_message = None, None, None
        if self.state == 'none':
            if self.require_ready:
                self.state = 'waiting_ready'
                return_message = self.ready_message()
                if len(self.secondary_panzers):
                    client.notice(self.channel, 'Less than two players in the queue have the panzer role. Type !panzer to be considered.')
            elif self.pick_teams == 'manual':
                return_message = self.startmsg_teams_picking_start()
                # split into 2 messages so that player picking won't replace start message
                client.notice(self.channel, return_message[0])
                return_message = None, return_message[1], return_message[2]
                self.state = 'teams_picking'
            elif self.map_pick_order is not None and self.map_pick_order != "mappref" and len(
                    self.unpicked_maps) > 1:
                if self.alpha_team is not None and self.beta_team is not None:
                    client.notice(self.channel, None, embed=self.teams_picking_to_embed())
                return_message = self.map_picking_message()
                self.state = 'map_picking'
            elif self.ranked:
                return_message = self.startmsg_instant()
                self.state = 'waiting_report'
            else:
                return_message = self.startmsg_instant()
                self.finish_match()

        elif self.state == 'waiting_ready':
            if self.require_ready:
                for i in self.players:
                    if i in allowoffline:
                        allowoffline.remove(i)
                    if i.id in scheduler.tasks.keys():
                        scheduler.cancel_task(i.id)
            if self.pick_teams == 'manual':
                return_message = self.startmsg_teams_picking_start()
                # split into 2 messages so that player picking won't replace start message
                client.notice(self.channel, return_message[0])
                return_message = None, return_message[1], return_message[2]
                self.state = 'teams_picking'
            elif self.map_pick_order is not None and self.map_pick_order != "mappref":
                if self.alpha_team is not None and self.beta_team is not None:
                    client.notice(self.channel, None, embed=self.teams_picking_to_embed())
                return_message = self.map_picking_message()
                self.state = 'map_picking'
            elif self.ranked:
                if self.pick_teams in ['auto', 'autortcw']:
                    self.create_teams() # calculate teams at the end so people have a chance to set roles and get seeded
                return_message = self.startmsg_instant()
                self.state = 'waiting_report'
            else:
                return_message = self.startmsg_instant()
                self.finish_match()

        elif self.state == 'teams_picking':
            if self.map_pick_order is not None and self.map_pick_order != "mappref":
                if self.alpha_team is not None and self.beta_team is not None:
                    client.notice(self.channel, None, embed=self.teams_picking_to_embed())
                return_message = self.map_picking_message()
                self.state = 'map_picking'
            elif self.ranked:
                return_message = self.startmsg_teams_picking_finish()
                self.state = 'waiting_report'
            else:
                return_message = self.startmsg_teams_picking_finish()
                self.finish_match()

        elif self.state == 'waiting_report':
            self.finish_match()

        elif self.state == 'map_picking':
            if self.ranked:
                return_message = self.startmsg_teams_picking_finish()
                self.state = 'waiting_report'
            else:
                return_message = self.startmsg_teams_picking_finish()
                self.finish_match()
        events.match_change(self)
        self.update_status_message()
        return return_message

    def next_state_with_notice(self):
        msg, embed, view = self.next_state()
        client.notice(self.channel, msg, embed, view=view)

    def finish_match(self, time=None):
        new_ranks, alpha_win_probability = stats3.register_pickup(self, time)
        self.pickup.channel.lastgame_cache = stats3.lastgame(self.pickup.channel.id)
        if time is None:
            active_matches.remove(self)
        if time or self.state == 'waiting_report':
            client.notice(self.channel, "Match *({0})* has been finished.".format(self.id))
            if len(new_ranks) and not self.pickup.channel.cfg['hide_ranks']:
                if alpha_win_probability:
                    alpha_team_name = self.team_names[0] if hasattr(self, 'team_names') else 'alpha'
                    win_probability_str = f"\nteam {alpha_team_name} had {alpha_win_probability*100:.1f}% chance of winning"
                else:
                    win_probability_str = ""
                summary = [
                    [
                        new_ranks[i][0],
                        self.ranks[i],
                        # utils.rating_to_icon(self.ranks[i],self.pickup.channel.cfg['custom_ranks'])[1:-1],
                        new_ranks[i][1],
                        # utils.rating_to_icon(new_ranks[i][1],self.pickup.channel.cfg['custom_ranks'])[1:-1],
                        "{:+d}".format(new_ranks[i][1] - self.ranks[i])
                    ] for i in new_ranks.keys()]
                client.notice(self.channel, "```" + tabulate(
                    summary,
                    headers=["nickname", "before", "after", "diff"]
                ) + "```" + win_probability_str)
                rankups = []
                for i in new_ranks.keys():
                    if utils.rating_to_icon(self.ranks[i],self.pickup.channel.cfg['custom_ranks']) != utils.rating_to_icon(new_ranks[i][1],self.pickup.channel.cfg['custom_ranks']) and int(self.ranks[i]) < int(new_ranks[i][1]):
                        rankups.append(new_ranks[i][0]+" has ranked up from "+ utils.rating_to_icon(self.ranks[i],self.pickup.channel.cfg['custom_ranks'],"long") +
                                       " " + utils.rating_to_icon(self.ranks[i],self.pickup.channel.cfg['custom_ranks']) +
                                       " to " + utils.rating_to_icon(new_ranks[i][1],self.pickup.channel.cfg['custom_ranks'],"long") +
                                       " " + utils.rating_to_icon(new_ranks[i][1],self.pickup.channel.cfg['custom_ranks']))
                if rankups and self.pickup.channel.cfg['custom_ranks']:
                    client.notice(self.channel, "```" + "\n".join(rankups) + "```")
            if self.ranked and self.winner and self.gtv_match_id is not None:
                gtvd.finish_match(
                    self.gtv_match_id,
                    1 if (self.winner == 'alpha' or self.winner == 'draw') else 0,
                    1 if (self.winner == 'beta' or self.winner == 'draw') else 0
                )
        self.state = "finished"
        self.re_queue_players()

    def re_queue_players(self):
        for channel in self.re_queue:
            change = False
            for pickup in self.re_queue[channel]:
                re_queue = list(self.re_queue[channel][pickup])
                if pickup.channel.cfg['requeue_random']:
                    random.shuffle(re_queue)
                for player in re_queue:
                    try:
                        pickup.channel.add_player(player, [pickup.name], False)
                        change = True
                    except PubobotException as e:
                        client.reply(pickup.channel.channel, player, str(e))
            # update topic only on other channels to not update status message twice
            if change and channel != self.pickup.channel:
                channel.update_topic()

    def format_status_message(self):
        matches_msg = ""
        for match in active_matches:
            if match.pickup.channel.id == self.pickup.channel.id:
                matches_msg += "{} | {}".format(match.pickup.name, match.state)
        return matches_msg

    def update_status_message(self):
        self.pickup.channel.update_status_message('matches_msg', '**Matches**', self.format_status_message())

    def cancel_match(self):
        client.notice(self.channel, "{0} your match has been cancelled.".format(
            ', '.join(["<@{0}>".format(i.id) for i in self.players])))
        if self.gtv_match_id:
            gtvd.cancel_match(self.gtv_match_id)
        active_matches.remove(self)
        self.re_queue_players()
        self.update_status_message()

    def cancel_match_freeze(self, player):
        client.notice(self.channel, "**{0}** has been removed!\r\nReverting **{1}** to gathering state...".format(
            player.nick or player.name, self.pickup.name))
        if self.gtv_match_id:
            gtvd.cancel_match(self.gtv_match_id)
        self.players.remove(player)
        self.ready_fallback()

    def draw_match(self):
        # client.notice(self.channel, "Match {0} finished. Your match has ended in a draw.".format(self.id))
        self.winner = 'draw'
        self.next_state_with_notice()

    def map_pick(self, member, map_idx):
        try:
            try:
                captains = [self.alpha_team[0], self.beta_team[0]]
            except IndexError:
                PubobotException("Error: captains not set")
            else:
                map_idx = int(map_idx) - 1
                cap_idx = 'ab'.find(self.map_pick_order[0].lower())
                if member.id != captains[cap_idx].id:
                    raise PubobotException("not your turn")
                if self.maps[map_idx] not in self.unpicked_maps:
                    raise PubobotException("map already picked or banned")
                if self.map_pick_order[0].isupper():
                    self.picked_maps.append(self.maps[map_idx])
                self.unpicked_maps.remove(self.maps[map_idx])
                self.map_pick_order = self.map_pick_order[1:]
                events.map_pick(self)
        except IndexError:
            raise PubobotException("wrong index")
        except ValueError:
            raise PubobotException("not an integer")

        if len(self.unpicked_maps) == 1:
            self.picked_maps.append(self.unpicked_maps[0])
            # client.notice(self.channel, "picked maps: {}".format(", ".join(self.picked_maps)))
            self.map = ", ".join(self.picked_maps)
            self.next_state_with_notice()
        elif len(self.map_pick_order) == 0:
            self.picked_maps += random.sample(self.unpicked_maps, min(
                len(self.unpicked_maps),
                max(0, self.best_of - len(self.picked_maps))
            ))
            self.map = ", ".join(self.picked_maps)
            self.next_state_with_notice()
        else:
            msg, embed, view = self.map_picking_message()
            client.notice(self.channel, msg, embed, view)

    def pick_server(self):
        try:
            server = game_server.pick_server(self.pickup.channel, active_matches, self.pickup)
        except game_server.PubobotNoGameServersConfiguredException:
            pass
        except PubobotException as e:
            client.notice(self.channel, str(e))
        else:
            self.server = server
            self.ip = server['ip']
            try:
                pw = server['pw']
            except KeyError:
                pw = ""
            self.password = pw
            try:
                ettv_pw = loads(server['info'])['ettv_pw']
            except JSONDecodeError:
                ettv_pw = None
            except KeyError:
                ettv_pw = None
            self.ettv_password = ettv_pw
            if ettv_pw is not None:
                gtvd.create_match(self)

    def ready_message_content(self):
        not_ready = list(filter(lambda i: i.id not in self.players_ready, self.players))
        abort_timestamp = int(self.require_ready + self.start_time)
        return f"__*({self.id})* **{self.pickup.name}** pickup is now on waiting ready state!__" + \
               (f"\nWaiting on: {self._players_to_str(not_ready)}\nAborting <t:{abort_timestamp}:R>" if not_ready else "")

    def ready_message(self):
        view = View(timeout=None)

        def build_ready_callback(ready: bool, user_override=None):
            async def ready_callback(interaction: Interaction):
                user = user_override if user_override is not None else interaction.user
                if self.state != 'waiting_ready':
                    return
                try:
                    member = next(_player for _player in self.players if _player.id == user.id)
                except StopIteration:
                    return
                is_ready = user.id in self.players_ready
                try:
                    if ready:
                        if not is_ready:
                            self.players_ready.add(user.id)
                            if len(self.players_ready) == len(self.players):
                                msg, embed, ready_view = self.next_state()
                                await interaction.response.edit_message(content="All players ready", view=None)
                                await self.channel.send(msg, embed=embed, view=ready_view)
                                return
                    else:
                        # TODO: should also replace message on ready timeout, but need to store message first
                        await interaction.response.edit_message(content=self.ready_notready(member), embed=None, view=View())
                        return

                    events.set_ready(self)

                    await interaction.response.edit_message(
                        content=self.ready_message_content(),
                        view=view)
                except PubobotException as e:
                    await interaction.response.send_message(e.with_mention(user))
            return ready_callback

        item = Button(label='Ready', style=ButtonStyle.green)
        item.callback = build_ready_callback(True)
        view.add_item(item=item)

        if os.getenv('DEBUG'):
            for player in self.players:
                user = client.c.get_user(player.id)
                item = Button(label=f'!R - {player.name}', style=ButtonStyle.green)
                item.callback = build_ready_callback(True, user)
                view.add_item(item=item)

            for player in self.players:
                user = client.c.get_user(player.id)
                item = Button(label=f'!NR - {player.name}', style=ButtonStyle.red)
                item.callback = build_ready_callback(False, user)
                view.add_item(item=item)

        async def noop_callback(interaction: Interaction):
            await interaction.response.defer()

        item = Button(label=' - ', style=ButtonStyle.gray)
        item.callback = noop_callback
        view.add_item(item=item)

        item = Button(label='Not ready', style=ButtonStyle.red)
        item.callback = build_ready_callback(False)
        view.add_item(item=item)
        return self.ready_message_content(), None, view

    def ready_notready(self, user):
        self.state = 'cancelled'
        events.match_change(self)
        self.players.remove(user)
        stats3.noadd(self.channel.id, user.id, user.name, self.pickup.channel.cfg["default_bantime"], None,
                     "aborted game")
        try:
            party = Party.find_party_by_player(user, self.pickup.channel)
            party.remove_player(user)
        except NotInPartyException:
            pass
        self.ready_fallback()
        return f"**{user.nick or user.name}** is not ready!\nReverting **{self.pickup.name}** to gathering state..."

    def ready_fallback(self):  # if ready event failed
        if not hasattr(self, 'from_parties'):
            newplayers = list(self.pickup.players)
            self.pickup.players = list(self.players)
            while len(self.pickup.players) < self.pickup.cfg['maxplayers'] and len(newplayers):
                self.pickup.players.append(newplayers.pop(0))
            if len(self.pickup.players) == self.pickup.cfg['maxplayers']:
                self.pickup.channel.start_pickup(self.pickup)
                self.pickup.players = newplayers
            if len(self.pickup.players):
                active_pickups.append(self.pickup)
            events.pickup_change(self.pickup)
        active_matches.remove(self)
        self.re_queue_players()
        self.pickup.channel.update_topic()
        self.update_status_message()


    def set_server(self, server):
        self.server = server
        self.ip = server['ip']
        self.password = server['pw']

        mentions = ', '.join(["<@{0}>".format(i.id) for i in self.players])
        ip = server['ip']
        pw = server['pw']
        client.notice(self.channel, f"{mentions} your match server was changed to: {ip};password {pw}")
        if self.gtv_match_id is not None:
            gtvd.update_match_server(self)


class Pickup:

    players: List[Member]
    name: str

    def __init__(self, channel, cfg):
        self.players = []
        self.name = cfg['pickup_name']
        self.channel = channel
        self.cfg = cfg

    def __repr__(self):
        return f"<Pickup {self.channel} {self.name}>"


def _match_and_player_by_player_id(member_id):
    for match in active_matches:
        for player in match.players:
            if member_id == player.id:
                return match, player
    return None, None


class Channel:

    def __init__(self, channel, cfg):
        self.channel = channel
        self.id = channel.id
        self.name = "{0}>{1}".format(channel.guild.name, channel.name)
        self.guild = channel.guild
        self.cfg = cfg
        self.update_channel_config('channel_name', channel.name)
        self.update_channel_config('server_name', channel.guild.name)
        if not self.cfg['startmsg']:
            self.update_channel_config('startmsg', "Please connect to steam://connect/%ip%/%password%")
            self.update_channel_config('submsg',
                                       "%promotion_role% SUB NEEDED @ **%pickup_name%**. Please connect to steam://connect/%ip%/%password%")
        self.oldtime = 0
        self.pickups = []
        self.init_pickups()
        self.pickup_groups = stats3.get_pickup_groups(self.id)
        self.lastgame_cache = stats3.lastgame(self.id)
        self.lastgame_pickup = None
        self.old_topic = None
        self.to_remove = []  # players
        self.active_parties = []
        self.last_clear_rating_message_created_at = None
        self.status_messages = []
        self.status_message_dict = {}  # {'pickups': '#local [2v2 (3/4)]', 'matches': '2v2 | teams_picking'}
        self.scheduled_adds = []

    def __repr__(self):
        return f"<Channel {self.name}>"

    def init_pickups(self):
        pickups = stats3.get_pickups(self.id)
        for i in pickups:
            try:
                self.pickups.append(Pickup(self, i))
            except Exception as e:
                console.display(
                    "ERROR| Failed to init a pickup of channel {0}({1}) @ {2}.".format(self.name, self.id, str(e)))

    def start_pickup(self, pickup, parties=None, pickup_party_players=None):
        if not parties and len(pickup.players) < 2:
            client.notice(self.channel, "Pickup must have atleast 2 players added to start...")
            return
        if parties:
            players = parties[0].members + parties[1].members
        elif pickup_party_players is None:
            players = list(pickup.players)
        else:
            players = pickup_party_players
        affected_channels = list()

        pmsg = self.get_value('start_pm_msg', pickup)
        if pmsg:
            pmsg = pmsg.replace("%channel%", "<#{0}>".format(self.id))
            pmsg = pmsg.replace("%pickup_name%", pickup.name)
            pmsg = pmsg.replace("%ip%", self.get_value('ip', pickup) or "")
            pmsg = pmsg.replace("%password%", self.get_value('password', pickup) or "")
            require_ready = pickup.channel.get_value('require_ready', pickup)
            if require_ready:
                pmsg += "\r You have {} to ready up".format(
                    datetime.timedelta(seconds=require_ready)
                )
                bantime = self.cfg['default_bantime']
                if bantime:
                    pmsg += ", otherwise you get banned for {}".format(
                        datetime.timedelta(seconds=bantime)
                    )
        for i in players:
            if not self.get_value("require_ready", pickup):
                if i in allowoffline:
                    allowoffline.remove(i)
                if i.id in scheduler.tasks.keys():
                    scheduler.cancel_task(i.id)

            for pu in (pu for pu in list(active_pickups) if i.id in [x.id for x in pu.players]):
                pu.players.remove(i)
                events.pickup_change(pu)
                if not len(pu.players):
                    active_pickups.remove(pu)
                if pu.channel != self:
                    if i not in pu.channel.to_remove:
                        pu.channel.to_remove.append(i)
                    if pu.channel not in affected_channels:
                        affected_channels.append(pu.channel)

        for i in affected_channels:
            client.notice(i.channel, "{0} was removed from all pickups! (pickup started on another channel)".format(
                ", ".join(["**{0}**".format(i.nick or i.name) for i in i.to_remove])))
            i.to_remove = []
            i.update_topic()

        # console.display("DEBUG| active_pickups: {0}".format(str([i.name for i in active_pickups])))
        # console.display("DEBUG| allowoffline: {0}".format(str([i.name for i in allowoffline])))
        if not parties and not pickup_party_players:
            pickup.players = []
        self.update_topic(False)
        match = Match(pickup, players, None, parties)
        if pmsg:
            if match.state != 'waiting_ready':
                for player in players:
                    client.private_reply(player, pmsg)
            else:
                def send_pm_start_not_ready_callback():
                    match.send_pm_start_not_ready(pmsg)
                scheduler.add_task(f'send_pm_start_{match.id}', 15, send_pm_start_not_ready_callback)
        self.lastgame_pickup = pickup

    def get_member_access_level(self, member):
        role_ids = [i.id for i in member.roles]
        if self.cfg['admin_role'] in role_ids or \
                member.id == self.cfg['admin_id'] or \
                self.channel.permissions_for(member).administrator or \
                (hasattr(client.client_config, 'ADMIN_IDS') and member.id in client.client_config.ADMIN_IDS):
            access_level = 2
        elif self.cfg['moderator_role'] in role_ids:
            access_level = 1
        else:
            access_level = 0
        return access_level

    async def process_message(self, msg: Message):
        member = msg.author
        msg_tup = msg.content.split()
        lower = [i.lower() for i in msg_tup]
        msg_len = len(lower)

        access_level = self.get_member_access_level(member)

        if re.match(r"^\+..", lower[0]):
            lower[0] = lower[0].lstrip(":+")
            self.add_player(member, lower[0:msg_len])
            return

        elif lower[0] == "++":
            self.add_player(member, [])
            return

        elif re.match(r"^-..", lower[0]):
            lower[0] = lower[0].lstrip(":-")
            self.remove_player(member, lower[0:msg_len])
            return

        elif lower[0] == "--":
            self.remove_player(member, [])
            return

        prefix, lower[0] = lower[0][0], lower[0][1:]
        if prefix == self.cfg["prefix"]:
            if lower[0] in self.cfg["disallowed_cmds"] and access_level < 2:
                await msg.add_reaction('🚫')
                return

            console.display(
                "CHAT| {0}>{1}>{2}: {3}".format(msg.guild, msg.channel, msg.author.display_name, msg.content))
            with start_transaction(op="command", name=lower[0]):
                if lower[0] in ["add", "j"]:
                    self.add_player(member, lower[1:msg_len])

                if lower[0] in ["add_in"]:
                    self.add_in(member, lower[1:])

                elif lower[0] in ["remove", "l"]:
                    self.remove_player(member, lower[1:msg_len])

                elif lower[0] in ["expire", "remove_in"]:
                    self.expire(member, lower[1:msg_len])

                elif lower[0] == "default_expire":
                    self.default_expire(member, lower[1:msg_len])

                elif lower[0] == "allowoffline" or lower[0] == "ao":
                    self.switch_allowoffline(member)

                elif lower[0] == "add_player":
                    self.add_players(member, lower[1], msg.mentions, access_level)

                elif lower[0] == "remove_player":
                    self.remove_players(member, msg.mentions, access_level)

                elif lower[0] == "web":
                    if hasattr(client.client_config, 'PUBLIC_WEB_URL'):
                        client.notice(self.channel, client.client_config.PUBLIC_WEB_URL)
                    else:
                        client.notice(self.channel, "public web interface url not configured")

                elif lower[0] == "who":
                    self.who(member, lower[1:msg_len])

                elif lower[0] in ["who_all", "wa"]:
                    self.who_all()

                elif lower[0] in ["who_scheduled", "ws"]:
                    await self.who_scheduled(msg)

                elif lower[0] == "start":
                    self.user_start_pickup(member, lower[1:msg_len], access_level)

                elif lower[0] == "pickups":
                    self.reply_pickups(member)

                elif lower[0] == "promote":
                    await self.promote_pickup(member, lower[1:2])

                elif lower[0] == "subscribe":
                    await self.subscribe(member, lower[1:msg_len], False)

                elif lower[0] == "unsubscribe":
                    await self.subscribe(member, lower[1:msg_len], True)

                elif lower[0] == "lastgame":
                    self.lastgame(member, msg_tup[1:msg_len])

                elif lower[0] == "sub":
                    await self.sub_request(member)

                elif lower[0] in ["cointoss", "ct"]:
                    self.cointoss(member, lower[1:2])

                elif lower[0] in ["pick", "p"]:
                    if len(msg.mentions) == 0:
                        client.reply(self.channel, member, "You must specify a highlight(s)!")
                        return
                    try:
                        for target in msg.mentions:
                            self.pick_player(member, target)
                    except PubobotException as e:
                        client.reply(self.channel, member, str(e))

                elif lower[0] == "put":
                    self.put_player(member, msg.mentions, lower[-1], access_level)

                elif lower[0] == "set_panzers":
                    await self.set_panzers(member, lower[1:3], access_level)

                elif lower[0] == "pick_captains":
                    await self.pick_captains(member, lower[1:3], access_level)

                elif lower[0] == "capfor":
                    self.capfor(member, lower[1:2])

                elif lower[0] == "subfor":
                    await self.subfor(member, lower[1:4], access_level)

                elif lower[0] in ["redo", "redue"]:
                    self.redo_teams(member, lower[1:msg_len], access_level)

                elif lower[0] == "teams":
                    self.print_teams(member)

                elif lower[0] == "matches":
                    self.get_matches()

                elif lower[0] in ["cancel_match", "cm"]:
                    self.cancel_match(member, lower[1:msg_len], msg.mentions, access_level)

                elif lower[0] in ["reportwin", "rw"]:
                    self.report_match(member, args=lower[1:3], access_level=access_level)

                elif lower[0] in ["reportlose", "rl"]:
                    self.report_match(member)

                elif lower[0] in ["reportdraw", "draw", "rd"]:
                    self.draw_match(member)

                elif lower[0] in ["reportcancel", "rc"]:
                    self.report_match_cancel(member)

                elif lower[0] in ["ps", "propose_server"]:
                    self.propose_server(member, lower[1:])

                elif lower[0] in ["as", "accept_server"]:
                    self.accept_server(member)

                elif lower[0] in ["ss", "set_server"]:
                    self.set_server(member, lower[1:], access_level)

                elif lower[0] == "stats":
                    self.get_stats(member, msg_tup[1:2])

                elif lower[0] == "top":
                    if len(lower) > 1 and lower[1] == "maps":
                        self.get_top_maps(member, msg_tup[2:msg_len])
                    else:
                        self.get_top(member, msg_tup[1:msg_len])

                elif lower[0] == "activity":
                    self.get_activity(member, msg_tup[1:msg_len])

                elif lower[0] == "set_ao_for_all":
                    self.set_ao_for_all(member, msg_tup[1:msg_len], access_level)

                elif lower[0] == "add_pickups":
                    self.add_pickups(member, msg_tup[1:msg_len], access_level)

                elif lower[0] == "remove_pickups":
                    self.remove_pickups(member, lower[1:msg_len], access_level)

                elif lower[0] == "add_pickup_group":
                    self.add_pickup_group(member, lower[1:msg_len], access_level)

                elif lower[0] == "remove_pickup_group":
                    self.remove_pickup_group(member, lower[1:msg_len], access_level)

                elif lower[0] == "pickup_groups":
                    self.show_pickup_groups()

                elif lower[0] == "maps":
                    self.show_maps(member, lower[1:msg_len], False)

                elif lower[0] == "map":
                    self.show_maps(member, lower[1:msg_len], True)

                elif lower[0] in ["mappref", "map_pref"]:
                    self.set_pref_maps(member, lower[1:msg_len])

                elif lower[0] in ["update_pref", "updatepref"]:
                    self.update_map_pref(member, lower[1:msg_len])

                elif lower[0] == "ip":
                    self.get_ip(member, lower[1:2])

                elif lower[0] == "noadd" and msg_len > 1:
                    await self.noadd(member, msg.mentions, msg_tup[1:msg_len], access_level, msg.channel_mentions)

                elif lower[0] == "forgive":
                    if len(msg.mentions):
                        self.forgive(member, msg.mentions, access_level)
                    else:
                        client.reply(self.channel, member, "usage: !forgive @mention")

                elif lower[0] == "noadds":
                    self.getnoadds(member, msg_tup[1:], msg.mentions)

                elif lower[0] == "report_match":
                    try:
                        await self.report_match_manually(member, msg_tup[1:], access_level)
                    except (ValueError, IndexError) as e:
                        raise PubobotException(e)

                elif lower[0] == "reset":
                    self.reset_players(member, lower[1:msg_len], access_level)

                elif lower[0] == "reset_stats":
                    self.reset_stats(member, access_level)

                elif lower[0] == "phrase":
                    self.set_phrase(member, msg.mentions, msg_tup[1:msg_len], access_level)

                elif lower[0] == "commands":
                    client.reply(self.channel, member, config.cfg.COMMANDS_LINK)

                elif lower[0] == "cfg":
                    self.show_config(member, msg_tup[1:2])

                elif lower[0] in ["pickup_cfg", "pcfg"]:
                    self.show_pickup_config(member, msg_tup[1:3])

                elif lower[0] == "myprefs":
                    self.show_prefs(member)

                elif lower[0] in ["set", "set_default"]:
                    self.configure_default(member, msg_tup[1:msg_len], access_level)

                elif lower[0] in ["setp", "set_pickups"]:
                    self.configure_pickups(member, msg_tup[1:msg_len], access_level)

                elif lower[0] == "help":
                    self.help_answer(member, lower[1:])

                elif lower[0] in ["draw", "rd"]:
                    self.draw_match(member)

                elif lower[0] == "clear_rating_messages":
                    await self.clear_rating_messages(msg, lower[1:], access_level)

                elif lower[0] == "panzer":
                    await self.toggle_panzer(member)

                elif lower[0] == "panzers":
                    self.panzers()

                elif lower[0] == "streaks":
                    self.get_streaks(lower[1:2])

                elif lower[0] == "donate":
                    if hasattr(client.client_config, 'DONATE_REPLY'):
                        client.reply(self.channel, member, client.client_config.DONATE_REPLY)

                elif game_server.process_command(self, member, lower[0], msg_tup, access_level, active_matches):
                    pass

                elif member_info.process_command(self, member, lower[0], msg_tup, access_level):
                    pass

                elif self.cfg["ranked"]:
                    if lower[0] in ['leaderboard', 'lb']:
                        if not self.cfg['hide_ranks']:
                            client.notice(self.channel, self.get_leaderboard(lower[1:2]))
                        elif self.cfg['hide_ranks'] == 2:
                            client.notice(self.channel, self.get_leaderboard(lower[1:2], True))
                            if access_level == 2:
                                client.private_reply(member, self.get_leaderboard(lower[1:2]))
                        elif access_level == 2:
                            client.private_reply(member, self.get_leaderboard(lower[1:2]))
                        else:
                            await msg.add_reaction('🤡')

                    if lower[0] in ['pflb', 'lbpf']:
                        if self.cfg['panzer_role']:
                            if not self.cfg['hide_ranks']:
                                client.notice(self.channel, self.get_leaderboard(lower[1:2], False, True))
                            elif self.cfg['hide_ranks'] == 2:
                                client.notice(self.channel, self.get_leaderboard(lower[1:2], True, True))
                                if access_level == 2:
                                    client.private_reply(member, self.get_leaderboard(lower[1:2], False, True))
                            elif access_level == 2:
                                client.private_reply(member, self.get_leaderboard(lower[1:2], False, True))
                            else:
                                await msg.add_reaction('🤡')

                    elif lower[0] in ["ts", "trueskill"]:
                        if not self.cfg['hide_ranks']:
                            client.notice(self.channel, self.get_trueskill_leaderboard(lower[1:2]))
                        else:
                            await msg.add_reaction('🤡')

                    elif lower[0] == "rank":
                        if not self.cfg['hide_ranks']:
                            self.get_rank_details(member, lower[1:len(lower)], msg.mentions)
                        elif self.cfg['hide_ranks'] == 2:
                            self.get_rank_details(member, lower[1:len(lower)], msg.mentions, True)
                        else:
                            await msg.add_reaction('🤡')

                    #elif lower[0] == "ranks_table": #Turned off for now
                        #pass
                        #self.show_ranks_table()

                    elif lower[0] == "undo_ranks":
                        self.undo_ranks(member, lower[1:2], access_level)

                    elif lower[0] == "reset_ranks":
                        self.reset_ranks(member, access_level)

                    elif lower[0] == "seed":
                        self.seed_player(member, msg.mentions, lower[-1], access_level)

                    elif lower[0] in ["seedpf", "pfseed"]:
                        self.seed_player(member, msg.mentions, lower[-1], access_level, True)

                if self.cfg["party"]:

                    if lower[0] == "party":
                        pickup_name = lower[1] if len(lower) > 1 else None
                        try:
                            party = Party.create_party(member, pickup_name, self)
                            client.notice(self.channel,
                                          "{} party created! use !invite @player (!i) !challenge <party number> (!c)"
                                          .format(party.pickup.name))
                        except PubobotException as e:
                            client.reply(self.channel, member, str(e))

                    if lower[0] == "parties":
                        client.notice(self.channel, Party.who(self))

                    elif lower[0] == "kick":
                        if len(msg.mentions):
                            for target in msg.mentions:
                                try:
                                    Party.kick(member, self, target)
                                    client.reply(self.channel, target, "you just got kicked from party.")
                                except PubobotException as e:
                                    client.reply(self.channel, member, str(e))
                        else:
                            client.reply(self.channel, member, "Argument must be a Member highlight.")

                    elif lower[0] == "add_party":
                        try:
                            party_idx = int(lower[1])
                        except ValueError:
                            client.reply(self.channel, member, "Failed to parse party number")
                            return

                        if access_level:
                            for target in msg.mentions:
                                try:
                                    Party.add_party(target, self, party_idx)
                                except PubobotException as e:
                                    client.reply(self.channel, member, str(e))
                            client.notice(self.channel, self.active_parties[party_idx])
                        else:
                            client.reply(self.channel, member, "You have no right for this!")

                    elif lower[0] == "remove_party":
                        if access_level:
                            for target in msg.mentions:
                                try:
                                    party = Party.find_party_by_player(target, self)
                                    party.remove_player(target)
                                    client.reply(self.channel, target, "You got removed from party by admin.")
                                except PubobotException as e:
                                    client.reply(self.channel, member, str(e))
                        else:
                            client.reply(self.channel, member, "You have no right for this!")

                    elif lower[0] == "create_party":
                        if not access_level:
                            raise MissingModeratorRightsException
                        if len(lower) < 2:
                            raise PubobotException("missing pickup name argument")
                        pickup_name = lower[1]
                        if not len(msg.mentions):
                            raise PubobotException("missing player mentions")
                        try:
                            pickup = next(pickup for pickup in self.pickups if pickup.name.lower() == pickup_name)
                        except StopIteration:
                            raise PubobotException(f"couldn't find pickup {pickup_name}")
                        else:
                            # mentions order doesn't have to be correct
                            party = Party(msg.mentions[0], pickup, msg.mentions[1:])
                            self.active_parties.append(party)
                            client.notice(self.channel, party)

                    elif lower[0] in ["c", "challenge"]:
                        try:
                            party_idx = int(lower[1]) if len(lower) > 1 else None
                        except ValueError:
                            client.reply(self.channel, member, "{} is not a number".format(lower[1]))
                            return
                        except KeyError:
                            client.reply(self.channel, member, "missing party number argument")
                            return
                        try:
                            party = Party.find_party_by_player(member, self)
                            target_party = party.challenge(member, self, party_idx)
                            if isinstance(target_party, PublicParty):
                                target_party.accept_challenge(target_party.get_captain(), self)
                                return
                            # client.notice(self.channel, "challenge created!")
                            msg = "Your party got challenged by {}'s party".format(member.nick or member.name)
                            client.reply(self.channel,
                                         target_party.get_captain(),
                                         msg + ". To accept type !accept_challenge (!ac)," +
                                         " to decline type !decline_challenge (!dc)"
                                         )
                            client.private_reply(target_party.get_captain(), msg +
                                                 " @ <#{}>. Go to channel to accept or decline".format(self.id))
                        except PubobotException as e:
                            client.reply(self.channel, member, str(e))

                    elif lower[0] in ["ac", "accept_challenge"]:
                        try:
                            Party.accept_challenge(member, self)
                        except PubobotException as e:
                            client.reply(self.channel, member, str(e))

                    elif lower[0] in ["dc", "decline_challenge"]:
                        try:
                            declined_party = Party.decline_challenge(member, self)
                            client.reply(self.channel, declined_party.get_captain(), "Your challenge was declined.")
                        except PubobotException as e:
                            client.reply(self.channel, member, str(e))

                    elif lower[0] in ["i", "invite"]:
                        if not len(msg.mentions):
                            client.reply(self.channel, member, "usage: !invite @member")

                        for target in msg.mentions:
                            try:
                                Party.invite(member, target, self)
                                client.reply(self.channel, target,
                                             "you just got invited to join {}'s party. "
                                             "Type !accept_invite (!ai) to join it."
                                             .format(member.nick or member.name)
                                             )
                            except PubobotException as e:
                                client.reply(self.channel, member, str(e))

                    elif lower[0] in ["ai", "accept_invite"]:
                        party_idx = int(lower[1]) if len(lower) > 1 else None
                        try:
                            party = Party.accept_invite(member, self, party_idx)
                            client.notice(self.channel, str(party))
                        except PubobotException as e:
                            client.reply(self.channel, member, str(e))

                    elif lower[0] in ["di", "decline_invite"]:
                        party_idx = int(lower[1]) if len(lower) > 1 else None
                        try:
                            declined_party = Party.decline_invite(member, self, party_idx)
                            client.reply(self.channel, declined_party.get_captain(), "{} declined your invite."
                                         .format(member.nick or member.name))
                        except PubobotException as e:
                            client.reply(self.channel, member, str(e))

                    elif lower[0] in ["lp", "leave_party"]:
                        try:
                            party = Party.find_party_by_player(member, self).remove_player(member)
                            if party is None:
                                info_str = "Party dismissed."
                            else:
                                info_str = str(party)
                            client.notice(self.channel, info_str)
                        except PubobotException as e:
                            client.reply(self.channel, member, str(e))

                    elif lower[0] in ["pp", "public_party"]:
                        pickup_name = lower[1] if len(lower) > 1 else None
                        try:
                            party = PublicParty.add(member, pickup_name, self)
                            client.notice(self.channel,
                                          str(party))
                        except PubobotException as e:
                            client.reply(self.channel, member, str(e))

                    elif lower[0] in ["pickup_party"]:
                        pickup_name = lower[1] if len(lower) > 1 else None
                        try:
                            party = PickupParty.create_party(member, pickup_name, self, True)
                            client.notice(self.channel,
                                          "{} pickup party created! use !invite @player (!i)"
                                          .format(party.pickup.name))
                        except PubobotException as e:
                            client.reply(self.channel, member, str(e))

                    elif lower[0] in ["start_party"]:
                        try:
                            party = Party.find_party_by_player(member, self)
                            if not party.get_captain() == member:
                                raise PartyNotCaptainException
                            if isinstance(party, PickupParty):
                                party.start_pickup()
                            else:
                                raise PubobotException("Not pickup party")
                        except PubobotException as e:
                            client.reply(self.channel, member, str(e))

                # MAP PICK/VETO aaAAbbBB
                if lower[0] == "m" or lower[0] == "pick_map":
                    try:
                        if len(lower) < 2:
                            raise PubobotException("usage: !m <map_index>")
                        match = self.match_by_player(member)
                        if match is None:
                            raise PubobotException("Could not find an active match.")
                        if match.state != "map_picking":
                            raise PubobotException("The match is not on map picking stage.")
                        match.map_pick(member, lower[1])
                    except PubobotException as e:
                        client.reply(self.channel, member, str(e))

    ### COMMANDS ###

    def add_player(self, member, target_pickups, update_topic=True):
        match = self.match_by_player(member)

        # check noadds and phrases
        noadd_tuple = stats3.check_memberid(self.id, member.id)  # is_banned, phrase, default_expire
        if noadd_tuple[0]:  # if banned
            raise NoAddException(noadd_tuple[1])

        changes = False
        # ADD GUY TO TEH GAMES
        if target_pickups == [] and len(
                self.pickups) < 2:  # make !add always work if only one pickup is configured on the channel
            filtered_pickups = self.pickups
        else:
            if not len(target_pickups):
                filtered_pickups = list(
                    filter(lambda p: len(p.players) > 0 and int(self.cfg["++_req_players"]) <= p.cfg["maxplayers"],
                           self.pickups))
            else:
                for i in list(target_pickups):
                    if i in self.pickup_groups.keys():
                        target_pickups.remove(i)
                        target_pickups += self.pickup_groups[i]
                filtered_pickups = list(filter(lambda p: p.name.lower() in target_pickups, self.pickups))

        if match is not None:
            change = False
            for pickup in filtered_pickups:
                if self not in match.re_queue:
                    match.re_queue[self] = {pickup: [member]}
                    change = True
                else:
                    if pickup not in match.re_queue[self]:
                        match.re_queue[self][pickup] = [member]
                        change = True
                    else:
                        if member not in match.re_queue[self][pickup]:
                            match.re_queue[self][pickup].append(member)
                            change = True
            if change and update_topic:
                self.update_topic()
            return

        for pickup in filtered_pickups:
            curr_prefs = stats3.get_pref_maps(self.id, member.id)
            # check if map preferences are set, else notify user
            if pickup.cfg["map_pick_order"] == "mappref" and not curr_prefs and pickup.cfg["map_pref_message"] \
                    and update_topic:  # no need to spam this for requeue
                client.reply(self.channel, member, pickup.cfg["map_pref_message"])

            # check if a map preferences are missing, notify user
            elif pickup.cfg["map_pick_order"] == "mappref" and curr_prefs:
                mapdict = {a: b for a, b in [pref.split(":") for pref in curr_prefs.split(",")]}
                maps = [map.strip() for map in pickup.cfg["maps"].split(",")]
                missing = [map for map in maps if map not in mapdict]
                if len(missing) == 1:
                    client.reply(self.channel, member, "You are missing a map preference for "+
                                 missing[0] + ". Set your preference using !mappref " + missing[0] + " 1/2/3/4")
                if len(missing) == 2:
                    client.reply(self.channel, member, "You are missing a map preference for "+
                                 missing[0] + " and "+ missing[1]+ ". Set your preferences using !mappref mapname 1/2/3/4")
                if len(missing) == 3:
                    client.reply(self.channel, member, "You are missing a map preference for "+
                                 missing[0] + ", " + missing[1] + " and "+ missing[2]+ ". Set your preferences using !mappref mapname 1/2/3/4")

            if member.id not in [i.id for i in pickup.players]:
                # check if pickup have blacklist or whitelist
                whitelist_role = self.get_value("whitelist_role", pickup)
                blacklist_role = self.get_value("blacklist_role", pickup)
                member_roles = [r.id for r in member.roles]
                if blacklist_role in member_roles:
                    blacklist_msg = pickup.channel.get_value('blacklist_msg', pickup)
                    if blacklist_msg is None:
                        client.reply(self.channel, member,
                                     "You are not allowed to play {0} (blacklisted).".format(pickup.name))
                    else:
                        blacklist_msg = blacklist_msg.replace('%pickup_name%', pickup.name)
                        blacklist_role_obj = self.guild.get_role(blacklist_role)
                        blacklist_role_name = "role not found" if blacklist_role_obj is None else blacklist_role_obj.name
                        blacklist_msg = blacklist_msg.replace('%blacklist_role%', blacklist_role_name)
                        client.reply(self.channel, member, blacklist_msg)

                elif not whitelist_role or whitelist_role in member_roles:
                    changes = True
                    pickup.players.append(member)
                    events.pickup_change(pickup)
                    if len(pickup.players) == pickup.cfg['maxplayers']:
                        self.start_pickup(pickup)
                        return
                    elif len(pickup.players) == pickup.cfg['maxplayers'] - 1 and pickup.cfg['maxplayers'] > 2:
                        client.notice(self.channel, "Only 1 player left for {0} pickup. Hurry up!".format(pickup.name))

                    if pickup not in active_pickups:
                        active_pickups.append(pickup)
                else:
                    whitelist_msg = pickup.channel.get_value('whitelist_msg', pickup)
                    if whitelist_msg is None:
                        client.reply(self.channel, member,
                                     "You are not allowed to play {0} (not in whitelist).".format(pickup.name))
                    else:
                        whitelist_msg = whitelist_msg.replace('%pickup_name%', pickup.name)
                        whitelist_role_obj = self.guild.get_role(whitelist_role)
                        whitelist_role_name = "role not found" if whitelist_role_obj is None else whitelist_role_obj.name
                        whitelist_msg = whitelist_msg.replace('%whitelist_role%', whitelist_role_name)
                        client.reply(self.channel, member, whitelist_msg)

        # update scheduler, reply a phrase and update topic
        if changes:
            expire = noadd_tuple[2] if noadd_tuple[2] is not None else self.cfg['global_expire']
            if expire and member.id not in scheduler.tasks.keys():  # if have default_expire and user haven't set expire
                if member.id in scheduler.tasks.keys():
                    scheduler.cancel_task(member.id)

                def global_remove_callback():
                    global_remove(member, 'scheduler')
                scheduler.add_task(member.id, expire, global_remove_callback)
            add_in_task_name = scheduler.TASK_NAMES['add_in'](member, self.channel)
            if add_in_task_name in scheduler.tasks.keys():
                scheduler.cancel_task(add_in_task_name)
            if update_topic:
                self.update_topic(phrase=f"{member.mention} {noadd_tuple[1]}" if noadd_tuple[1] else None)

    def add_in_callback(self, member, pickups):
        def add_player():
            client.reply(self.channel, member, f"Trying to add you from schedule")
            self.scheduled_adds.remove(member)
            try:
                self.add_player(member, pickups)
            except PubobotException as e:
                client.reply(self.channel, member, str(e))
        return add_player

    def add_in_cancel_callback(self, member):
        def on_cancel_callback():
            self.scheduled_adds.remove(member)
        return on_cancel_callback

    def add_in(self, member, args):
        usage = f"Usage: {self.cfg['prefix']}add_in *time* *[pickups]*\n" \
                f"`Example: {self.cfg['prefix']}add_in 2h 30m 2v2 3v3`\n" \
                f"`{self.cfg['prefix']}add_in -`"

        task_name = scheduler.TASK_NAMES['add_in'](member, self.channel)

        if len(args) == 0:
            if task_name in scheduler.tasks:
                time_in = scheduler.tasks[task_name][0]
                target_pickups = scheduler.tasks[task_name][2][0]
                to_str = " To: " + " ".join(target_pickups) if len(target_pickups) else ""
                client.reply(self.channel, member,
                             f"You will be added in {str(datetime.timedelta(seconds=int(time_in - time.time())))}{to_str}")
                return
            else:
                client.reply(self.channel, member, "task not found")
                return

        if len(args) and args[0] in ['-', '--', 'cancel']:
            if task_name in scheduler.tasks:
                scheduler.cancel_task(task_name)
                self.update_topic()
            else:
                client.reply(self.channel, member, "task not found")
            return

        time_list = []
        while len(args):
            if re.match(r"[0-9]+([dhms])", args[0].lower()):
                time_list.append(args.pop(0).lower())
            else:
                break

        if len(time_list):
            delay = utils.parse_time_input(time_list)
            max_delay = 24 * 60 * 60
            if delay > max_delay:
                max_delay_str = arrow.utcnow().shift(seconds=max_delay).humanize(locale='en', only_distance=True)
                client.reply(self.channel, member, f"Adding delay maximum is {max_delay_str}")
                return
        else:
            client.notice(self.channel, usage)
            return

        if self.is_added(member):
            client.reply(self.channel, member, "Not allowed to schedule if you are already queue up.")
            return

        if task_name not in scheduler.tasks:
            self.scheduled_adds.append(member)
        scheduler.add_task(task_name, delay, self.add_in_callback(member, args), self.add_in_cancel_callback(member))

        self.update_topic()
        to_str = " To: " + " ".join(args) if len(args) else ""
        client.reply(self.channel, member,
                     f"You will be added in {str(datetime.timedelta(seconds=int(delay)))}{to_str}")

    def remove_player(self, member, args, reason='online'):
        changes = []
        all_pickups = True

        # add pickups from pickup_groups
        for i in list(args):
            if i in self.pickup_groups.keys():
                args.remove(i)
                args += self.pickup_groups[i]
        # remove player from games
        for pickup in list(active_pickups):
            if member.id in [i.id for i in pickup.players]:
                if pickup.channel.id == self.id and (args == [] or pickup.name.lower() in args):
                    changes.append(pickup.name)
                    pickup.players.remove(member)
                    events.pickup_change(pickup)
                    if len(pickup.players) == 0:
                        active_pickups.remove(pickup)
                elif all_pickups:
                    all_pickups = False

        match = self.match_by_player(member)
        if match is not None:
            if self in match.re_queue:
                change = False
                for pickup in match.re_queue[self]:
                    if member in match.re_queue[self][pickup] and (args == [] or pickup.name.lower() in args):
                        match.re_queue[self][pickup].remove(member)
                        change = True
                if change:
                    self.update_topic()
            return

        # update topic and warn player
        if changes:
            self.update_topic()
            if reason == 'banned':
                client.notice(self.channel, "{0} have been removed from all pickups...".format(member.name))
            # elif reason == 'reset':
            #	if all_pickups:
            #		client.reply(self.channel, member, "You have been removed from all pickups, pickups has been reset.")
            #	else:
            #		client.reply(self.channel, member, "You have been removed from {0} - pickups has been reset.".format(", ".join(changes)))
            elif reason == 'admin':
                if all_pickups:
                    client.reply(self.channel, member, "You have been removed from all pickups by an admin.")
                else:
                    client.reply(self.channel, member,
                                 "You have been removed from {0} by an admin.".format(", ".join(changes)))
            # if reason == 'online':
            # if allpickups:
            #	client.private_reply(self.channel, member, "You have been removed from all pickups")
            # else:
            #	client.private_reply(self.channel, member, "You have been removed from {0}.".format(", ".join(changes)))

            # REMOVE !expire AUTOREMOVE IF HE IS REMOVED FROM ALL GAMES
            if all_pickups and member.id in scheduler.tasks.keys():
                scheduler.cancel_task(member.id)

    def add_players(self, member, pickup, players, access_level):
        if access_level:
            for target in players:
                self.add_player(target, [pickup], False)
            self.update_topic()
        else:
            client.reply(self.channel, member, "You have no right for this!")

    def remove_players(self, member, targets, access_level):
        if access_level:
            for target in targets:
                self.remove_player(target, [], 'admin')
        else:
            client.reply(self.channel, member, "You have no right for this!")

    def who(self, member, args):
        temp_list = []
        
        def member_list_with_expires(players, separator=' / ', max_timeout=3600) -> str:
            return separator.join(["`" + utils.format_member_nick(player) + "`" + 
                                ((" " + arrow.get(scheduler.tasks[player.id][0]).humanize(locale='_en', only_distance=True, granularity=["minute"])) if 
                                    (player.id in scheduler.tasks.keys() and (scheduler.tasks[player.id][0] - time.time()) < max_timeout) else '') 
                                        for player in players])
    
        for pickup in (pickup for pickup in self.pickups if (pickup.name.lower() in args or args == [])):

            to_re_queue = [utils.member_list(match.re_queue[self][pickup]) for match in active_matches if self in match.re_queue and pickup in match.re_queue[self] and match.re_queue[self][pickup]]
            
            if pickup.players or to_re_queue:
                
                player_list = member_list_with_expires(pickup.players)
                pickup_str = f"[**{pickup.name}** ({len(pickup.players)}/{pickup.cfg['maxplayers']})] {player_list}"
                if to_re_queue:
                    pickup_str += " :repeat_one: " + ':small_orange_diamond:'.join(to_re_queue)
                temp_list.append(pickup_str)

        if len(self.scheduled_adds):
            scheduled_members = []
            for _member in self.scheduled_adds:
                for task_name in scheduler.tasks:
                    if task_name == scheduler.TASK_NAMES['add_in'](_member, self.channel):
                        time_in = scheduler.tasks[task_name][0]
                        scheduled_members.append((_member, time_in))
            scheduled_members.sort(key=lambda item: item[1])

            def format_scheduled_member(item):
                time_in_humanized = arrow.get(item[1]).humanize(locale='_en', only_distance=True)
                return f"`{utils.format_member_nick(item[0])}` {time_in_humanized}"
            if len(scheduled_members):
                temp_list.append(f" :timer: {'/'.join([format_scheduled_member(item) for item in scheduled_members])}")
            else:
                client.capture_exception(Exception("scheduled_adds and add_in tasks mismatch"))

        if temp_list:
            client.notice(self.channel, '\n'.join(temp_list))
        else:
            client.notice(self.channel, 'no one added...ZzZz')

    def panzers(self):
        if self.cfg['panzer_role']:
            templist = []
            for pickup in (pickup for pickup in self.pickups if pickup.players != []):
                templist.append('[**{0}** ({1} Panzer Players)] {2}'.format(pickup.name, len(
                    [i for i in pickup.players if
                     self.cfg['panzer_role'] in [role.id for role in i.roles]]),
                                                                            '/'.join(
                                                                                ["`" + (i.nick or i.name).replace("`",
                                                                                                                  "") + "`"
                                                                                 for i
                                                                                 in pickup.players if
                                                                                 self.cfg['panzer_role'] in [
                                                                                     role.id for role in i.roles]])))

            if templist != []:
                client.notice(self.channel, ' '.join(templist))
            else:
                client.notice(self.channel, 'No Panzers in the Queue')

    async def who_scheduled(self, message):
        if len(self.scheduled_adds):
            lines = []
            for member in self.scheduled_adds:
                for task_name in scheduler.tasks:
                    if task_name == scheduler.TASK_NAMES['add_in'](member, self.channel):
                        time_in = scheduler.tasks[task_name][0]
                        target_pickups = scheduler.tasks[task_name][1].__closure__[1].cell_contents
                        to_str = " To: " + " ".join(target_pickups) if len(target_pickups) else ""
                        line = f"{utils.format_member_nick(member)} in: {str(datetime.timedelta(seconds=int(time_in - time.time())))}{to_str}"
                        lines.append(line)
            if lines:
                client.notice(self.channel, "\n".join(lines))
            else:
                client.capture_exception(Exception("scheduled_adds and add_in tasks mismatch"))
                await message.add_reaction('🦋')
        else:
            await message.add_reaction('🦋')

    def set_pref_maps(self, member, prefs):
        if not prefs:
            self.show_prefs(member)
            return
        try:
            if len(prefs) == 2:
                self.update_map_pref(member, prefs)
                return
            mapdict = {a: b.strip() for a, b in [pref.split(":") for pref in prefs[0].split(",")]}
            curr_prefs = stats3.get_pref_maps(self.id, member.id)
            curr_mapdict = {a.strip(): b.strip() for a, b in [pref.split(":") for pref in curr_prefs.split(",")]} if curr_prefs else dict()

            for mapname, pref in mapdict.items():
                if pref in ["1", "always"]:
                    curr_mapdict[mapname] = "1"
                if pref in ["2", "sometimes"]:
                    curr_mapdict[mapname] = "2"
                if pref in ["3", "never"]:
                    curr_mapdict[mapname] = "3"
                if pref in ["4", "dunno"]:
                    curr_mapdict[mapname] = "4"

            new_prefs = ",".join([a + ":" + b for a, b in curr_mapdict.items()])

            stats3.set_pref_maps(self.id, member, new_prefs, self.cfg['initial_rating'])
            client.notice(self.channel, 'Map preferences set.')
        except Exception:
            client.notice(self.channel, 'Wrong command format. Correct usage: !mappref map1:number1,map2:number2,... with the numbers encoding: 1 - always, 2 - sometimes, 3 - never, 4 - dont know. Example: !mappref ice:1,beach:3,base:2')

    def update_map_pref(self, member, pref):
        if len(pref) == 2 and pref[1] in ["1", "2", "3", "4", "always", "sometimes", "never", "dunno"]:
            curr_prefs = stats3.get_pref_maps(self.id, member.id)
            mapdict = {a: b.strip() for a, b in [pref.split(":") for pref in curr_prefs.split(",")]} if curr_prefs else dict()

            if pref[1] in ["1", "always"]:
                mapdict[pref[0]] = "1"
            if pref[1] in ["2", "sometimes"]:
                mapdict[pref[0]] = "2"
            if pref[1] in ["3", "never"]:
                mapdict[pref[0]] = "3"
            if pref[1] in ["4", "dunno"]:
                mapdict[pref[0]] = "4"

            new_prefs = ",".join([a+":"+b for a, b in mapdict.items()])
            self.set_pref_maps(member, [new_prefs])
        else:
            client.notice(self.channel, 'Wrong command format. Correct usage: ' + self.cfg["prefix"] + 'mappref mapname always/sometimes/never/dunno')

    def who_all(self):
        matches = {}
        pickups = {}
        parties = {}

        for match in active_matches:
            if match.channel.guild.id == self.guild.id:
                ago = datetime.timedelta(seconds=int(time.time() - match.start_time))
                match_str = "**{}** {} {} ago".format(match.pickup.name, match.state, ago)
                if match.channel.id in matches.keys():
                    matches[match.channel.id].append(match_str)
                else:
                    matches[match.channel.id] = [match_str]

        for pickup in active_pickups:
            if pickup.channel.guild.id == self.guild.id:
                pickup_str = "**{}** ({}/{})".format(
                    pickup.name,
                    len(pickup.players),
                    pickup.cfg['maxplayers'])
                if pickup.channel.id in pickups.keys():
                    pickups[pickup.channel.id].append(pickup_str)
                else:
                    pickups[pickup.channel.id] = [pickup_str]

        for channel in channels:
            if channel.guild.id == self.guild.id:
                for party in channel.active_parties:
                    party_str = "**{}** ({}/{})".format(
                        party.pickup.name,
                        len(party.members),
                        party.capacity)
                    if party.pickup.channel.id in parties.keys():
                        parties[party.pickup.channel.id].append(party_str)
                    else:
                        parties[party.pickup.channel.id] = [party_str]

        pickups_field_value = "\n".join(
            ["<#{}> ".format(channel_id) + " ".join(pickups[channel_id]) for channel_id in pickups.keys()])
        parties_field_value = "\n".join(
            ["<#{}> ".format(channel_id) + " ".join(parties[channel_id]) for channel_id in parties.keys()])
        matches_field_value = "\n".join(
            ["<#{}> ".format(channel_id) + "\n".join(matches[channel_id]) for channel_id in matches.keys()])

        embed = Embed()
        if pickups_field_value != "":
            embed.add_field(name="**Pickups**", value=pickups_field_value)
        if parties_field_value != "":
            embed.add_field(name="**Parties**", value=parties_field_value)
        if matches_field_value != "":
            embed.add_field(name="**Matches**", value=matches_field_value)
        if embed.fields:
            client.notice(self.channel, 'who all:', embed=embed)
        else:
            client.notice(self.channel, "it's dead...")

    def user_start_pickup(self, member, args, access_level):
        if access_level:
            if len(args):
                for pickup in self.pickups:
                    if pickup.name.lower() == args[0]:
                        self.start_pickup(pickup)
            elif len(self.pickups) == 1:
                self.start_pickup(self.pickups[0])
            else:
                client.reply(self.channel, member, "You must specify a pickup to start!")
        else:
            client.reply(self.channel, member, "You have no right for this!")

    def lastgame(self, member, args):
        if args != []:
            l = stats3.lastgame(self.id, args[0])  # id, ago, gametype, players, alpha_players, beta_players
        else:
            l = self.lastgame_cache
        if l:
            n = l[0]
            ago = datetime.timedelta(seconds=int(time.time() - int(l[1])))
            gt = l[2]
            if l[4] and l[5]:
                players = "[{0}] vs [{1}]".format(l[4], l[5])
            else:
                players = ", ".join(l[3].strip().split(" "))
            client.notice(self.channel, "Pickup #{0}, {1} ago [{2}]: {3}".format(n, ago, gt, players))
        else:
            client.notice(self.channel, "No pickups found.")

    async def sub_request(self, member):
        if not self.lastgame_pickup:
            client.reply(self.channel, member, "No pickups played yet.")
            return

        self.newtime = time.time()
        if self.newtime - self.oldtime > int(self.cfg['promotion_delay']):
            promotion_role = self.get_value('promotion_role', self.lastgame_pickup)
            if promotion_role:
                promotion_role = "<@&{0}>".format(promotion_role)
            ip = self.get_value('ip', self.lastgame_pickup)
            password = self.get_value('password', self.lastgame_pickup)
            submsg = self.get_value('submsg', self.lastgame_pickup)
            if not submsg:
                submsg = "%promotion_role% NEED SUB @ **%pickup_name%**, please connect to %ip%."

            submsg = submsg.replace("%pickup_name%", self.lastgame_pickup.name)
            submsg = submsg.replace("%ip%", ip or "")
            submsg = submsg.replace("%password%", password or "")
            submsg = submsg.replace("%promotion_role%", str(promotion_role or ""))

            promotion_role = self.get_value('promotion_role', self.lastgame_pickup)
            edit_role = False
            if promotion_role:
                roles = self.guild.roles
                try:
                    role_obj = next(x for x in roles if x.id == promotion_role)
                except StopIteration:
                    client.notice(self.channel, "Specified promotion role doesn't exist on the server.")
                    return

                if not role_obj.mentionable:
                    try:
                        await client.edit_role(role_obj, mentionable=True)
                        edit_role = True
                    except:
                        pass

            await self.channel.send(submsg)

            if edit_role:
                await client.edit_role(role_obj, mentionable=False)

            self.oldtime = self.newtime
        else:
            client.reply(self.channel, member, "You can't promote too often! You have to wait {0}.".format(
                str(datetime.timedelta(seconds=int(int(self.cfg['promotion_delay']) - self.newtime + self.oldtime)))))

    def cointoss(self, member, args):
        if len(args):
            if args[0] in ['heads', 'tails']:
                pick = args[0]
            else:
                client.reply(self.channel, member, "Its best to pick between **heads** or **tails**. But who knows...")
                # return
                pick = args[0]
        else:
            pick = 'heads'

        result = random.choice(['heads', 'tails'])
        if result == pick:
            client.reply(self.channel, member, "You win, it's **{0}**!".format(result))
        else:
            client.reply(self.channel, member, "You lose, it's **{0}**!".format(result))

    # next
    def match_by_player(self, member) -> Optional[Match]:
        for match in active_matches:
            if member in [player for player in match.players]:
                return match
        return None

    def pick_player(self, member, target, match=None):
        if match is None:
            match = self.match_by_player(member)
        if match is None:
            raise NotInActiveMatch

        if match.state != "teams_picking":
            raise PubobotException("The match is not on teams picking stage.")

        if match.pick_step < 0 or len(match.alpha_team) == 0 or len(match.beta_team) == 0:
            raise PubobotException("wait until moderator chooses captain(s) with `!put @member <team_name>` command")

        if member in match.alpha_team[0:1]:
            team = match.alpha_team
            if match.pick_order:
                if match.pick_order[match.pick_step] == "b":
                    raise PubobotException("Not your turn to pick.")

        elif member in match.beta_team[0:1]:
            team = match.beta_team
            if match.pick_order:
                if match.pick_order[match.pick_step] == "a":
                    raise PubobotException("Not your turn to pick.")

        else:
            raise PubobotException("You are not a captain.")

        # TODO: if target is string then search in nicks and member_info

        if isinstance(target, int):
            target_id = target
        else:
            target_id = target.id

        for i in match.unpicked:
            if i.id == target_id:
                team.append(i)
                match.unpicked.remove(i)
                if len(match.unpicked) == 0:
                    match.next_state_with_notice()
                elif len(match.unpicked) == 1 and match.pick_order:
                    match.pick_step += 1
                    if match.pick_order[match.pick_step] == 'a':
                        match.alpha_team.append(match.unpicked[0])
                    else:
                        match.beta_team.append(match.unpicked[0])
                    match.unpicked.remove(match.unpicked[0])
                    match.next_state_with_notice()
                else:
                    msg = ""
                    if match.pick_order:
                        match.pick_step += 1
                        if match.pick_order[match.pick_step] == 'a':
                            if len(match.alpha_team):
                                who = "<@{0}>".format(match.alpha_team[0].id)
                            else:
                                who = match.team_names[0]
                        else:
                            if len(match.beta_team):
                                who = "<@{0}>".format(match.beta_team[0].id)
                            else:
                                who = match.team_names[1]
                        msg += "\n{0}'s turn to pick!".format(who)
                    team_str, embed, view = self.teams_picking_to_str_or_embed_and_view(match)
                    if team_str:
                        msg = team_str + msg
                    client.notice(match.channel, msg, embed, view=view)
                    events.player_pick(match)
                return
        raise PubobotException("Specified player are not in unpicked players list.")

    async def pick_captains(self, member, args, access_level):
        if not access_level:
            client.reply(self.channel, member, "You don't have right for this!")
            return
        if len(args) == 2:
            try:
                player1_id = utils.get_id_from_mention(args[0])
                player2_id = utils.get_id_from_mention(args[1])
            except (ValueError, IndexError, AttributeError):
                client.reply(self.channel, member, "Failed to get member from mention.")
                return

            match, player1 = _match_and_player_by_player_id(player1_id)
            match2, player2 = _match_and_player_by_player_id(player2_id)
            if match != match2:
                client.reply(self.channel, member, "Users from different matches.")
                return
            if match.state != "teams_picking":
                client.reply(self.channel, member, "Match not in picking stage.")
                return
            match.alpha_team = [player1]
            match.beta_team = [player2]
            match.pick_step = 0
            match.captains = [player1, player2]
            match.unpicked = match.players.copy()
            try:
                match.unpicked.remove(player1)
                match.unpicked.remove(player2)
            except ValueError:
                pass
            match.suggested_teams_accepted = Teams.NONE
            match.cancel_request = Teams.NONE
            msg, embed, view = self.teams_picking_to_str_or_embed_and_view(match)
            client.notice(self.channel, msg, embed, view=view)
            events.player_pick(match)
        else:
            client.notice(self.channel, "usage: `!pick_captains @player1 @player2`")

    async def set_panzers(self, member, args, access_level):
        if not access_level:
            client.reply(self.channel, member, "You don't have right for this!")
            return
        if len(args) == 2:
            try:
                player1_id = utils.get_id_from_mention(args[0])
                player2_id = utils.get_id_from_mention(args[1])
            except (ValueError, IndexError, AttributeError):
                client.reply(self.channel, member, "Failed to get member from mention.")
                return

            match, player1 = _match_and_player_by_player_id(player1_id)
            match2, player2 = _match_and_player_by_player_id(player2_id)
            if match != match2:
                client.reply(self.channel, member, "Users from different matches.")
                return
            if match.state != "waiting_report":
                client.reply(self.channel, member, "Match isn't in progress.")
                return
            if not self.cfg['panzer_role']:
                client.reply(self.channel, member, "No panzer role set on this channel.")
                return
            if player1 in match.alpha_team and player2 in match.beta_team:
                match.panzers = [player1, player2]
            elif player2 in match.alpha_team and player1 in match.beta_team:
                match.panzers = [player2, player1]
            else:
                client.reply(self.channel, member, "Players must be on opposite teams.")
                return
            match.ranks = stats3.get_ranks(self, [i.id for i in match.players])
            match.panzer_ranks = stats3.get_panzer_ranks(self, [i.id for i in match.players])

            client.notice(self.channel, "Panzers set")
        else:
            client.notice(self.channel, "usage: `!set_panzers @player1 @player2`")

    def put_player(self, member, targets, team_name, access_level):
        if not access_level:
            client.reply(self.channel, member, "You dont have right for this!")
            return

        for player in targets:

            match = self.match_by_player(player)
            if match is None:
                client.reply(self.channel, member, "Specified user is not in a match.")
                return

            if match.pick_teams not in ['auto', 'manual']:
                client.reply(self.channel, member, "This match does not have teams.")
                return

            if team_name == match.team_names[0].lower():
                team = match.alpha_team
            elif team_name == match.team_names[1].lower():
                team = match.beta_team
            elif team_name == 'unpicked':
                team = match.unpicked
            else:
                client.reply(self.channel, member,
                             "Team argument must be **{0}**, **{1}** or **unpicked**.".format(*match.team_names))
                return

            if player in match.unpicked:
                match.unpicked.remove(player)
            elif player in match.beta_team:
                match.beta_team.remove(player)
            elif player in match.alpha_team:
                match.alpha_team.remove(player)
            team.append(player)
            if match.state == "teams_picking" and len(match.unpicked) == 0:
                match.next_state_with_notice()
            else:
                # pick order could be still broken if for example alpha got 10 players and beta 1,
                # while pick_order is abababab...
                match.pick_step = len(match.alpha_team) + len(match.beta_team) - 2
                if len(match.alpha_team) and len(match.beta_team):
                    match.captains = [match.alpha_team[0], match.beta_team[0]]
                msg, embed, view = self.teams_picking_to_str_or_embed_and_view(match)
                client.notice(self.channel, msg, embed, view=view)
                events.player_pick(match)

    async def subfor(self, member, args, access_level):

        if not access_level:
            client.reply(self.channel, member, "You don't have right for this")
            return

        if len(args) > 1:

            target = args[0]
            ntarget = await client.get_member_by_id(self.channel, args[1])

            if ntarget is None:
                client.reply(self.channel, member, "Could not find specified substitute.")
                return

            try:
                target_id = utils.get_id_from_mention(target)
            except:
                try:
                    target_id = int(target)
                except ValueError:
                    target_id = None

            match = None
            for _match in active_matches:
                for player in _match.players:
                    if target_id is None:
                        if target in (player.nick or player.name):
                            match = _match
                            target_id = player.id
                            break
                    else:
                        if target_id == player.id:
                            match = _match
                            break

            replace = self.match_by_player(ntarget)

            if match is None:
                client.reply(self.channel, member, "Could not find target in an active match.")
                return

            if replace is not None:
                client.reply(self.channel, member, "Substitute is already in an active match.")
                return

            if match.state not in ["teams_picking", "waiting_report"]:
                client.reply(self.channel, member, "The match is not on teams picking stage.")
                return

            if ntarget in match.players:
                client.reply(self.channel, member, "Substitute is already in the players list!")
                return

            if match.captains:
                try:
                    team_idx = [_p.id for _p in match.captains].index(target_id)
                except ValueError:
                    # player to be replaced isn't captain
                    pass
                else:
                    match.captains[team_idx] = ntarget

            if match.panzers:
                try:
                    team_idx = [_p.id for _p in match.panzers].index(target_id)
                except ValueError:
                    # player to be replaced isn't captain
                    pass
                else:
                    match.panzers[team_idx] = ntarget

            for x in [match.unpicked, match.alpha_team, match.beta_team]:
                try:
                    idx = [player.id for player in x].index(target_id)
                except ValueError:
                    continue
                else:
                    x[idx] = ntarget
                    idx = [player.id for player in match.players].index(target_id)

                    match.players = list(match.players)
                    match.players[idx] = ntarget

                    # update ranks table if needed
                    if match.ranked:
                        match.ranks = stats3.get_ranks(self, [i.id for i in match.players])
                        match.panzer_ranks = stats3.get_panzer_ranks(self, [i.id for i in match.players])
                        match.players = list(sorted(match.players, key=lambda p: match.ranks[p.id], reverse=True))
                        if len(args) == 3 and args[2].lower() == "redue":
                            match.create_teams(sub=True)
                            client.notice(self.channel, *match.startmsg_instant())
                            global_remove(ntarget, "sub")
                            return

                    client.notice(self.channel, *self.teams_picking_to_str_or_embed_and_view(match))
                    global_remove(ntarget, "sub")
                    return
            client.reply(self.channel, member, "Specified target not found in the match!")
        else:
            client.reply(self.channel, member, "You must specify a target and substitute.")

    def capfor(self, member, args):
        match = self.match_by_player(member)
        if match is None:
            client.reply(self.channel, member, "Could not find an active match.")
            return

        if match.state != "teams_picking":
            client.reply(self.channel, member, "The match is not on the teams picking stage.")
            return

        if match.captains_role:
            if match.captains_role not in [role.id for role in member.roles]:
                client.reply(self.channel, member, "You don't possess the captain role for this pickup.")
                return

        if match.pick_order:
            if match.pick_step > 0:
                client.reply(self.channel, member, "You can only use !capfor before picks.")
                return

        if len(args):
            if args[0] == match.team_names[0].lower():
                team = match.alpha_team
                team_idx = 0
            elif args[0] == match.team_names[1].lower():
                team = match.beta_team
                team_idx = 1
            else:
                client.reply(self.channel, member,
                             "Specified team must be **{0}** or **{1}**.".format(*match.team_names))
                return
        else:
            client.reply(self.channel, member, "You must specify the team.")
            return

        if match.captains:
            other_team_idx = (team_idx + 1) % 2
            if match.captains[other_team_idx] == member:
                # swap if member is captain of other team
                match.captains[team_idx], match.captains[other_team_idx] = member, match.captains[team_idx]
            else:
                match.captains[team_idx] = member

        for x in [match.unpicked, match.beta_team, match.alpha_team]:
            if member in x:
                idx = x.index(member)
                if len(team):
                    x[idx] = team[0]
                    team[0] = member
                else:
                    x.remove(member)
                    team.append(member)
                client.notice(self.channel, *self.teams_picking_to_str_or_embed_and_view(match))
                return

    def teams_picking_to_str_or_embed_and_view(self, match) -> Tuple[str, Embed, View]:
        embed = None
        msg = None
        if self.channel.permissions_for(self.channel.guild.me).embed_links:
            embed = match.teams_picking_to_embed()
        else:
            msg = match.teams_picking_to_str()
        view = match.teams_picking_to_view()
        return msg, embed, view

    def print_teams(self, member):
        match = self.match_by_player(member)
        if match is None:
            client.reply(self.channel, member, "Could not find an active match.")
            return
        if match.state == 'waiting_ready':
            return

        if match.pick_teams != "no_teams":
            client.notice(self.channel, *self.teams_picking_to_str_or_embed_and_view(match))
        else:
            client.reply(self.channel, member, "This match does not have teams.")

    def redo_teams(self, member, args, access_level):
        if not access_level:
            client.reply(self.channel, member, "You dont have right for this!")
            return

        if not len(args):
            client.reply(self.channel, member, "You must specify the match id.")
            return

        for i in active_matches:
            if str(i.id) == args[0] and i.pickup.channel.id == self.id:
                i.create_teams(sub=True)
                client.notice(self.channel, *i.startmsg_instant())
                return

        client.reply(self.channel, member, "Could not find an active match with id '{0}'".format(args[0]))

    def cancel_match(self, member, args, mentions, access_level):
        if not access_level:
            client.reply(self.channel, member, "You dont have right for this!")
            return

        if not len(args):
            client.reply(self.channel, member, "You must specify the match id or mention player.")
            return

        # !cancel_match 12345 @mention
        if len(mentions) > 0:
            match = self.match_by_player(mentions[0])
            if match is None:
                client.reply(self.channel, member,
                             "Could not find player {0} in an active match".format(
                                 mentions[0].nick or mentions[0].name))
                return
            else:
                match.cancel_match_freeze(mentions[0])
                return

        # !cancel_match 12345
        for i in active_matches:
            if str(i.id) == args[0] and i.pickup.channel.id == self.id:
                i.cancel_match()
                return

        client.reply(self.channel, member, "Could not find an active match with id '{0}'".format(args[0]))

    def report_match(self, member, args=None, access_level=None):
        # !reportwin
        if args is not None:
            if access_level < 1:
                client.reply(self.channel, member, "Insufficient permissions.")
                return

            if len(args) < 2:
                client.reply(self.channel, member, "You must specify *match_id* and winner team.")
                return
            match_id, winner = args[0:2]

            match = None
            for i in active_matches:
                if str(i.id) == match_id and i.pickup.channel.id == self.id:
                    match = i
                    break
            if not match:
                client.reply(self.channel, member, "Could not find an active match with {0} id.".format(match_id))
                return
            if match.state != "waiting_report":
                client.reply(self.channel, member, "This match is not on waiting report state yet.")
                return

            if winner == match.team_names[0].lower():
                winner = 'alpha'
            elif winner == match.team_names[1].lower():
                winner = 'beta'
            elif winner == 'draw':
                pass
            else:
                client.reply(self.channel, member, "Winner team must be '{0}' or '{1}'.".format(*match.team_names))
                return

        # !reportlose
        else:
            match = None
            for i in active_matches:
                if i.pickup.channel.id == self.id and member in i.players:
                    match = i
                    if match.state != "waiting_report":
                        client.reply(self.channel, member, "This match is not on waiting report state yet.")
                        return
                    if match.captains and len(match.captains) == 2:
                        if match.captains[0] == member:
                            winner = 'beta'
                        elif match.captains[1] == member:
                            winner = 'alpha'
                        else:
                            client.reply(self.channel, member, "You must be captain of the team to report its loss.")
                            return
                    else:
                        if match.beta_team[0] == member:
                            winner = 'alpha'
                        elif match.alpha_team[0] == member:
                            winner = 'beta'
                        else:
                            client.reply(self.channel, member, "You must be captain of the team to report its loss.")
                            return
                    break
            if not match:
                client.reply(self.channel, member, "You are not in an active match.")
                return

        if len(match.alpha_team) == 0 or len(match.beta_team) == 0:
            client.reply(self.channel, member, "Each team must contain at least one player.")
            return

        match.winner = winner
        match.next_state()

    def draw_match(self, member):
        # !draw
        match = None
        for i in active_matches:
            if i.pickup.channel.id == self.id and member in i.players:
                match = i
                if match.state != "waiting_report":
                    client.reply(self.channel, member, "This match is not on waiting report state yet.")
                    return
                alpha_cap = match.captains[0] if match.captains and len(match.captains) == 2 else match.alpha_team[0]
                beta_cap = match.captains[1] if match.captains and len(match.captains) == 2 else match.beta_team[0]
                if beta_cap == member:
                    if not match.beta_draw:
                        match.beta_draw = True
                        if not match.alpha_draw:
                            client.reply(self.channel, member, "Match is now waiting for Alpha captain to report draw.")
                        else:
                            i.draw_match()
                    else:
                        client.reply(self.channel, member, "You already requested a draw, waiting for Alpha captain.")
                elif alpha_cap == member:
                    if not match.alpha_draw:
                        match.alpha_draw = True
                        if not match.beta_draw:
                            client.reply(self.channel, member, "Match is now waiting for Beta captain to report draw.")
                        else:
                            i.draw_match()
                    else:
                        client.reply(self.channel, member, "You already requested a draw, waiting for Beta captain.")
                else:
                    client.reply(self.channel, member, "You must be captain of the team to report a draw.")

        if not match:
            client.reply(self.channel, member, "You are not in an active match.")

    def report_match_cancel(self, member: Member):
        """
        !report_cancel
        """
        try:
            _, match = find_member_in_active_matches(member.id)
            captain_idx = match.captains.index(member)
            other_captain_idx = (captain_idx+1) % 2
            if match.cancel_request == Teams.NONE:
                match.cancel_request = Teams(captain_idx)
                view = View(timeout=10 * 60)
                button = Button(label='Cancel match')

                async def button_callback(interaction: Interaction):
                    if self.report_match_cancel(self.channel.guild.get_member(interaction.user.id)):
                        await interaction.response.edit_message(content="Match cancelled.", view=None)

                button.callback = button_callback
                view.add_item(item=button)
                client.reply(self.channel, match.captains[other_captain_idx],
                             f"{member.nick or member.name} proposed match cancel. "
                             f"`!rc` to accept.", view=view)
            elif match.cancel_request.value == other_captain_idx:
                match.cancel_match()
                return True

        except StopIteration:
            client.private_reply(member, "You are not in an active match.")
        except ValueError:
            client.private_reply(member, "You must be captain of the team to request match cancel.")

    def propose_server(self, member, args):
        if not len(args):
            client.reply(self.channel, member, "You must specify the server id.")
            return

        try:
            server_id = int(args[0])
        except ValueError:
            client.reply(self.channel, member, "Server id must be integer.")
            return

        match = self.match_by_player(member)

        if match is None:
            client.reply(self.channel, member, "You are not in an active match.")
            return

        try:
            captain_index = match.captains.index(member)
        except (ValueError, AttributeError):
            client.reply(self.channel, member, "You must be captain of the team to propose a server.")
            return

        servers = stats3.get_game_servers(self.channel.guild.id)
        try:
            server = next(server for server in servers if server['game_server_id'] == server_id)
        except StopIteration:
            client.reply(self.channel, member, "Server not found")
            return

        if server_id in [match.server['game_server_id'] for match in active_matches if match.channel.guild == self.channel.guild and match.server is not None]:
            client.reply(self.channel, member, "Server used by other match. see `!servers`")
            return

        match.proposed_server = {'server': game_server.to_dict(server), 'captain_index': captain_index}
        other_captain_index = (captain_index+1) % 2
        view = View(timeout=5*60)

        async def button_callback(interaction: Interaction):
            picker = interaction.user
            try:
                self.accept_server(picker)
                await interaction.response.edit_message(
                    content=f"Server proposition accepted.",
                    view=None)
            except PubobotException as e:
                await interaction.response.send_message(e.with_mention(picker))

        button = Button(label='Accept server')
        button.callback = button_callback
        view.add_item(item=button)
        client.reply(self.channel, match.captains[other_captain_index],
                     f"{member.nick or member.name} proposed server:"
                     f" {server['ip']} {game_server.get_server_pw(server)}. "
                     "`!as` to accept", view=view)

    def accept_server(self, member):
        match = self.match_by_player(member)

        if match is None:
            raise NotInActiveMatch

        try:
            captain_index = match.captains.index(member)
        except ValueError:
            raise PubobotException("You must be captain of the team to accept a server.")

        other_captain_index = (captain_index + 1) % 2
        if match.proposed_server is None or match.proposed_server['captain_index'] != other_captain_index:
            raise PubobotException("The other captain hasn't proposed server.")

        server = match.proposed_server['server']

        if server['game_server_id'] in [match.server['game_server_id'] for match in active_matches if match.channel.guild == self.channel.guild and match.server is not None]:
            raise PubobotException("Server used by other match. see `!servers`")

        match.proposed_server = None
        match.set_server(server)

    def set_server(self, member, args, access_level):
        if not access_level:
            client.reply(self.channel, member, "You don't have right for this!")
            return
        if len(args) != 2:
            client.notice(self.channel, "Usage: `!set_server <match_id> <server_id>`")
            return

        try:
            match_id = int(args[0])
        except ValueError:
            client.reply(self.channel, member, "Match id must be integer.")
            return

        try:
            server_id = int(args[1])
        except ValueError:
            client.reply(self.channel, member, "Server id must be integer.")
            return

        try:
            match = next(_match for _match in active_matches if _match.id == match_id and self.channel.id == _match.pickup.channel.id)
        except StopIteration:
            client.reply(self.channel, member, "Match not found.")
            return

        servers = stats3.get_game_servers(self.channel.guild.id)
        try:
            server = next(server for server in servers if server['game_server_id'] == server_id)
        except StopIteration:
            client.reply(self.channel, member, "Server not found")
            return

        if server['game_server_id'] in [match.server['game_server_id'] for match in active_matches if match.channel.guild == self.channel.guild and match.server is not None]:
            client.reply(self.channel, member, "Server used by other match. see `!servers`")
            return

        match.set_server(game_server.to_dict(server))

    def get_matches(self):
        l = []
        for match in active_matches:
            if match.pickup.channel.id == self.id:
                if match.pick_teams != 'no_teams' and match.pick_teams is not None and match.state == 'waiting_report':
                    if match.captains and len(match.captains) == 2:

                        def display_nick(player):
                            if player in match.captains and player in match.panzers:
                                return "(C/PF)"+(player.nick or player.name)
                            if player in match.captains:
                                return "(C)"+(player.nick or player.name)
                            if player in match.panzers:
                                return "(PF)"+(player.nick or player.name)
                            return player.nick or player.name

                        alpha_str = ", ".join(map(display_nick, match.alpha_team))
                        beta_str = ", ".join(map(display_nick, match.beta_team))
                    else:
                        alpha_str = "(C)"+", ".join([i.nick or i.name for i in match.alpha_team])
                        beta_str = "(C)"+", ".join([i.nick or i.name for i in match.beta_team])
                    match_str = "*({0})* **{1}** | {2} | `{3}` vs `{4}` | started <t:{5}:R>".format(
                        match.id,
                        match.pickup.name,
                        match.state,
                        alpha_str,
                        beta_str,
                        int(match.start_time)
                    )
                    if match.server is not None:
                        match_str += " | server: " + game_server.dict_to_str(match.server)
                    if match.gtv_match_id is not None:
                        match_str += " | " + gtvd.match_link(match.gtv_match_id)
                        match_str += gtvd.relays_to_str(gtvd.get_relays(match.gtv_match_id), ' ettv: ')
                    l.append(match_str)
                else:
                    not_ready = (" | not ready: `" + ", ".join([i.nick or i.name for i in match.players if i.id not in match.players_ready]) + "`") if match.state == 'waiting_ready' else ""
                    l.append("*({0})* **{1}** | {2} | `{3}` | started <t:{4}:R> {5}".format(match.id, match.pickup.name,
                                                                                          match.state, ", ".join(
                            [i.nick or i.name for i in match.players]), int(match.start_time), not_ready))
        if len(l):
            client.notice(self.channel, "\n".join(l))
        else:
            client.notice(self.channel, "There is no active matches right now.")

    def get_streaks(self, args):
        limit = 5
        streaks_active_past_days = self.get_cfg_extension('streaks_active_past_days', default=7)
        if streaks_active_past_days != -1:
            past_seconds = streaks_active_past_days * 24 * 60 * 60
        else:  # to make it configurable to disable activity check
            past_seconds = None
        if "-a" in args:
            past_seconds = None
        top, bottom = stats3.get_streaks(self.id, limit, past_seconds)
        if len(top) > 0 or len(bottom) > 0:
            # use as little columns as possible because atm discord forces line breaks
            headers = ['S', 'Nick']
            s = f"```{tabulate([*top,SEPARATING_LINE,*bottom], headers=headers, tablefmt='simple')}```"
            client.notice(self.channel, s)
        else:
            client.notice(self.channel, "Not enough data to display top streaks.")

    def get_leaderboard(self, page, win_loss_only=False, pf=False):
        try:
            page = int(page[0]) - 1
        except (ValueError, IndexError):
            page = 0

        min_matches = self.cfg['min_matches_leaderboard'] if 'min_matches_leaderboard' in self.cfg else None
       
        # [rank, nick, wins, loses]
        data, period_limit = stats3.get_ladder(self.id, page, win_loss_only=win_loss_only, pf=True, min_matches=min_matches) if pf else stats3.get_ladder(self.id, page, win_loss_only=win_loss_only, min_matches=min_matches)
        if len(data):

            l = [
                [(page * 10) + (n + 1)] +
                ([] if win_loss_only else [str(data[n]['rank'])])
                + [data[n]['nick'],
                   data[n]['wins'],
                   data[n]['draws'],
                   data[n]['loses'],
                   int(data[n]['wr'] * 100)
                   ] + ([] if win_loss_only else [utils.rating_to_icon(data[n]['rank'],self.cfg['custom_ranks'])[1:-1]])
                for n in range(0, len(data))]
            # use as little columns as possible because atm discord forces line breaks
            headers = ['№'] + ([] if win_loss_only else ['Rating']) + ['Nickname', 'W', 'D', 'L', '%'] + ([] if win_loss_only else ['Rank'])
            s = "```{}```".format(tabulate(l, headers=headers))
            return s
        else:
            msg = f"No rated players, who played at least once past {arrow.get(period_limit).humanize(locale='en', only_distance=True)}"
            if min_matches:
                msg += f" and have played atleast {min_matches} matches"
            msg += ", found."
            return msg

    def get_trueskill_leaderboard(self, page):
        try:
            page = int(page[0])
        except (ValueError, IndexError):
            page = 0
        data = stats3.get_ladder_trueskill(self.id, page)
        if not len(data):
            return "No data"
        rows = [[
            row['row'],
            row['nick'],
            f"{row['rating']:.2f}",
            f"{row['mu']:.2f}",
            f"{row['sigma']:.2f}"
        ] for row in data]
        headers = ['№', 'nick', 'rating', 'μ', 'ς']
        return f"```{tabulate(rows, headers=headers)}```"

    def get_rank_details(self, member, args, mentions, win_loss_only=False):

        if len(mentions):
            details, matches = stats3.get_rank_details(self.id, user_id=mentions[0].id, win_loss_only=win_loss_only)
        elif len(args):
            details, matches = stats3.get_rank_details(self.id, nick=" ".join(args), win_loss_only=win_loss_only)
        else:
            details, matches = stats3.get_rank_details(self.id, user_id=member.id, win_loss_only=win_loss_only)

        if details:
            details_str = ""
            pfdetails_str = ""

            # we need to calculate spaces for neat display in case pf rank is set:
            pfplacespaces = " " * max(len(str(details['place'])) - len(str(details['pfplace'])), 0)
            smgplacespaces = " " * max(len(str(details['pfplace'])) - len(str(details['place'])), 0)

            matchnum = int(details['wins'] + details['draws'] + details['loses'])
            pfmatchnum = int(details['pfwins'] + details['pfdraws'] + details['pfloses'])
            pfmatchesspaces = " " * max(len(str(matchnum)) - len(str(pfmatchnum)), 0)
            smgmatchesspaces = " " * max(len(str(pfmatchnum)) - len(str(matchnum)), 0)

            rating_str = "" if win_loss_only else "Rating {} {} | ".format(
                details['rank'],
                utils.rating_to_icon(details['rank'], self.cfg['custom_ranks']))

            if self.cfg['panzer_role'] and details['pfplace']:
                pfrating_str = "" if (win_loss_only or not details['pfrank']) else "Rating {} {} | ".format(
                    str(details['pfrank']),
                    utils.rating_to_icon(details['pfrank'], self.cfg['custom_ranks']))

                pfdetails_str = "\nPF  | № {}{} | {} {} Matches{} | W/D/L: {}/{}/{}, {}% WR".format(
                    details['pfplace'],
                    pfplacespaces,
                    pfrating_str,
                    pfmatchnum,
                    pfmatchesspaces,
                    details['pfwins'],
                    details['pfdraws'],
                    details['pfloses'],
                    int(details['pfwr'] * 100))

                details_str = "SMG | "

            if 'mu' in details and details['mu'] is not None:
                trueskill_str = f"\nTrueSkill: " \
                                f"№ {details['ts_row_num']} " \
                                f"rating={details['mu']-3*details['sigma']:.3f} " \
                                f"μ={details['mu']:.3f} " \
                                f"ς={details['sigma']:.3f}"
            else:
                trueskill_str = ""

            details_str += "№ {} {}| {} {} Matches {}| W/D/L: {}/{}/{}, {}% WR".format(
                details['place'],
                smgplacespaces if self.cfg['panzer_role'] and details['pfplace'] else "",
                rating_str,
                matchnum,
                smgmatchesspaces if self.cfg['panzer_role'] and details['pfplace'] else "",
                details['wins'],
                details['draws'],
                details['loses'],
                int(details['wr'] * 100))
            try:
                streak = " ("+details['streak'] + " Streak)" if details['streak'][-1] in ["W", "L"] else ""
            except Exception:
                streak = ""
            s = "```**{nick:^{length}.{length}}**\n".format(nick=details['nick']+streak, length=max(len(pfdetails_str), len(details_str))-2)
            s += details_str
            s += pfdetails_str
            s += trueskill_str
            s += "\n{}".format("-" * len(details_str))
            for i in matches:
                ago = arrow.get(i['at']).humanize(locale='_en', only_distance=True)
                s += "\n({}) {} ago, {}".format(i['pickup_id'], ago, i['pickup_name'])
                if not win_loss_only:
                    s += ", {:+} rating".format(i['rank_change'])
                else:
                    s += ", {}".format('WON' if i['is_winner'] else 'LOST')
                s += " (pf)" if i['is_panzer'] else ""
            s += "```"
            client.notice(self.channel, s)

        else:
            client.notice(self.channel, "No rating data found.")

    def show_ranks_table(self):

        if not self.cfg["custom_ranks"]:
            s = "```markdown\nrating | rank\n-------------\n"
            s += "\n".join(["{0:^6} | {1:^4}".format(i[0], i[1]) for i in
                        sorted(utils.ranks.items(), key=lambda i: i[0], reverse=True)])
        else:
            s = "```markdown\nrating | rank name | symbol\n---------------------------\n"
            s += "\n".join(["{0:^6} | {1:<9} | {2:<4}".format(i[0], i[1][1], i[1][0][1:-1]) for i in
                            sorted(utils.christmas_ranks.items(), key=lambda i: i[0], reverse=True)])
        s += "```"
        client.notice(self.channel, s)

    def undo_ranks(self, member, args, access_level):
        if access_level < 1:
            client.reply(self.channel, member, "You have no right for this.")
            return

        if not len(args) or not args[0].isdigit():
            client.reply(self.channel, member, "You must specify a match id.")
            return

        if not self.cfg['hide_ranks']:
            reply = stats3.undo_ranks(self.id, int(args[0]))
        else:
            reply = "done."
        client.notice(self.channel, reply)

    def reset_ranks(self, member, access_level):
        if access_level < 2:
            client.reply(self.channel, member, "You must posses administrator rights to use this command.")
            return
        else:
            stats3.reset_ranks(self.id)
            client.reply(self.channel, member, "All rating data has been flushed.")

    def seed_player(self, member, targets, rating, access_level, panzer=False):
        if access_level < 1:
            client.reply(self.channel, member, "Insufficient permissions.")
            return

        if not len(targets) or not rating.isdecimal():
            client.reply(self.channel, member,
                         "This commands requires 2 arguments: member mentions and rating integer.")
            return

        for target in targets:
            stats3.seed_player(self.channel, target.id, target.name, int(rating), self.cfg['initial_rating'], panzer)
        client.reply(self.channel, member, "done.")

    def get_topic(self):
        sorted_pickups = sorted(self.pickups, key=lambda x: len(x.players), reverse=True)
        str_list = []
        for pickup in sorted_pickups:
            to_re_queue = [match.re_queue[self][pickup] for match in active_matches if self in match.re_queue and pickup in match.re_queue[self] and match.re_queue[self][pickup]]
            to_re_queue_flattened_len = len([item for sublist in to_re_queue for item in sublist])
            if pickup.players or to_re_queue_flattened_len:
                str_list.append("**{}** ({}/{}){}".format(
                    pickup.name,
                    len(pickup.players),
                    pickup.cfg['maxplayers'],
                    f" :repeat_one: {to_re_queue_flattened_len}" if to_re_queue_flattened_len else ""
                ))
        if len(self.scheduled_adds):
            str_list.append(f":timer: {len(self.scheduled_adds)}")
        topic = "[{}]".format(" | ".join(str_list))

        if topic == "[]":
            topic = "[**no pickups**]"
        return topic

    def update_topic(self, send_message: bool = True, phrase: Optional[str] = None):
        embed = None
        new_topic = self.get_topic()
        if new_topic != self.old_topic:
            if send_message:
                if phrase:
                    embed = Embed(description=phrase)
                client.notice(self.channel, new_topic, embed=embed)
            # client.channel_queued_notice(self.channel, 'topic', new_topic)
            self.old_topic = new_topic
            self.update_status_message('pickup_msg', "**Pickups**", new_topic)

    def format_matches_status(self):
        return ' '.join(
            ["`{} | {}`".format(match.pickup.name, match.state) for match in active_matches
             if match.pickup.channel.id == self.id]
        )

    def update_status_message(self, key, field_name, content):
        self.status_message_dict[key] = content
        for status_message in self.status_messages:
            message = status_message.discord_msg
            if message is None:
                continue
            try:
                embed = next(iter(message.embeds))
            except StopIteration:
                embed = None
            field_value = ""

            for channel in status_message.channels:
                channel_link = status_message.get_channel_link(channel, status_message.discord_msg.channel)
                if not channel.cfg['public']:
                    continue
                if key in channel.status_message_dict and channel.status_message_dict[key] != "":
                    field_value += "{} {}\n".format(channel_link, channel.status_message_dict[key])

            if embed is not None:
                found = False
                for idx, field in enumerate(embed.fields):
                    if field.name == field_name:
                        if field_value != "":
                            embed.set_field_at(idx, name=field_name, value=field_value)
                        else:
                            embed.remove_field(idx)
                        found = True
                if not found and field_value != "":
                    embed.add_field(name=field_name, value=field_value)
            else:
                embed = Embed(title="Status:").add_field(name=field_name, value=field_value)
            client.edit_message(message, None, embed)

    def reply_pickups(self, member):
        if self.pickups:
            s = []
            for i in self.pickups:
                s.append("**{0}** ({1}/{2})".format(i.name, len(i.players), i.cfg['maxplayers']))
            s = ' | '.join(s)

            client.notice(self.channel, s)
        else:
            client.notice(self.channel, "No pickups configured on this channel.")

    # next also fix !sub
    async def promote_pickup(self, member, args):
        if not len(self.pickups):
            client.reply(self.channel, member, "This channel does not have any pickups configured.")
            return

        self.newtime = time.time()
        if self.newtime - self.oldtime > int(self.cfg['promotion_delay']):
            # get pickup to promote
            pickup = None
            if args:
                for i in self.pickups:
                    if i.name.lower() == args[0]:
                        pickup = i
                        break
                if not pickup:
                    client.reply(self.channel, member,
                                 escape_mentions("Pickup '{0}' not found on this channel.".format(args[0])))
                    return

            elif len(self.pickups) == 1:
                pickup = self.pickups[0]

            else:
                pickups = sorted(self.pickups, key=lambda x: len(x.players), reverse=True)
                if len(pickups[0].players):
                    pickup = pickups[0]

            if pickup is not None:
                promotion_role = self.get_value('promotion_role', pickup)
                players_left = pickup.cfg['maxplayers'] - len(pickup.players)
                edit_role = False
                remove_role_players = []
                role_obj = None

                if promotion_role:
                    role_obj = client.find_role_by_id(self.channel, promotion_role)

                    if role_obj:
                        if not role_obj.mentionable:
                            try:
                                await client.edit_role(role_obj, mentionable=True)
                                edit_role = True
                            except:
                                pass
                        if edit_role:
                            for player in [x for x in pickup.players if role_obj in x.roles]:
                                remove_role_players.append(player)
                                await client.remove_roles(player, role_obj)

                promote_msg = self.get_value('promotemsg', pickup) or \
                              "%promotion_role% please !add %pickup_name%, %required_players% players to go!"
                if promotion_role:
                    promote_msg = promote_msg.replace("%promotion_role%", "<@&" + str(promotion_role) + ">")
                else:
                    promote_msg = promote_msg.replace("%promotion_role%", "")
                promote_msg = promote_msg.replace("%pickup_name%", pickup.name)
                promote_msg = promote_msg.replace("%required_players%", str(players_left))
                await self.channel.send(promote_msg)
                if edit_role and role_obj is not None:
                    for player in remove_role_players:
                        await client.add_roles(player, role_obj)
                    await client.edit_role(role_obj, mentionable=False)

            else:
                promote_msg = self.cfg['promotemsg'] or "%promotion_role% please !add to pickups!"
                if self.cfg["promotion_role"]:
                    promote_msg = promote_msg.replace("%promotion_role%", "<@&" + str(self.cfg["promotion_role"]) + ">")
                else:
                    promote_msg = promote_msg.replace("%promotion_role%", "")
                client.notice(self.channel, promote_msg)

            self.oldtime = self.newtime

        else:
            client.reply(self.channel, member, "You can't promote too often! You have to wait {0}.".format(
                str(datetime.timedelta(seconds=int(int(self.cfg['promotion_delay']) - self.newtime + self.oldtime)))))

    async def subscribe(self, member, args, unsub):
        if len(args) < 1:
            client.notice(self.channel, "Specify pickup(s).")
            return

        found_roles = []
        for arg in args:
            pickup = None
            for i in self.pickups:
                if i.name.lower() == arg:
                    pickup = i
                    break
            if pickup is None:
                client.reply(self.channel, member,
                             escape_mentions("Pickup '{0}' not found on this channel.".format(args[0])))
                return
            promotion_role = self.get_value('promotion_role', pickup)
            if promotion_role:
                role_obj = client.find_role_by_id(self.channel, promotion_role)
                if not role_obj:
                    client.reply(self.channel, member,
                                 "Promotion role for '{0}' pickup is not set.".format(pickup.name))
                    return

                if not (unsub ^ bool(role_obj in member.roles)):  # inverted xor =)
                    found_roles.append(role_obj)

            else:
                client.reply(self.channel, member, "Promotion role for '{0}' pickup is not set.".format(pickup.name))
                return

        if not len(found_roles):
            client.reply(self.channel, member, "No changes to apply.")
            return

        try:
            if unsub:
                await client.remove_roles(member, *found_roles)
                client.reply(self.channel, member, "Done, removed {0} roles from you.".format(len(found_roles)))
            else:
                await client.add_roles(member, *found_roles)
                client.reply(self.channel, member, "Done, added {0} roles to you.".format(len(found_roles)))
        except errors.Forbidden:
            client.reply(self.channel, member, "Insufficient server rights to do the promotion roles manipulation.")

    def is_added(self, member: Member) -> bool:
        for players in [i.players for i in self.pickups]:
            if member.id in [i.id for i in players]:
                return True
        return False

    def expire(self, member, timelist):
        if not self.is_added(member):
            client.reply(self.channel, member, "You must be added first!")
            return

        # set expire if time is specified
        if timelist:
            try:
                time_int = utils.parse_time_input(timelist)
            except Exception as e:
                client.reply(self.channel, member, str(e))
                return

            # apply given time
            if 0 < time_int <= max_expire_time:  # restart the scheduler task, no afk check task for this guy
                if member.id in scheduler.tasks.keys():
                    scheduler.cancel_task(member.id)

                def global_remove_callback():
                    global_remove(member, 'scheduler')
                scheduler.add_task(member.id, time_int, global_remove_callback)
                client.reply(self.channel, member,
                             "You will be removed in {0}".format(str(datetime.timedelta(seconds=int(time_int)))))
            else:
                client.reply(self.channel, member, "Invalid time amount. Maximum expire time is {0}".format(
                    str(datetime.timedelta(seconds=max_expire_time))))

        # return expire time	if no time specified
        else:
            if not member.id in scheduler.tasks.keys():
                client.reply(self.channel, member, "No !expire time is set. You will be removed on your AFK status.")
                return

            time_int = scheduler.tasks[member.id][0]

            client.reply(self.channel, member, "You will be removed in {0}".format(
                str(datetime.timedelta(seconds=int(time_int - time.time()))), ))

    # next
    def default_expire(self, member, time_list):
        # print user default expire time
        if not time_list:
            time_int = stats3.get_expire(member.id)
            time_int = time_int if time_int is not None else self.cfg['global_expire']
            if time_int:
                client.reply(self.channel, member,
                             "Your default expire time is {0}".format(str(datetime.timedelta(seconds=int(time_int))), ))
            else:
                client.reply(self.channel, member, "You will be removed on AFK status by default.")

        # set expire time to afk
        elif time_list[0] == 'afk':
            stats3.set_expire(member.id, 0)
            client.reply(self.channel, member, "You will be removed on AFK status by default.")

        elif time_list[0] == 'none':
            stats3.set_expire(member.id, None)
            client.reply(self.channel, member,
                         "Your default expire time will now depend on guild's global_expire value.")

        # format time string and set new time amount
        else:
            try:
                time_int = utils.parse_time_input(time_list)
            except Exception as e:
                client.reply(self.channel, member, str(e))
                return
            if 0 < time_int <= max_expire_time:
                stats3.set_expire(member.id, time_int)
                client.reply(self.channel, member, "set your default expire time to {0}".format(
                    str(datetime.timedelta(seconds=int(time_int))), ))
            else:
                client.reply(self.channel, member,
                             "Invalid time amount. Maximum expire time on this channel is {0}".format(
                                 str(datetime.timedelta(seconds=max_expire_time))))

    def switch_allowoffline(self, member):
        if member in allowoffline:
            allowoffline.remove(member)
            client.reply(self.channel, member, "Your offline/afk immune is gone.")
        else:
            allowoffline.append(member)
            client.reply(self.channel, member, "You will have offline/afk immune until your next pickup.")

    def get_stats(self, member, target):
        if not target:
            s = stats3.stats(self.id)
        else:
            s = stats3.stats(self.id, target[0])
        client.notice(self.channel, s)

    def get_activity(self, member, arg):
        pickup = False
        if len(arg):
            if arg[0] not in ["daily", "weekly", "monthly", "yearly", "by_hour"]:
                pickup = arg[0]
                arg.pop(0)

        if not arg:
            client.reply(self.channel, member, 'Usage: !activity [pickup_name] daily|weekly|monthly|yearly [page]\n'
                                               '!activity 1v1 weekly 2')
            return
        elif arg[0] == "daily":
            time_gap = 86400
        elif arg[0] == "weekly":
            time_gap = 604800
        elif arg[0] == "monthly":
            time_gap = 2629744
        elif arg[0] == "yearly":
            time_gap = 31556926
        elif arg[0] == "by_hour":
            headers = ["H", "#"]
            rows = stats3.activity_by_hour(self.id, pickup)
            msg = f"```{tabulate(rows, headers=headers, tablefmt='presto')}```"
            client.notice(self.channel, msg)
            return
        else:
            client.reply(self.channel, member, 'Specify daily, weekly, monthly or yearly as argument.')
            return

        try:
            back = int(arg[-1])
        except Exception:
            back = 0

        activity = stats3.activity(self.id, time_gap, pickup, back)
        activity = [(row["num_games"], row["uniq_players"], datetime.datetime.utcfromtimestamp(t0).strftime('%d.%m.%y'),
                     datetime.datetime.utcfromtimestamp(t1).strftime('%d.%m.%y')) for row, t0, t1 in activity]

        timeframe = [t0 + " - " + t1 for _, _, t0, t1 in activity]
        num_games = [str(num_games) for num_games, _, _, _ in activity]
        uniq_players = [str(uniq_players) for _, uniq_players, _, _ in activity]
        display_timeframe = "\n".join(timeframe)
        display_num_games = "\n".join(num_games)
        display_uniq_players = "\n".join(uniq_players)

        embed = Embed()
        embed.add_field(name="Timeframe", value=display_timeframe, inline=True)
        embed.add_field(name="Matches", value=display_num_games, inline=True)
        embed.add_field(name="Uniq. players", value=display_uniq_players, inline=True)

        if activity:
            client.notice(self.channel, '', embed=embed)
        else:
            client.reply(self.channel, member, "Nothing found.")

    def get_top_maps(self, member, arg):
        pickup = False
        if len(arg):
            if arg[0] not in ["daily", "weekly", "monthly", "yearly"]:
                pickup = arg[0]
                arg.pop(0)

        if not arg:
            time_gap = False
        elif arg[0] == "daily":
            time_gap = 86400
        elif arg[0] == "weekly":
            time_gap = 604800
        elif arg[0] == "monthly":
            time_gap = 2629744
        elif arg[0] == "yearly":
            time_gap = 31556926
        else:
            client.reply(self.channel, member, "Bad argument.")
            return
        if time_gap:
            try:
                back = int(arg[-1][1:])
            except Exception:
                back = 0
        else: back = 0

        top = stats3.top_maps(self.id, time_gap, pickup, back)
        sorted_top = sorted(top.items(), key = lambda x:x[1], reverse = True)[:20] # 20 most played maps for display

        display_string = "\n".join([str(i) + " : " + str(j) for i,j in sorted_top])

        embed = Embed()
        descriptor = "Most played maps "
        if arg:
            if back > 1:
                if arg[0] == "daily":
                    descriptor += str(back)
                    descriptor += " days ago"
                elif arg[0] == "weekly":
                    descriptor += str(back)
                    descriptor += " weeks ago"
                elif arg[0] == "monthly":
                    descriptor += str(back)
                    descriptor += " months ago"
                elif arg[0] == "yearly":
                    descriptor += str(back)
                    descriptor += " years ago"
            elif back == 1:
                if arg[0] == "daily":
                    descriptor += str(back)
                    descriptor += " day ago"
                elif arg[0] == "weekly":
                    descriptor += str(back)
                    descriptor += " week ago"
                elif arg[0] == "monthly":
                    descriptor += str(back)
                    descriptor += " month ago"
                elif arg[0] == "yearly":
                    descriptor += str(back)
                    descriptor += " year ago"
            elif back == 0:
                if arg[0] == "daily":
                    descriptor += "today"
                elif arg[0] == "weekly":
                    descriptor += "this week"
                elif arg[0] == "monthly":
                    descriptor += "this month"
                elif arg[0] == "yearly":
                    descriptor += "this year"
        embed.add_field(name=descriptor, value=display_string, inline=False)

        if top:
            client.notice(self.channel, '', embed=embed)
        else:
            client.reply(self.channel, member, "Nothing found.")

    def get_top(self, member, arg):
        pickup = False
        if len(arg):
            if arg[0] not in ["daily", "weekly", "monthly", "yearly"]:
                pickup = arg[0]
                arg.pop(0)

        if not arg:
            time_gap = False
            reply = "Top 10 of all time"
        elif arg[0] == "daily":
            time_gap = int(time.time()) - 86400
            reply = "Top 10 of the day"
        elif arg[0] == "weekly":
            time_gap = int(time.time()) - 604800
            reply = "Top 10 of the week"
        elif arg[0] == "monthly":
            time_gap = int(time.time()) - 2629744
            reply = "Top 10 of the month"
        elif arg[0] == "yearly":
            time_gap = int(time.time()) - 31556926
            reply = "Top 10 of the year"
        else:
            client.reply(self.channel, member, "Bad argument.")
            return

        top10 = stats3.top(self.id, time_gap, pickup)
        if top10:
            if pickup:
                client.reply(self.channel, member, "{0} for {1}: {2}".format(reply, pickup, stats3.format_top(top10)))
            else:
                client.reply(self.channel, member, "{0}: {1}".format(reply, stats3.format_top(top10)))
        else:
            client.reply(self.channel, member, "Nothing found.")

    def getnoadds(self, member, args, mentions):
        usage = "!noadds [-perm] [member_highlight] [page_index]"
        index = None
        permanent = stats3.TEMP_ONLY
        if not args:
            l = stats3.noadds(self.id, None, None, stats3.TEMP_ONLY)
        else:
            if args[0] == '-perm':
                permanent = stats3.PERM_ONLY
                args.pop(0)
            else:
                permanent = stats3.TEMP_ONLY

            if len(mentions):
                user_id = mentions[0].id
                args.pop(0)
            else:
                user_id = None

            def index_error_reply():
                client.reply(self.channel, member, "Index argument must be a positive number. \n" + usage)

            try:
                index = int(args[0])
            except ValueError:
                index_error_reply()
                return
            except IndexError:
                index = None

            if index is not None and index < 0:
                index_error_reply()
                return

            l = stats3.noadds(self.id, index, user_id, permanent)
        if l:
            client.notice(self.channel, "\n".join(l))
        else:
            if permanent == stats3.PERM_ONLY:
                noadd_type_str = "permanent "
            elif permanent == stats3.TEMP_ONLY:
                noadd_type_str = "temporary "
            else:
                noadd_type_str = ""
            client.reply(self.channel, member, "No {}{}noadds found.".format(
                "active " if index is None else "",
                noadd_type_str
            ))

    def set_ao_for_all(self, member, targs, access_level):
        if access_level <= 1:
            client.reply(self.channel, member, "You have no right for this!")
            return

        if len(targs) < 2:
            client.reply(self.channel, member, "usage: !set_ao_for_all <pickup_name> 0|1")
            return

        pickup_name, ao = targs
        ao = int(ao)
        try:
            pickup = self.find_pickup(pickup_name)
        except ValueError as e:
            client.reply(self.channel, member, str(e))
            return

        self.update_pickup_config(pickup, 'allow_offline', ao)

        client.reply(self.channel, member, 'Offline is {} for all {} pickups'.format(
            'allowed' if ao else 'disallowed by default',
            pickup_name
        ))

    # next
    def add_pickups(self, member, targs, access_level):
        if access_level > 1:
            new_pickups = []
            for i in range(0, len(targs)):
                try:
                    name, players = targs[i].split(":")
                    if int(players) > 1:
                        if name.lower() not in [i.name.lower() for i in self.pickups]:
                            new_pickups.append([name, int(players)])
                        else:
                            client.reply(self.channel, member, "Pickup with name '{0}' allready exists!".format(name))
                            return
                    else:
                        client.reply(self.channel, member, "Players number must be more than 1, dickhead.")
                        return
                except:
                    client.reply(self.channel, member, "Bad argument @ {0}".format(targs[i]))
                    return
            if new_pickups:
                for i in new_pickups:
                    cfg = stats3.new_pickup(self.id, i[0], i[1])
                    self.pickups.append(Pickup(self, cfg))
                self.reply_pickups(member)
        else:
            client.reply(self.channel, member, "You have no right for this!")

    def remove_pickups(self, member, args, access_level):
        if access_level > 1:
            to_remove = [pickup for pickup in self.pickups if pickup.name.lower() in args]
            if len(to_remove) > 0:
                for i in to_remove:
                    self.pickups.remove(i)
                    stats3.delete_pickup(self.id, i.name)
                self.reply_pickups(member)
            else:
                client.reply(self.channel, member, "No such pickups found.")
        else:
            client.reply(self.channel, member, "You have no right for this!")

    def add_pickup_group(self, member, args, access_level):
        if access_level > 1:
            if len(args) > 1:
                group_name = args[0]
                desired_pickup_names = args[1:len(args)]
                pickup_names = [i.name.lower() for i in self.pickups]
                for i in desired_pickup_names:
                    if i not in pickup_names:
                        client.reply(self.channel, member, "Pickup '{0}' not found.".format(i))
                        return
                if group_name in pickup_names:
                    client.reply(self.channel, member, "Group name can not match any of pickup names.")
                    return
                if group_name in self.pickup_groups.keys():
                    stats3.delete_pickup_group(self.id, group_name)
                self.pickup_groups[group_name] = desired_pickup_names
                stats3.new_pickup_group(self.id, group_name, desired_pickup_names)
                self.show_pickup_groups()
            else:
                client.reply(self.channel, member, "This command requires more arguments.")
        else:
            client.reply(self.channel, member, "You have no right for this!")

    def remove_pickup_group(self, member, args, access_level):
        if access_level > 1:
            if len(args) > 0:
                if args[0] in self.pickup_groups:
                    stats3.delete_pickup_group(self.id, args[0])
                    self.pickup_groups.pop(args[0])
                    client.reply(self.channel, member, "Pickup group '{0}' deleted.".format(args[0]))
                else:
                    client.reply(self.channel, member, "Pickup group '{0}' not found.".format(args[0]))
            else:
                client.reply(self.channel, member, "You must specify the pickup group name")
        else:
            client.reply(self.channel, member, "You have no right for this!")

    def show_pickup_groups(self):
        if not len(self.pickup_groups):
            client.notice(self.channel, "No pickup groups is configured on this channel.")
            return
        msg = "Pickup groups:"
        for i in self.pickup_groups.keys():
            msg += "\r\n**{0}**: [{1}]".format(i, ", ".join(self.pickup_groups[i]))
        client.notice(self.channel, msg)

    def show_maps(self, member, args, pick):
        if len(args):
            pickup_name = args[0].lower()
            for pickup in self.pickups:
                if pickup.name.lower() == pickup_name:
                    maps = self.get_value('maps', pickup)
                    best_of = self.get_value('best_of', pickup)

                    if not maps:
                        client.reply(self.channel, member, "No maps set for **{0}** pickup".format(pickup.name))
                    else:
                        map_list = [_map.strip() for _map in maps.split(',')]
                        if pick:
                            client.notice(self.channel, "**{0}**".format(', '.join(random.sample(map_list, best_of))))
                        else:
                            if pickup.cfg["map_probabilities"] and not pickup.cfg['map_pick_order'] == "mappref":
                                probs = pickup.cfg['map_probabilities']
                                probs = [float(prob.strip()) for prob in probs.split(",")]
                                total_probs = sum(probs)
                                maps_probs = ", ".join(
                                    [_map + " (" + str(int(100 * prob / total_probs)) + "%)" for _map, prob in
                                     zip(map_list, probs)])
                                warning = "Warning: length of map_probabilities and maps is different" if len(
                                    probs) != len(map_list) else ""
                                client.notice(self.channel, f"{warning}\nMaps for [**{pickup.name}**]: {maps_probs}.")
                            else:
                                client.notice(self.channel, "Maps for [**{0}**]: {1}.".format(pickup.name, maps))
                    return
            client.reply(self.channel, member, "Pickup '{0}' not found!".format(args[0]))
        elif self.cfg['maps']:
            map_list = self.cfg['maps'].split(', ')
            best_of = self.cfg['best_of']
            if pick:
                client.notice(self.channel, "**{0}**".format(', '.join(random.sample(map_list, best_of))))
            else:
                client.reply(self.channel, member, "Default maps: {0}".format(self.cfg['maps']))
        else:
            client.reply(self.channel, member, "No default maps are set")

    def get_ip(self, member, args):  # GET IP FOR GAME
        # find desired parameter
        pickup = None
        if args:
            for i in self.pickups:
                if i.name.lower() == args[0]:
                    pickup = i
                    break
            if not pickup:
                client.reply(self.channel, member,
                             escape_mentions("Pickup '{0}' not found on this channel.".format(args[0])))
                return
        else:
            if self.lastgame_pickup:
                pickup = self.lastgame_pickup
            else:
                client.reply(self.channel, member, "No pickups played yet.")

        if pickup is not None:
            ip = self.get_value('ip', pickup)
            password = self.get_value('password', pickup)
            if ip:
                reply = "Ip for {0}: {1}".format(pickup.name, ip)
                if password:
                    reply += "\r\nPassword: '{0}'.".format(password)
            else:
                reply = "No ip is set for {0} pickup.".format(pickup.name)
            client.notice(self.channel, reply)

    # TODO: apply multiple mentions
    def set_phrase(self, member, mentions, args, access_level):
        if access_level:
            if len(args) >= 2:
                if len(mentions):
                    target = mentions[0]
                    if target is not None:
                        phrase = ' '.join(args[1:len(args)])
                        if phrase.lower() == "none":
                            phrase = None
                            client.reply(self.channel, member, "Phrase has been removed.")
                        else:
                            client.reply(self.channel, member, "Phrase has been set.")
                        stats3.set_phrase(self.id, target.id, phrase)
                    else:
                        client.reply(self.channel, member,
                                     "Could not found specified Member on the server, is the highlight valid?")
                else:
                    client.reply(self.channel, member, "Target must be a Member highlight.")
            else:
                client.reply(self.channel, member, "This command needs more arguments.")
        else:
            client.reply(self.channel, member, "You have no right for this!")

    async def noadd(self, member, mentions, args, access_level, _channels=None):
        usage = "!noadd [-anon] [-perm] (member_mention|member_id)+ [(channel_mention)+] [duration] [reason]"
        try:
            args.pop(args.index("-anon"))
            by = 'anonymous admin'
        except ValueError:
            by = member.name

        try:
            args.pop(args.index("-perm"))
            permanent = True
        except ValueError:
            permanent = False

        targets = []
        if mentions:
            args = args[len(mentions):]
            targets = mentions
        else:
            while len(args):
                try:
                    member_id = int(args[0])
                    args.pop(0)
                except ValueError:
                    break
                try:
                    targets.append(await self.channel.guild.fetch_member(member_id))
                except errors.NotFound:
                    client.reply(self.channel, member, "member {} not found \n {}".format(member_id, usage))
                    return

        if not targets:
            client.reply(self.channel, member, "No targets specified." + "\n" + usage)
            return
        if _channels:
            args = args[len(_channels):]
            # channels - global containing all loaded bot.Channels
            try:
                target_channels = [next(channel for channel in channels
                                        if channel.id == _channel.id and channel.guild.id == _channel.guild.id)
                                   for _channel in _channels]
            except StopIteration:
                client.reply(self.channel, member, "I don't use this channel")
                return
        else:
            target_channels = [self]
        reason = ''
        duration = self.cfg['default_bantime']

        s = None
        summary = []
        for target in targets:
            if permanent:
                duration = -1
            else:
                time_list = []
                while len(args):
                    if re.match(r"[0-9]+(d|h|m|s)", args[0].lower()):
                        time_list.append(args.pop(0).lower())
                    else:
                        break

                if len(time_list):
                    duration = utils.parse_time_input(time_list)

            if len(args):
                reason = " ".join(args)

            if abs(duration) > max_bantime:
                client.reply(self.channel, member,
                             "Max ban duration is {}.".format(str(datetime.timedelta(seconds=max_bantime))))
                return

            for channel in target_channels:
                # check access_level in target channels
                if _channels is not None and channel.get_member_access_level(member) == 0:
                    client.reply(self.channel, member, "You have no right for this!")
                    return
                channel.remove_player(target, [], 'banned')
                s = stats3.noadd(channel.id, target.id, target.name, duration, by, reason)
                if _channels:
                    summary.append(s[0])
                try:
                    party = Party.find_party_by_player(target, channel)
                    party.remove_player(target)
                except NotInPartyException:
                    pass
                client.notice(channel.channel, f"{s[0]} {s[1]}")
        if _channels and summary:
            client.notice(self.channel, '\n'.join(summary))

    def forgive(self, member, mentions, access_level):
        if access_level:
            if len(mentions):
                for target in mentions:
                    s = stats3.forgive(self.id, target.id, target.name, member.name)
                    client.reply(self.channel, member, s)
            else:
                client.reply(self.channel, member, "Target must be a Member highlight.")
        else:
            client.reply(self.channel, member, "You have no right for this!")

    async def report_match_manually(self, member, arg, access_level):
        if len(arg) < 5:
            raise PubobotException("usage: !report_match <pickup_name> <time: %m.%d.%y-%H:%M> <winning_team>"
                                   " <team1 mentions> <team2 mentions> [pf]")
        pickup_name = arg[0]
        time_at = mktime(datetime.datetime.strptime(arg[1], '%d.%m.%y-%H:%M').timetuple())
        winner = arg[2]
        if winner not in ["alpha", "beta", "draw"]:
            raise PubobotException("winner argument can only be alpha, beta or draw")
        if arg[-1] in ["pf", "pfs", "panzer", "panzers"]:
            panzers = True
            players = arg[3:-1]
        else:
            panzers = False
            players = arg[3:]
        discord_players = []

        def parse_json_player(string, players):
            player = loads(string)

            class MemberMock:
                def __init__(self, d):
                    self.__dict__ = d

            try:
                if not isinstance(player['id'], int):
                    return None
                if not isinstance(player['nick'], str):
                    return None
                players.append(MemberMock(player))
                return True
            except KeyError:
                return None

        if access_level:
            for player in players:
                if re.match("^<@(!|)[0-9]+>$", player):
                    discord_player = await client.get_member_by_id(self.channel, player)
                    if discord_player is not None:
                        discord_players.append(discord_player)
                    else:
                        client.reply(self.channel, member,
                                     "Could not found specified Member on the server, is the highlight valid?")
                        return
                elif parse_json_player(player, discord_players) is not None:
                    pass
                else:
                    client.reply(self.channel, member, 'Target must be a Member highlight or {"id":123,"nick":"nick"}.')
                    return
            pickup = self.find_pickup(pickup_name)

            match = Match(pickup, discord_players, winner)
            if panzers:
                match.panzers = [match.alpha_team[0], match.beta_team[0]]
            else:
                match.panzers = []
            match.finish_match(time_at)
        else:
            client.reply(self.channel, member, "You have no right for this!")

    def reset_players(self, member=None, args=None, access_level=0, comment=False):
        if args is None:
            args = []
        if member is None or access_level:
            removed = []
            for pickup in self.pickups:
                if len(pickup.players) and (pickup.name in args or args == []):
                    for player in pickup.players:
                        if player not in removed:
                            removed.append(player)
                    pickup.players = []
                    active_pickups.remove(pickup)
            if removed:
                for player in removed:
                    all_pickups = True
                    for pickup in active_pickups:
                        if player in pickup.players:
                            all_pickups = False
                            break
                    if all_pickups and player.id in scheduler.tasks.keys():
                        scheduler.cancel_task(player.id)
                if not args:
                    client.notice(self.channel, "{0} was removed from all pickups!".format(
                        '<@' + ', <@'.join([str(i.id) + '>' for i in removed])))
                elif len(args) == 1:
                    client.notice(self.channel, "{0} was removed from {1} pickup!".format(
                        '<@' + ', <@'.join([str(i.id) + '>' for i in removed]), args[0]))
                else:
                    client.notice(self.channel, "{0} was removed from {1} pickups!".format(
                        '<@' + ', <@'.join([str(i.id) + '>' for i in removed]), ', '.join(args)))
                self.update_topic()
                if comment:
                    client.notice(self.channel, comment)
        else:
            client.reply(self.channel, member, "You have no right for this!")

    def reset_stats(self, member, access_level):
        if access_level > 1:
            stats3.reset_stats(self.id)
            client.reply(self.channel, member, "Done.")
        else:
            client.reply(self.channel, member, "You have no right for this!")

    def help_answer(self, member, args):
        if args:
            answer = None
            for p in self.pickups:
                if args[0] == p.name.lower():
                    answer = self.get_value('help_answer', p)
        else:
            try:
                answer = self.cfg['help_answer']
            except KeyError:
                client.reply(self.channel, member, config.cfg.COMMANDS_LINK)
                return
        if answer:
            client.notice(self.channel, answer)

    def get_value(self, variable, pickup):
        if pickup is not None and pickup.cfg[variable] is not None:
            return pickup.cfg[variable]
        else:
            return self.cfg[variable]

    def update_channel_config(self, variable, value):
        self.cfg[variable] = value
        stats3.update_channel_config(self.id, variable, value)

    def update_pickup_config(self, pickup, variable, value):
        pickup.cfg[variable] = value
        stats3.update_pickup_config(self.id, pickup.name, variable, value)

    def role_name_from_id(self, role_id):
        role = self.channel.guild.get_role(role_id)
        if role is not None:
            return role.name
        else:
            return 'role not found'

    def format_variable(self, key, value) -> str:
        rv = f"{key}: {str(value)}"
        if value is not None:
            if '_role' in key:
                rv += ' -> ' + self.role_name_from_id(value)

            elif key == 'admin_id':
                rv += ' '
                admin = self.channel.guild.get_member(value)
                if admin is not None:
                    rv += admin.nick or admin.name
                else:
                    rv += 'admin member not found'
        return rv

    def get_cfg_extension(self, key: str, pickup=None, default=None):
        extension_str = self.get_value('extension', pickup)
        if extension_str is None:
            return default
        json_dist = json.loads(extension_str)
        if key not in json_dist:
            return default
        return json_dist[key]

    def show_config(self, member, args):
        if len(args):
            if args[0] in self.cfg.keys():
                client.private_reply(member, self.channel.mention + ' ' + self.format_variable(args[0], self.cfg[args[0]]))
            else:
                client.reply(self.channel, member, "No such variable '{0}'.".format(args[0]))
        else:
            client.private_reply(member, self.channel.mention + '\n' + '\n'.join(
                [self.format_variable(key, value) for (key, value) in self.cfg.items()]))

    def show_prefs(self, member):
        prefs = stats3.get_pref_maps(self.id, member.id)
        mapdict = {a: b for a, b in [pref.split(":") for pref in prefs.split(",")]} if prefs else dict()
        message = '\r\n'.join(
            ["{0}: '{1}'".format(key, str(value)) for (key, value) in mapdict.items()])
        if message == "":
            message = f"you haven't set any map preferences yet, use `{self.cfg['prefix']}mappref` command to set them"
        client.private_reply(member, message)

    def show_pickup_config(self, member, args):
        if len(args):
            args[0] = args[0].lower()
            for pickup in self.pickups:
                if pickup.name.lower() == args[0]:
                    if len(args) > 1:
                        if args[1] in pickup.cfg.keys():
                            client.private_reply(member, f"{self.channel.mention} [{pickup.name}] {self.format_variable(args[1], pickup.cfg[args[1]])}")
                            return
                        else:
                            client.reply(self.channel, member, "No such variable '{0}'.".format(args[1]))
                            return
                    else:
                        client.private_reply(member, f"{self.channel.mention} [{pickup.name}]\n" + '\n'.join(
                            [self.format_variable(key, value) for (key, value) in
                             pickup.cfg.items()]))
                        return
            client.reply(self.channel, member, "Pickup '{0}' not found.".format(args[0]))
        else:
            client.reply(self.channel, member, "You must specify a pickup")

    def configure_default(self, member, args, access_level):
        if len(args) < 2:
            client.reply(self.channel, member,
                         f"usage: `{self.cfg['prefix']}set <variable> <value>`")
            return

        if access_level < 2:
            client.reply(self.channel, member, "You have no right for this!")
            return

        variable = args.pop(0).lower()
        value = " ".join(args)

        if variable == "admin_role":
            if value.lower() == "none":
                client.reply(self.channel, member, "Cant unset {0} value.".format(variable))
            else:
                role = client.find_role_by_name(self.channel, value)
                if role:
                    self.update_channel_config('admin_role', role.id)
                    client.reply(self.channel, member, "Set '{0}' {1} as default value".format(role.name, variable))
                else:
                    client.reply(self.channel, member, "Role '{0}' not found on this discord server".format(value))

        elif variable == "moderator_role":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                role = client.find_role_by_name(self.channel, value)
                if role:
                    self.update_channel_config(variable, role.id)
                    client.reply(self.channel, member, "Set '{0}' {1} as default value".format(role.name, variable))
                else:
                    client.reply(self.channel, member, "Role '{0}' not found on this discord server".format(value))

        elif variable == "captains_role":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                role = client.find_role_by_name(self.channel, value)
                if role:
                    self.update_channel_config(variable, role.id)
                    client.reply(self.channel, member, "Set '{0}' {1} as default value".format(role.name, variable))
                else:
                    client.reply(self.channel, member, "Role '{0}' not found on this discord server".format(value))

        elif variable == "panzer_role":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                role = client.find_role_by_name(self.channel, value)
                if role:
                    self.update_channel_config(variable, role.id)
                    client.reply(self.channel, member, "Set '{0}' {1} as default value".format(role.name, variable))
                else:
                    client.reply(self.channel, member, "Role '{0}' not found on this discord server".format(value))

        elif variable == "prefix":
            if value.lower() == "none":
                client.reply(self.channel, member, "Cant unset {0} value.".format(variable))
            else:
                if len(value) == 1:
                    self.update_channel_config(variable, value)
                    client.reply(self.channel, member,
                                 "Set '{0}' prefix for all commands on this channel.".format(value))
                else:
                    client.reply(self.channel, member, "Prefix must be one symbol.")

        elif variable == "default_bantime":
            if value.lower() == "none":
                client.reply(self.channel, member, "Cant unset {0} value.".format(variable))
            else:
                try:
                    seconds = utils.parse_time_input(value.split(" "))
                except Exception as e:
                    client.reply(self.channel, member, str(e))
                    return
                if seconds <= max_bantime:
                    self.update_channel_config(variable, seconds)
                    client.reply(self.channel, member, "Set '{0}' {1} as default value".format(seconds, variable))
                else:
                    client.reply(self.channel, member, "Maximum bantime is 30 days.")

        elif variable == "++_req_players":
            if value.lower() == "none":
                client.reply(self.channel, member, "Cant unset {0} value.".format(variable))
            else:
                try:
                    number = int(value)
                except ValueError:
                    client.reply(self.channel, member, "Value must be a number")
                    return
                if 0 <= number < 50:
                    self.update_channel_config(variable, number)
                    client.reply(self.channel, member, "Done.")
                else:
                    client.reply(self.channel, member, "++_req_players number must be a positive number less than 50.")

        elif variable == "startmsg":
            if value.lower() == "none":
                client.reply(self.channel, member, "Cant unset {0} value.".format(variable))
            else:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))

        elif variable == "help_answer":
            if value.lower() == "none":
                client.reply(self.channel, member, "Cant unset {0} value.".format(variable))
            else:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))

        elif variable == "start_pm_msg":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))

        elif variable == "submsg":
            if value.lower() == "none":
                client.reply(self.channel, member, "Cant unset {0} value.".format(variable))
            else:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))

        elif variable == "promotemsg":
            if value.lower() == "none":
                client.reply(self.channel, member, "Cant unset {0} value.".format(variable))
            else:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))

        elif variable == "ip":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))

        elif variable == "password":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))

        elif variable == "maps":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))

        elif variable == "custom_ranks":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))


        elif variable == "best_of":
            try:
                value = int(value)
                if value < 1:
                    raise ValueError
            except ValueError:
                client.reply(self.channel, member, "best_of must be positive integer")
            else:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))

        elif variable == "team_emojis":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            elif len(value.split(' ')) == 2:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))
            else:
                client.reply(self.channel, member, "{0} value must be exactly two emojis.".format(variable))

        elif variable == "team_names":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            elif len(value.split(' ')) == 2:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))
            else:
                client.reply(self.channel, member, "{0} value must be exactly two words.".format(variable))

        elif variable == "pick_teams":
            allowed_values = ["no_teams", "manual", "auto", "autortcw"]
            value = value.lower()
            if value in allowed_values:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))
            else:
                client.reply(self.channel, member,
                             f"pick_teams value must be one of: {', '.join(allowed_values)}")

        elif variable == "pick_captains":
            _range = [str(v) for v in range(7)]
            if value in _range:
                self.update_channel_config(variable, int(value))
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))
            else:
                client.reply(self.channel, member, f"pick_captains value must be {', '.join(_range)}")

        elif variable == "ranked":
            if value in ["0", "1"]:
                self.update_channel_config(variable, bool(int(value)))
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))
            else:
                client.reply(self.channel, member, "ranked value must be 0 or 1.")

        elif variable == "party":
            if value in ["0", "1"]:
                self.update_channel_config(variable, bool(int(value)))
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))
            else:
                client.reply(self.channel, member, "party value must be 0 or 1.")

        elif variable == "ranked_calibrate":
            if value in ["0", "1"]:
                self.update_channel_config(variable, bool(int(value)))
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))
            else:
                client.reply(self.channel, member, "ranked_calibrate value must be none, 0 or 1.")

        elif variable == "ranked_streaks":
            if value in ["0", "1", "2"]:
                self.update_channel_config(variable, int(value))
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))
            else:
                client.reply(self.channel, member, "ranked_streaks value must be 0, 1 or 2.")

        elif variable == "ranked_multiplayer":
            if value.lower() == "none":
                client.reply(self.channel, member, "Cant unset {0} value.".format(variable))
            else:
                try:
                    number = int(value)
                except ValueError:
                    client.reply(self.channel, member, "Value must be a number")
                else:
                    if 0 <= number < 256:
                        self.update_channel_config(variable, number)
                        client.reply(self.channel, member, "Done.")
                    else:
                        client.reply(self.channel, member,
                                     "ranked_multiplayer number must be a number between 0 and 256.")

        elif variable == "hide_ranks":
            if value in ["0", "1", "2"]:
                self.update_channel_config(variable, int(value))
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))
            else:
                client.reply(self.channel, member, "hide_ranks value must be 0,1 or 2.")

        elif variable == "initial_rating":
            if value.lower() == "none":
                client.reply(self.channel, member, "Cant unset {0} value.".format(variable))
            else:
                try:
                    number = int(value)
                except ValueError:
                    client.reply(self.channel, member, "Value must be a number")
                else:
                    if 0 < number < 3000:
                        self.update_channel_config(variable, number)
                        client.reply(self.channel, member, "Done.")
                    else:
                        client.reply(self.channel, member, "initial_rating  must be a number between 1 and 3000.")

        elif variable == "promotion_role":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                role = client.find_role_by_name(self.channel, value)
                if role:
                    self.update_channel_config(variable, role.id)
                    client.reply(self.channel, member, "Set '{0}' {1} as default value".format(role.name, variable))
                else:
                    client.reply(self.channel, member, "Role '{0}' not found on this discord server".format(value))

        elif variable == "promotion_delay":
            if value.lower() == "none":
                client.reply(self.channel, member, "Cant unset {0} value.".format(variable))
            else:
                try:
                    seconds = utils.parse_time_input(value.split(" "))
                except Exception as e:
                    client.reply(self.channel, member, str(e))
                    return
                self.update_channel_config(variable, seconds)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(seconds, variable))

        elif variable == "blacklist_role":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                role = client.find_role_by_name(self.channel, value)
                if role:
                    self.update_channel_config(variable, role.id)
                    client.reply(self.channel, member, "Set '{0}' {1} as default value".format(role.name, variable))
                else:
                    client.reply(self.channel, member, "Role '{0}' not found on this discord server".format(value))

        elif variable == "whitelist_role":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                role = client.find_role_by_name(self.channel, value)
                if role:
                    self.update_channel_config(variable, role.id)
                    client.reply(self.channel, member, "Set '{0}' {1} as default value".format(role.name, variable))
                else:
                    client.reply(self.channel, member, "Role '{0}' not found on this discord server".format(value))

        elif variable == "require_ready":
            if value.lower() == 'none':
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                try:
                    seconds = utils.parse_time_input(value.split(" "))
                except Exception as e:
                    client.reply(self.channel, member, str(e))
                    return
                self.update_channel_config(variable, seconds)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(seconds, variable))

        elif variable == "match_livetime":
            if value.lower() == 'none':
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                try:
                    seconds = utils.parse_time_input(value.split(" "))
                except Exception as e:
                    client.reply(self.channel, member, str(e))
                    return
                if 59 < seconds < 259201:
                    self.update_channel_config(variable, seconds)
                    client.reply(self.channel, member, "Set '{0}' {1} as default value".format(seconds, variable))
                else:
                    client.reply(self.channel, member,
                                 "match_livetime value must be more than 60 seconds and less than 3 days.")

        elif variable == "global_expire":
            if value.lower() == 'none':
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            elif value.lower() == 'afk':
                self.update_channel_config(variable, 0)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                try:
                    seconds = utils.parse_time_input(value.split(" "))
                except Exception as e:
                    client.reply(self.channel, member, str(e))
                    return
                self.update_channel_config(variable, seconds)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(seconds, variable))
        elif variable == "servers":
            game_server.set_servers_default(self, member, variable, value)

        elif variable == "whitelist_msg":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))

        elif variable == "blacklist_msg":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))

        elif variable == "public":
            if value.lower() == "none":
                self.update_channel_config(variable, None)
                client.reply(self.channel, member, "Removed {0} default value".format(variable))
            else:
                try:
                    number = int(value)
                except ValueError:
                    client.reply(self.channel, member, "Value must be a number")
                else:
                    if 0 <= number <= 2:
                        self.update_channel_config(variable, number)
                        client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))
                    else:
                        client.reply(self.channel, member, variable + " must be a number between 0 and 2.")

        elif variable == "requeue_random":
            try:
                number = int(value)
            except ValueError:
                client.reply(self.channel, member, "Value must be a number")
            else:
                if 0 <= number <= 1:
                    self.update_channel_config(variable, number)
                    client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))
                else:
                    client.reply(self.channel, member, variable + " must be 0 or 1.")

        elif variable == "extension":
            try:
                json.loads(value)
                self.update_channel_config(variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(value, variable))
            except ValueError as e:
                client.reply(self.channel, member, repr(e))

        elif variable == "min_matches_leaderboard":
            try:
                number = int(value)
                if number < 1:
                    raise ValueError
                self.update_channel_config(variable, number)
                client.reply(self.channel, member, "Set '{0}' {1} as default value".format(number, variable))
            except ValueError:
                client.reply(self.channel, member, "min_matches_leaderboard must positive be integer")

        elif variable == "disallowed_cmds":
            if value.lower() == "none":
                disallowed_cmds = set()
            else:
                disallowed_cmds = set(split(value))
            self.update_channel_config(variable, value)
            self.cfg[variable] = disallowed_cmds
            client.reply(self.channel, member, f"Set {', '.join(['`'+cmd+'`' for cmd in disallowed_cmds])} as disallowed commands")

        else:
            client.reply(self.channel, member, "Variable '{0}' is not configurable.".format(variable))

    def configure_pickups(self, member, args, access_level):
        if len(args) < 3:
            client.reply(self.channel, member,
                         f"usage: `{self.cfg['prefix']}set_pickups <pickup_name> <variable> <value>`")
            return

        if access_level < 2:
            client.reply(self.channel, member, "You have no right for this!")
            return

        # determine pickup names, variable name, and value
        pickups = []
        variable = False
        for i in list(args):
            args.remove(i)
            if i != "":
                i = i.strip().lower()
                f = list(filter(lambda x: x.name.lower() == i, self.pickups))
                if len(f):
                    pickups.append(f[0])
                else:
                    variable = i.lower()
                    break

        if not variable:
            client.reply(self.channel, member, "You must specify a variable.")
            return
        value = " ".join(args)

        if not len(pickups):
            client.reply(self.channel, member, "No specified pickups found")
            return

        # configure!
        if variable == "maxplayers":
            if value.lower() == "none":
                client.reply(self.channel, member, "Cant unset {0} value.".format(variable))
            else:
                try:
                    value = int(value)
                except:
                    client.reply(self.channel, member, "Maxplayers value must be a number.")
                    return
                if 1 < value < 101:
                    for i in pickups:
                        self.update_pickup_config(i, variable, value)
                    client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable,
                                                                                               ", ".join(i.name for i in
                                                                                                         pickups)))
                else:
                    client.reply(self.channel, member, "maxplayers value must be between 2 and 100")

        elif variable == "minplayers":
            pass

        elif variable == "startmsg":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "help_answer":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "start_pm_msg":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "submsg":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "promotemsg":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "ip":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "password":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "maps":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "team_emojis":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            elif len(value.split(' ')) == 2:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))
            else:
                client.reply(self.channel, member, "{0} value must be exactly two emojis.".format(variable))

        elif variable == "team_names":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            elif len(value.split(' ')) == 2:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))
            else:
                client.reply(self.channel, member, "{0} value must be exactly two words.".format(variable))

        elif variable == "pick_teams":
            value = value.lower()
            allowed_values = ["no_teams", "manual", "auto", "autortcw"]
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            elif value in allowed_values:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))
            else:
                client.reply(self.channel, member,
                             f"pick_teams value must be one of: {', '.join(['none'] + allowed_values)}.")

        elif variable == "pick_captains":
            _range = [str(v) for v in range(7)]
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            elif value in _range:
                for i in pickups:
                    self.update_pickup_config(i, variable, int(value))
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))
            else:
                client.reply(self.channel, member, f"pick_captains value must be none or {', '.join(_range)}")

        elif variable == "ranked":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            elif value in ["0", "1"]:
                for i in pickups:
                    self.update_pickup_config(i, variable, bool(int(value)))
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))
            else:
                client.reply(self.channel, member, "ranked value must be none, 0 or 1.")

        elif variable == "best_of":
            try:
                value = int(value)
                if value < 1:
                    raise ValueError
            except ValueError:
                client.reply(self.channel, member, "best_of must positive be integer")
            else:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "pick_order":
            value = value.lower()
            if value == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "Disabled {0} for {1} pickups.".format(variable, ", ".join(i.name for i in pickups)))

            else:
                if len(pickups) > 1:
                    client.reply(self.channel, member,
                                 "Only one pickup at time is supported for this variable configuration.")
                    return
                if len(value) != pickups[0].cfg['maxplayers'] - 2:
                    client.reply(self.channel, member,
                                 "pick_order letters count must equal required players number minus 2 (captains) for specified pickup.")
                    return
                for i in value:
                    if i not in ['a', 'b']:
                        client.reply(self.channel, member, "pick_order letters must be 'a' (team 1) or 'b' (team 2).")
                        return
                self.update_pickup_config(pickups[0], variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "map_pick_order":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "Disabled {0} for {1} pickups.".format(variable, ", ".join(i.name for i in pickups)))

            elif value.lower() == "mappref":
                for pickup in pickups:
                    self.update_pickup_config(pickup, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickup.".format(
                    value, variable, ", ".join(i.name for i in pickups)))

            else:
                for i in value.lower():
                    if i not in ['a', 'b']:
                        client.reply(self.channel, member, "pick_order letters must be `a`|`A`|`b`|`B`.")
                        return
                self.update_pickup_config(pickups[0], variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "map_probabilities":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "Disabled {0} for {1} pickups.".format(variable, ", ".join(i.name for i in pickups)))

            else:
                try:
                    [float(prob.strip()) for prob in value.split(",")]
                except ValueError:
                    client.reply(self.channel, member, "map_probabilities has to be numbers separated by comma")
                else:
                    for pickup in pickups:
                        self.update_pickup_config(pickup, variable, value)
                    client.reply(self.channel, member, "Set '{0}' {1} for {2} pickup.".format(
                        value, variable, ", ".join(i.name for i in pickups)))

        elif variable in ["no_repeat_maps", "map_pref_message"]:
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "Disabled {0} for {1} pickups.".format(variable, ", ".join(i.name for i in pickups)))

            else:
                for pickup in pickups:
                    self.update_pickup_config(pickup, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickup.".format(
                    value, variable, ", ".join(i.name for i in pickups)))

        elif variable == "promotion_role":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                role = client.find_role_by_name(self.channel, value)
                if role:
                    for i in pickups:
                        self.update_pickup_config(i, variable, role.id)
                    client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(role.name, variable,
                                                                                               ", ".join(i.name for i in
                                                                                                         pickups)))
                else:
                    client.reply(self.channel, member, "Role '{0}' not found on this discord server".format(value))

        elif variable == "blacklist_role":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                role = client.find_role_by_name(self.channel, value)
                if role:
                    for i in pickups:
                        self.update_pickup_config(i, variable, role.id)
                    client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(role.name, variable,
                                                                                               ", ".join(i.name for i in
                                                                                                         pickups)))
                else:
                    client.reply(self.channel, member, "Role '{0}' not found on this discord server".format(value))

        elif variable == "whitelist_role":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                role = client.find_role_by_name(self.channel, value)
                if role:
                    for i in pickups:
                        self.update_pickup_config(i, variable, role.id)
                    client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(role.name, variable,
                                                                                               ", ".join(i.name for i in
                                                                                                         pickups)))
                else:
                    client.reply(self.channel, member, "Role '{0}' not found on this discord server".format(value))

        elif variable == "captains_role":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                role = client.find_role_by_name(self.channel, value)
                if role:
                    for i in pickups:
                        self.update_pickup_config(i, variable, role.id)
                    client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(role.name, variable,
                                                                                               ", ".join(i.name for i in
                                                                                                         pickups)))
                else:
                    client.reply(self.channel, member, "Role '{0}' not found on this discord server".format(value))

        elif variable == "require_ready":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                try:
                    seconds = utils.parse_time_input(value.split(" "))
                except Exception as e:
                    client.reply(self.channel, member, str(e))
                    return
                for i in pickups:
                    self.update_pickup_config(i, variable, seconds)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(seconds, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "servers":
            game_server.set_servers_pickups(self, pickups, member, variable, value)

        elif variable == "whitelist_msg":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "blacklist_msg":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                for i in pickups:
                    self.update_pickup_config(i, variable, value)
                client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                    i.name for i in pickups)))

        elif variable == "extension":
            if value.lower() == "none":
                for i in pickups:
                    self.update_pickup_config(i, variable, None)
                client.reply(self.channel, member,
                             "{0} for {1} pickups will now fallback to the channel's default value.".format(variable,
                                                                                                            ", ".join(
                                                                                                                i.name
                                                                                                                for i in
                                                                                                                pickups)))
            else:
                try:
                    json.loads(value)
                    for i in pickups:
                        self.update_pickup_config(i, variable, value)
                    client.reply(self.channel, member, "Set '{0}' {1} for {2} pickups.".format(value, variable, ", ".join(
                        i.name for i in pickups)))
                except ValueError as e:
                    client.reply(self.channel, member, repr(e))

        else:
            client.reply(self.channel, member, "Variable '{0}' is not configurable.".format(variable))

    def find_pickup(self, pickup_name):
        # should have used a dict for pickups, because this is obviously going to take O(n)
        for p in self.pickups:
            if p.channel.id == self.channel.id and p.name == pickup_name:
                return p
        raise ValueError('there is no pickup with this name')

    async def clear_rating_messages(self, msg, args, access_level):
        if access_level > 1:
            async with msg.channel.typing():
                found_counter = 0
                counter = 0
                limit = 500
                if len(args) > 0:
                    if args[0] == "None":
                        limit = None
                    else:
                        try:
                            limit = int(args[0])
                        except ValueError:
                            limit = 500

                before = self.last_clear_rating_message_created_at or datetime.datetime.now()
                jump_url = ""
                async for message in msg.channel.history(limit=limit, before=before):
                    counter += 1
                    before = message.created_at
                    jump_url = message.jump_url
                    self.last_clear_rating_message_created_at = before
                    if message.author == msg.guild.me:
                        if any([message.content.startswith(needle) for needle in [
                            '```markdown',
                            '```python'
                        ]]):
                            if 'Rating' in message.content or all([needle in message.content for needle in [
                                '|', 'nickname', 'before'
                            ]]):
                                await message.edit(content="removed")
                                found_counter += 1
                        elif any([needle in message.content for needle in [
                            "pickup has been started",
                            "Unpicked",
                            "TEAMS READY"
                        ]]) and re.search(r'〈.?.?〉', message.content) is not None:
                            await message.edit(content=re.sub(r'〈.?.?〉', '-', message.content))
                            found_counter += 1
                if counter > 0:
                    client.private_reply(msg.author, "deleted or edited {} messages in {}.\n"
                                                     "Last msg processed: {}\n"
                                                     "Run it again to continue".format(
                        found_counter, msg.channel.mention, jump_url)
                                         )
                else:
                    client.private_reply(msg.author, "No more messages in channel")
        else:
            client.reply(self.channel, msg.author, "You have no right for this!")

    async def toggle_panzer(self, member: Member):
        panzer_role = self.cfg['panzer_role']
        if panzer_role is not None:
            role_obj = client.find_role_by_id(self, panzer_role)
            if not role_obj:
                client.reply(self.channel, member, f"Panzer role {panzer_role} not found.")
                return
            if role_obj in member.roles:
                await member.remove_roles(role_obj)
                client.reply(self.channel, member,
                             "Your panzer role has been removed. "
                             "Type !panzer again to regain the role.")
                return
            else:
                await member.add_roles(role_obj)
                client.reply(self.channel, member,
                             "You now have the panzer role. "
                             "Type !panzer again to remove the role.")
                return
        client.reply(self.channel, member, "Panzer role not set for this channel.")


def delete_channel(channel):
    for match in list(active_matches):
        if match.pickup.channel.id == channel.id:
            active_matches.remove(match)

    channels.remove(channel)
    stats3.delete_channel(channel.id)


def update_member(member):  # on status change
    if member not in allowoffline:
        if str(member.status) == 'offline':
            global_remove(member, 'offline')
        elif str(member.status) == 'idle':
            # dont remove if user have expire time set!
            if member.id not in scheduler.tasks.keys():
                global_remove(member, 'idle')


def global_remove(member, reason):
    # removes player from pickups on all channels
    affected_channels = []

    if reason == 'scheduler':
        affected_pickups = list(active_pickups)
    else:
        affected_pickups = [p for p in active_pickups if not p.cfg['allow_offline']]

    for p in affected_pickups:
        if member.id in [i.id for i in p.players]:
            if isinstance(member, User):
                member = p.channel.channel.guild.get_member(member.id)
            p.players.remove(member)
            if len(p.players) == 0:
                active_pickups.remove(p)
            if p.channel not in affected_channels:
                affected_channels.append(p.channel)

    for i in affected_channels:
        i.update_topic()
        if reason == 'scheduler':
            client.reply(i.channel, member, "you have been removed from all pickups as your !expire time ran off...")
        elif reason == 'idle':
            client.notice(i.channel, "<@{0}> went AFK and was removed from all pickups...".format(member.id))
        elif reason == 'offline':
            client.notice(i.channel, "**{0}** went offline and was removed from all pickups...".format(member.name))


def save_state():
    channels_dict = {}
    allowoffline_dict = {}
    scheduler_list = []
    matches_list = []

    for channel in channels:
        pickups = {}
        for pickup in channel.pickups:
            pickups[pickup.name] = {
                'players': [player.id for player in pickup.players]
            }

        # save party pickup, members and invites. Challenges are bit complicated and not really important
        parties = [{
            'pickup_name': party.pickup.name,
            'members': [member.id for member in party.members],
            'invite_list': [member.id for member in party.invite_list],
            'public': isinstance(party, PublicParty),
            'pickup': isinstance(party, PickupParty),
        } for party in channel.active_parties]

        channels_dict[channel.id] = {
            'pickups': pickups,
            'parties': parties
        }
    for member in allowoffline:
        # TODO why are there nones?
        if member is not None:
            if member.guild.id in allowoffline_dict.keys():
                allowoffline_dict[member.guild.id].append(member.id)
            else:
                allowoffline_dict[member.guild.id] = [member.id]

    for task_key in scheduler.tasks:
        task = scheduler.tasks[task_key]
        func_name = task[1].__name__
        if func_name == 'global_remove_callback':
            scheduler_list.append([task_key, int(task[0]), func_name, [task[1].__closure__[0].cell_contents.id]])
        if func_name == 'add_player':
            user_id = task[1].__closure__[0].cell_contents.id
            pickups = task[1].__closure__[1].cell_contents
            channel_id = task[1].__closure__[2].cell_contents.id
            scheduler_list.append([task_key, int(task[0]), func_name, [user_id, channel_id, pickups]])

    for match in active_matches:
        if match.state not in ['waiting_report']:
            raise Exception(f"cant save match #{match.id}, saving match with state {match.state} is not implemented. Save state aborting...")
        if hasattr(match, 'from_parties') and match.from_parties:
            raise Exception(f"cant save match #{match.id}, saving party match is not implemented. Save state aborting...")
        match_dict = {
            'id': match.id,
            'channel_id': match.channel.id,
            'pickup': match.pickup.name,
            'alpha_team': [player.id for player in match.alpha_team],
            'beta_team': [player.id for player in match.beta_team],
            'state': match.state,
            'start_time': int(match.start_time),
            'server': match.server,
            'gtv_match_id': match.gtv_match_id,
            'panzers': [player.id for player in match.panzers],
            're_queue': {
                channel.id: {
                    pickup.name: [player.id for player in match.re_queue[channel][pickup]]
                    for pickup in match.re_queue[channel]
                } for channel in match.re_queue
            },
            'captains': [player.id for player in (match.captains or [])]
        }
        matches_list.append(match_dict)

    state = {
        'version': 2,
        'channels': channels_dict,
        'active_matches': matches_list,
        'allowoffline': allowoffline_dict,
        'scheduler': scheduler_list
    }
    with open('state.json', 'w') as outfile:
        dump(state, outfile, indent=2)
        outfile.close()
    console.display("saved state")


def find_member_in_active_matches(user_id: int) -> Tuple[Member, Match]:
    for match in active_matches:
        member = next(player for player in match.players if player.id == user_id)
        return member, match
    raise StopIteration


def run(frame_time):
    for match in active_matches:
        match.think(frame_time)
