import threading
import client_config
from sentry_sdk import capture_exception
if hasattr(client_config, 'WEB_URL'):
    import requests
if client_config.IPC_ENABLED:
    from modules import ipc

# TODO: use observer pattern? https://stackoverflow.com/a/2022629

# TODO: subscribe to:
# player added, removed
# match started
# match state change
# match pick_step change
# match map_pick_order change, !put, !subfor, !capfor, !pick_captains or just alpha/beta players?
# match ready_use
# match alpha/beta draw
# match players_ready change
# user role change, role permission change -> reload pickups state
# !noadd !forgive
# party invite, challenge, list change, party player change
# rank change?
# expire change
# on channel message for read only channel? could also send messages as bot account


def post_to_web(endpoint, json, **kwargs):
    if not hasattr(client_config, 'WEB_URL'):
        return
    try:
        requests.post(client_config.WEB_URL + endpoint, json=json, verify=False)
    except Exception as e:
        capture_exception(e)
        pass


def fire_and_forget(endpoint, json, **kwargs):
    if client_config.IPC_ENABLED:
        threading.Thread(target=post_to_web, args=(endpoint, json), kwargs=kwargs).start()


# fire_and_forget(url, json=data, headers=headers)

def pickup_change(pickup):
    if client_config.IPC_ENABLED:
        fire_and_forget("/events", {"event": "pickup_change", "pickup": ipc.pickup_change_json(pickup)})


def match_change(match):
    if client_config.IPC_ENABLED:
        fire_and_forget("/events", {"event": "match_change", "match": ipc.match_json(match)})


def map_pick(match):
    if client_config.IPC_ENABLED:
        fire_and_forget("/events", {"event": "map_pick", "match": ipc.map_pick_json(match)})


def player_pick(match):
    if client_config.IPC_ENABLED:
        fire_and_forget("/events", {"event": "player_pick", "match": ipc.player_pick_json(match)})


def set_ready(match):
    if client_config.IPC_ENABLED:
        fire_and_forget("/events", {"event": "set_ready", "match": ipc.set_ready_json(match)})
