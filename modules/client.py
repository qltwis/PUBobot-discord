import re
from json import load, loads, JSONDecodeError, dumps
from typing import Optional

import discord
import time
import traceback

from discord import ActivityType
from discord.ext import commands
from sentry_sdk import capture_exception

import client_config
from modules import console, config, bot, stats3, scheduler
from modules.party import Party, PublicParty, PickupParty
from modules.status_message import StatusMessage, STATUS_MESSAGE_FIELD_NAMES
from modules.exceptions import PubobotException
from discord_sentry_reporting import use_sentry

status_messages_ids = set()


def init():
    global ready, send_queue
    ready = False
    send_queue = []


async def process_connection():
    global ready

    console.display('SYSTEM| Logged in as: {0}, ID: {1}'.format(c.user.name, c.user.id))

    channels = stats3.get_channels()
    state = None
    try:
        with open('state.json') as json_file:
            state = load(json_file)
            json_file.close()
    except FileNotFoundError as e:
        console.display("SYSTEM| loading state.json: file doesn't exist")
        pass

    for cfg in channels:
        discord_channel = c.get_channel(cfg['channel_id'])
        if discord_channel is None:
            console.display(
                "SYSTEM| Could not find channel '{0}>{1}#' with id: '{2}'! Skipping..."
                .format(cfg['server_name'], cfg['channel_name'], cfg['channel_id']))
        # todo: delete channel
        else:
            chan = bot.Channel(discord_channel, cfg)
            if state is not None:

                async def get_member_by_id_exception(channel, id):
                    res = await get_member_by_id(channel, str(id))
                    if res is None:
                        raise Exception("member {} in {} not found".format(id, channel.id))
                    else:
                        return res

                for pickup in chan.pickups:
                    if str(chan.id) in state['channels'] and pickup.name in state['channels'][str(chan.id)]['pickups']:
                        try:
                            pickup.players = [
                                await get_member_by_id_exception(chan, str(player_id)) for player_id in
                                state['channels'][str(chan.id)]['pickups'][pickup.name]['players']
                            ]
                            if len(pickup.players):
                                bot.active_pickups.append(pickup)
                        except Exception as e:
                            console.display("ERROR| loading pickup from saved state: {}".format(traceback.format_exc()))
                if cfg['party']:
                    if str(chan.id) in state['channels']:
                        try:
                            for party_dict in state['channels'][str(chan.id)]['parties']:
                                try:
                                    pickup = next(pickup for pickup in chan.pickups
                                                  if pickup.name.lower() == party_dict['pickup_name'])
                                    members = [
                                        await get_member_by_id_exception(chan, str(player_id)) for player_id in
                                        party_dict['members']
                                    ]
                                    if party_dict['public']:
                                        party = PublicParty(members[0], pickup, members[1:])
                                    elif party_dict['pickup']:
                                        party = PickupParty(members[0], pickup, members[1:])
                                    else:
                                        party = Party(members[0], pickup, members[1:])
                                    party.invite_list = [
                                        await get_member_by_id_exception(chan, str(player_id)) for player_id in
                                        party_dict['invite_list']
                                    ]
                                    chan.active_parties.append(party)
                                except Exception as e:
                                    console.display(
                                        "ERROR| loading party from saved state: {}".format(traceback.format_exc()))
                        except Exception as e:
                            console.display(
                                "ERROR| loading parties from saved state: {}".format(traceback.format_exc()))
            bot.channels.append(chan)
            console.display(
                "SYSTEM| '{0}>{1}#' channel init successful".format(chan.cfg['server_name'], chan.cfg['channel_name']))
    if state is not None and 'allowoffline' in state:
        for guild_id in state['allowoffline']:
            try:
                server = c.get_guild(int(guild_id))
                for member_id in state['allowoffline'][guild_id]:
                    bot.allowoffline.append(discord.utils.find(lambda m: m.id == member_id, server.members))
            except:
                pass

    if state is not None and 'scheduler' in state and state['version'] == 2:
        for task in state['scheduler']:
            func_name = task[2]
            if func_name == 'global_remove_callback':
                try:
                    user = await c.fetch_user(task[3][0])
                except Exception as e:
                    console.display("ERROR| loading scheduler: user {} failed to fetch".format(task[3]))
                else:
                    def global_remove_callback():
                        bot.global_remove(user, 'scheduler')

                    scheduler.tasks[task[0]] = [task[1], global_remove_callback]
            elif func_name == 'add_player':
                channel = next((ch for ch in bot.channels if ch.id == task[3][1]))
                try:
                    member = await channel.channel.guild.fetch_member(task[3][0])
                except Exception as e:
                    console.display("ERROR| loading scheduler: guild member {} failed to fetch".format(task[3]))
                else:
                    pickups = task[3][2]
                    callback = channel.add_in_callback(member, pickups)
                    scheduler.tasks[task[0]] = [task[1], callback, channel.add_in_cancel_callback(member)]
                    channel.scheduled_adds.append(member)
            else:
                console.display("ERROR| loading scheduler: function name {} not implemented".format(func_name))
        scheduler.define_next_task()

    if state is not None and 'active_matches' in state:
        for match in state['active_matches']:
            channel = next(channel for channel in bot.channels
                           if channel.id == int(match['channel_id']))
            pickup = next(pickup for pickup in channel.pickups if pickup.name == match['pickup'])
            players = [channel.channel.guild.get_member(player) for player in
                       (match['alpha_team'] + match['beta_team'])]
            match['alpha_team'] = [next(player for player in players
                                        if player.id == team_player) for team_player in match['alpha_team']]
            match['beta_team'] = [next(player for player in players
                                       if player.id == team_player) for team_player in match['beta_team']]
            match_obj = bot.Match(pickup, players, match_dict=match)
            match_obj.id = match['id']
            stats3.last_match = match['id']
            if 'panzers' in match:
                match_obj.panzers = [channel.channel.guild.get_member(player) for player in match['panzers']]

            if 're_queue' in match:
                if 'version' in state and state['version'] > 1:
                    match_obj.re_queue = {
                        (_ch := next(channel for channel in bot.channels
                                     if channel.id == int(channel_id))): {
                            next(pickup for pickup in _ch.pickups if pickup.name == pickup_name): [
                                next(player for player in players if player.id == player_id)
                                for player_id in match['re_queue'][channel_id][pickup_name]
                                if player_id in [_p.id for _p in players]
                            ]
                            for pickup_name in match['re_queue'][channel_id]
                        } for channel_id in match['re_queue']
                    }
                else:
                    """
                    1st version of re_queue in state.json just contains list of players
                    that would be added to the same pickup as match pickup 
                    """
                    match_obj.re_queue = {
                        channel: {
                            pickup: [
                                next(player for player in players if player.id == player_id)
                                for player_id in match['re_queue']
                                if player_id in [_p.id for _p in players]
                            ]
                        }
                    }
            if 'captains' in match:
                match_obj.captains = [channel.channel.guild.get_member(player) for player in match['captains']]
    ready = True
    await init_status_messages()


async def init_status_messages():
    console.display("DEBUG| init status msg")
    for row in stats3.get_status_messages():
        status_channel = c.get_channel(row['channel_id'])
        if status_channel is None:
            console.display("DEBUG| init status msg: channel # not found, removing msg".format(row['channel_id']))
            stats3.remove_status_message(row['message_id'])
            continue
        try:
            status_message = StatusMessage(await status_channel.fetch_message(row['message_id']))
        except discord.errors.NotFound:
            console.display("DEBUG| init status msg: msg {}>{}#{} not found, removing msg".format(
                status_channel.guild.name,
                status_channel.name,
                row['message_id']))
            stats3.remove_status_message(row['message_id'])
            continue
        status_messages_ids.add(row['message_id'])
        # TODO: check row['channel_ids']!=""
        for status_message_channel_id in [int(s) for s in row['channel_ids'].split(',')]:
            try:
                channel = next(_channel for _channel in bot.channels if _channel.id == status_message_channel_id)
            except StopIteration:
                console.display("DEBUG| init status msg: bot channel #{} not found".format(status_channel.id))
            else:
                if not channel.cfg['public']:
                    console.display("DEBUG| init status msg: bot channel #{} not public".format(status_channel.id))
                    continue
                status_message.channels.append(channel)
                channel.status_messages.append(status_message)

    for channel in bot.channels:
        channel.status_message_dict['pickup_msg'] = channel.get_topic()

        channel.status_message_dict['matches_msg'] = channel.format_matches_status()

        channel.status_message_dict['party_msg'] = ' '.join(
            [p.status_message_str() for p in channel.active_parties]
        )


def get_empty_servers():
    for serv in c.guilds:
        n = 0
        for chan in serv.channels:
            if chan.id in [i.cfg['channel_id'] for i in bot.channels]:
                n = 1
                break
        if not n:
            console.display("server name: {0}, id: {1}".format(serv.name, serv.id))


async def send():  # send messages in queue
    global send_queue
    if len(send_queue):
        for func, kwargs in send_queue:
            try:
                await func(**kwargs)
            except Exception as e:
                capture_exception(e)
                console.display("ERROR| could not send data ({0}). {1}".format(str(func), str(e)))
        send_queue = []


async def close():  # on quit
    if c.is_closed():
        try:
            await c.logout()
            print("Successfully logged out.")
        except Exception as e:
            print("Error on logging out. {0}".format(str(e)))
    else:
        print("Connection is already closed.")


### api for bot.py ###
def find_role_by_name(channel, name):
    name = name.lower()
    server = c.get_guild(channel.guild.id)
    if server:
        for role in server.roles:
            if name == role.name.lower():
                return role
    return None


def find_role_by_id(channel, role_id):
    server = c.get_guild(channel.guild.id)
    if server:
        for role in server.roles:
            if role_id == role.id:
                return role
    return None


async def edit_role(role, **fields):
    await role.edit(**fields)


async def remove_roles(member, *roles):
    await member.remove_roles(*roles)


async def add_roles(member, *roles):
    await member.add_roles(*roles)


def notice(
        channel: discord.TextChannel,
        msg: Optional[str],
        embed: Optional[discord.Embed] = None,
        view: Optional[discord.ui.View] = None) -> None:
    console.display("SEND| {0}> {1}".format(channel.name, msg))
    send_queue.append([channel.send, {'content': msg, 'embed': embed, 'view': view}])


def reply(
        channel: discord.TextChannel,
        member: discord.Member,
        msg: str,
        view: Optional[discord.ui.View] = None) -> None:
    console.display("SEND| {0}> {1}, {2}".format(channel.name, member.nick or member.name, msg))
    send_queue.append([channel.send, {'content': "<@{0}>, {1}".format(member.id, msg), 'view': view}])


def private_reply(member, msg):
    if not member.bot:
        console.display("SEND_PM| {0}> {1}".format(member.name, msg))
        send_queue.append([member.send, {'content': msg}])


def delete_message(msg):
    send_queue.append([msg.delete, {}])


def edit_message(msg, new_content, embed=None):
    console.display("EDIT| {}>{}# {}".format(
        msg.channel.guild.name,
        msg.channel.name,
        new_content or 'embed #{}'.format(msg.id))
    )
    send_queue.append([msg.edit, {'content': new_content, 'embed': embed}])


def get_member_by_nick(channel, nick):
    server = c.get_guild(channel.guild.id)
    return discord.utils.find(lambda m: m.name == nick, server.members)


async def get_member_by_id(channel, highlight):
    member_id = highlight.lstrip('<@!').rstrip('>')
    if member_id.isdigit():
        member_id = int(member_id)
        guild = c.get_guild(channel.guild.id)
        return await guild.fetch_member(member_id)
    else:
        return None


### discord events ###
intents = discord.Intents.default()
intents.presences = client_config.INTENTS_PRESENCES
intents.members = client_config.INTENTS_MEMBERS
intents.message_content = True


class MyBot(commands.Bot):
    def __init__(self):
        super().__init__(command_prefix='$', intents=intents)
        self.client_config = client_config

    async def setup_hook(self):
        if client_config.IPC_ENABLED:
            await self.load_extension('modules.ipc')


c = MyBot()

if hasattr(client_config, "SENTRY_DSN"):
    use_sentry(
        c,
        dsn=client_config.SENTRY_DSN,
        traces_sample_rate=client_config.SENTRY_RATE,
        _experiments={
            "profiles_sample_rate": client_config.SENTRY_RATE,
        }
    )


@c.event
async def on_ready():
    global ready
    if not ready:
        await process_connection()
        ready = True
    else:
        console.display("DEBUG| Unexpected on_ready event!")
    status = '!help'
    if hasattr(client_config, 'DONATE_REPLY'):
        status += ' !donate'
    await c.change_presence(activity=discord.Activity(type=ActivityType.watching, name=status))


@c.event
async def on_message_edit(_, after):
    await on_message(after)


@c.event
async def on_message(message):
    if isinstance(message.channel, discord.abc.PrivateChannel) and message.author.id != c.user.id:
        console.display("PRIVATE| {0}>{1}>{2}: {3}".format(message.guild, message.channel, message.author.display_name,
                                                           message.content))

        if message.content == "!help":
            private_reply(message.author, config.cfg.HELPINFO)
        elif message.content == "!donate":
            if hasattr(client_config, 'DONATE_REPLY'):
                private_reply(message.author, client_config.DONATE_REPLY)
        elif message.content == "!web":
            if hasattr(client_config, 'PUBLIC_WEB_URL'):
                private_reply(message.author, client_config.PUBLIC_WEB_URL)

    elif isinstance(message.channel, discord.TextChannel):
        if message.content == '!enable_pickups':
            if message.channel.permissions_for(message.author).manage_channels:
                if message.channel.id not in [x.id for x in bot.channels]:
                    new_cfg = stats3.new_channel(message.guild.id, message.guild.name, message.channel.id,
                                                 message.channel.name, message.author.id)
                    bot.channels.append(bot.Channel(message.channel, new_cfg))
                    reply(message.channel, message.author, config.cfg.FIRST_INIT_MESSAGE)
                else:
                    reply(message.channel, message.author, "this channel already have pickups configured!")
            else:
                reply(message.channel, message.author, "You must have permission to manage channels to enable pickups.")
        elif message.content == '!disable_pickups':
            if message.channel.permissions_for(message.author).manage_channels:
                for chan in bot.channels:
                    if chan.id == message.channel.id:
                        bot.delete_channel(chan)
                        reply(message.channel, message.author, "pickups on this channel have been disabled.")
                        return
                reply(message.channel, message.author, "pickups on this channel has not been set up yet!")
            else:
                reply(message.channel, message.author,
                      "You must have permission to manage channels to disable pickups.")

        elif message.content.startswith('!status_message'):
            if not message.channel.permissions_for(message.author).manage_guild:
                reply(message.channel, message.author,
                      "You must have permission to manage guild.")
                return
            if not message.channel.permissions_for(message.guild.me).embed_links:
                reply(message.channel, message.author, f"bot doesn't have permission to embed links")
                return

            args = message.content.split()[1:]
            if not len(args):
                await message.channel.send("usage: !status_message channel_id,...")
            status_message = StatusMessage()
            for arg in args:
                try:
                    arg_channel_id = int(re.search(r'\d+', arg)[0])
                except (IndexError, TypeError):
                    await message.channel.send("status_message: could not parse channel id")
                    return
                found = False
                for channel in bot.channels:
                    if channel.id == arg_channel_id:
                        found = True
                        if not channel.cfg['public']:
                            await message.channel.send("status_message: specified channel is not public"
                                                       "Ask channel admin to !set_default public 1")
                            return
                        status_message.channels.append(channel)
                        channel.status_messages.append(status_message)
                        break
                if not found:
                    await message.channel.send(
                        "status_message: channel with id {}, does not have any pickups configured".format(
                            arg_channel_id)
                    )
            status_message.discord_msg = await message.channel.send(
                embed=discord.Embed.from_dict(status_message.build_embed_dict(message.channel))
            )
            status_messages_ids.add(status_message.discord_msg.id)
            stats3.add_status_message(
                status_message.discord_msg.id,
                message.channel.id,
                [channel.id for channel in status_message.channels]
            )

        elif message.reference and message.reference.message_id in status_messages_ids:
            if message.content == 'json':
                try:
                    embed = next(iter(message.reference.resolved.embeds))
                except StopIteration:
                    return
                else:
                    embed_dict = embed.to_dict()
                    if "fields" in embed_dict:
                        embed_dict["fields"] = [field for field in embed_dict["fields"] if
                                                field["name"] not in STATUS_MESSAGE_FIELD_NAMES]
                        if not len(embed_dict["fields"]):
                            del embed_dict["fields"]
                    if "type" in embed_dict:
                        del embed_dict["type"]
                    private_reply(message.author, dumps(embed_dict, indent=2))
                    return
            if not message.channel.permissions_for(message.author).manage_guild:
                reply(message.channel, message.author,
                      "You must have permission to manage guild.")
                return
            if message.content == 'remove':
                status_messages_ids.remove(message.reference.message_id)
                for channel in bot.channels:
                    channel.status_messages = [
                        sm for sm in channel.status_messages if sm.discord_msg.id != message.reference.message_id
                    ]
                stats3.remove_status_message(message.reference.message_id)
                if message.reference.resolved is not None and isinstance(message.reference.resolved, discord.Message):
                    await message.reference.resolved.delete()
                return
            if message.content == 'bump':
                return  # TODO
            for channel in bot.channels:
                for status_message in channel.status_messages:
                    if message.reference.message_id == status_message.discord_msg.id:
                        try:
                            input_embed_dict = loads(message.content)
                        except JSONDecodeError as e:
                            reply(message.channel, message.author, str(e))
                            return
                        embed_dict = next(iter(status_message.discord_msg.embeds)).to_dict()
                        embed_dict = {**embed_dict, **input_embed_dict}
                        edit_message(status_message.discord_msg, None, discord.Embed.from_dict(embed_dict))
                        return

        elif message.content != '':
            for channel in bot.channels:
                if message.channel.id == channel.id and isinstance(message.author, discord.Member):
                    try:
                        await channel.process_message(message)
                    except PubobotException as e:
                        reply(channel.channel, message.author, e)
                    except Exception as e:
                        capture_exception(e)
                        console.display("ERROR| Error processing message: {0}".format(traceback.format_exc()))


@c.event
async def on_presence_update(before, after):
    # console.display("DEBUG| {0} changed status from {1}  to -{2}-".format(after.name, before.status, after.status))
    if (str(before.status) in ['online', 'dnd'] and str(after.status) in ['idle', 'offline']) \
            or (str(before.status) == 'idle' and str(after.status) == 'offline'):
        if str(after.mobile_status) != 'online':
            bot.update_member(after)
    if not any([isinstance(activity, discord.Streaming) for activity in before.activities]):
        for activity in after.activities:
            if isinstance(activity, discord.Streaming):
                for match in bot.active_matches:
                    if after.guild.id == match.channel.guild.id:
                        if after.id not in match.streaming_players:
                            for player in match.players:
                                if before.id == player.id:
                                    try:
                                        embed = discord.Embed()
                                        embed.title = activity.name
                                        embed.description = f"{player.mention} is streaming match #{match.id}"
                                        if activity.platform=='Twitch':
                                            embed.set_author(name=activity.twitch_name, url=activity.url)
                                            embed.set_thumbnail(url=f"https://static-cdn.jtvnw.net/previews-ttv/live_user_{activity.twitch_name}-162x90.jpg")
                                        embed.url=activity.url
                                        embed.colour = discord.Colour.from_str('#593695')
                                        notice(match.channel, msg=None, embed=embed)
                                        match.streaming_players[after.id] = activity
                                        break
                                    except Exception as e:
                                        capture_exception(e)
                break


@c.event
async def on_reaction_add(reaction, user):
    if not reaction.me:
        return
    if user.bot and user == c.user:
        return
    if reaction.message.id in status_messages_ids and reaction.message.channel.permissions_for(
            reaction.message.author).manage_guild:
        if reaction.emoji == '🇯':
            status_message = reaction.message
            try:
                embed = next(iter(reaction.message.embeds))
            except StopIteration:
                return
            else:
                private_reply(user, embed.to_dict())


### connect to discord ###
async def run(background_task):
    async with c:
        c.loop.create_task(background_task())
        while True:
            try:
                if config.cfg.DISCORD_TOKEN != "":
                    console.display("SYSTEM| logging in with token...")
                    await c.start(config.cfg.DISCORD_TOKEN)
                await c.connect()
            except KeyboardInterrupt:
                console.display("ERROR| Keyboard interrupt.")
                console.terminate()
                await close()
                print("QUIT NOW.")
                break
            except Exception as e:
                console.display("ERROR| Disconnected from the server: " + str(e) + "\nReconnecting in 15 seconds...")
                time.sleep(15)
